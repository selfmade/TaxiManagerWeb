<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.bdy.lm.browser.domain.CarMeasure"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import= "com.bdy.lm.browser.domain.*"%>

<html>
<head>
<title>主页</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<%-- #nav,#main 修改width整体占屏百分比 --%>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

body {
	font-size: 25px;
	font-family: Arial, 微软雅黑， "Microsoft yahei", "Hiragino Sans GB",
		"冬青黑体简体中文 w3", STXihei, 华文细黑, SimSun, 宋体, Heiti, 黑体, sans-serif;
}

ul {
	list-style: none;
}

div table table td {
	text-align: center;
}

div table table tr {
	text-align: center;
}

#nav,#main {
	width: 99%;
	margin: 0px auto;
	border: 1px solid #999;
}

#nav {
	line-height: 70px;
	background: #666;
}

#nav a {
	display: block;
	width: 100%;
	text-align: center;
}

a:link {
	text-decoration: none;
}

a:visited {
	text-decoration: none;
}

a:hover {
	text-decoration: none;
	font-weight: bold;
	background: #ff9900;
	color: #fff;
}

#nav li {
	width: 25%;
	float: left;
	background: #5F9EA0;
}

#nav li ul {
	line-height: 50px;
	display: none;
}

#nav li ul li {
	float: left;
	width: 310px;
	background: #6495ED;
}

#nav li ul li a {
	display: block;
	width: 300px;
	text-align: left;
	padding-left: 10px;
}

#nav li:hover ul {
	display: block;
	text-align: left;
	width: 180px;
	position: absolute;
	left: auto;
	top: auto;
}

#main {
	height: 500px;
	border: none;
}

.dis {
	display: block;
}

.udis {
	display: none;
}

.show {
	visibility: visible;
}

.ushow {
	visibility: hidden;
}
h2 { text-align: center;}
</style>
<script type="text/javascript">
	function base(x) {
	var li = x.parentNode.getElementsByTagName("li");
	var div = document.getElementsByTagName("div");
	// if(x.parentNode.id == "base"){alert(x.parentNode.id);}
	// alert(x.parentNode.id);
	var j, l;
	if (x.parentNode.id == "base") {
		l = 1;
	} else if (x.parentNode.id == "app") {
		l = 5;
	} else if (x.parentNode.id == "monitor") {
		l = 9;
	} else if (x.parentNode.id == "verify") {
		l = 17;
	}
	for (i = 0; i < li.length; i++) {
		if (x == li[i]) {
			j = i;
			div[i + l].className = "dis";
			// session.setAttribute("location", i+l);
		}
	}
	for (k = 0; k < 20; k++) {
		if (k != j + l) {
			div[k].className = "udis";
		}
	}
	// alert("第"+0 +"个:"+ ct[0].className);
	// location.reload();/TaxiManagerServer/home/baseInfo.jsp
}
function deleteDriver() {
	var select = document.getElementsByName("driverSelected");
	var tableId = document.getElementById("driverTable");
	var index = 0;
	var uri = "/TaxiManagerServer/DriverServlet?method=deleteDriver&";

	var con = confirm("确定要删除吗?"); // 在页面上弹出对话框
	if (con == true) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += index;
				uri += "=";
				uri += tableId.rows[i + 1].cells[2].innerHTML;
				uri += "&";
				index++;
			}
		}
		location.href = uri;
	}
	// alert(id);
}
function editDriver() {
	var select = document.getElementsByName("driverSelected");
	var tableId = document.getElementById("driverTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/DriverServlet?method=editDriver&id=";
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[2].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行修改！！");
	}
}

function editTaxi() {
	var select = document.getElementsByName("taxiSelected");
	var tableId = document.getElementById("taxiTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/TaxiServlet?method=editTaxi&license=";//
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[0].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行修改！！");
	}
}
function deleteTaxi() {
	var select = document.getElementsByName("taxiSelected");
	var tableId = document.getElementById("taxiTable");
	var index = 0;
	var uri = "/TaxiManagerServer/TaxiServlet?method=deleteTaxi&";

	var con = confirm("确定要删除吗?"); // 在页面上弹出对话框
	if (con == true) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += index;
				uri += "=";
				uri += tableId.rows[i + 1].cells[0].innerHTML;
				uri += "&";
				index++;
			}
		}
		location.href = uri;
	}
	// alert(id);
}
function editCompany() {
	var select = document.getElementsByName("companySelected");
	var tableId = document.getElementById("companyTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/CompanyServlet?method=editCompany&companyId=";//
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[0].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行修改！！");
	}
}
function deleteCompany() {
	var select = document.getElementsByName("companySelected");
	var tableId = document.getElementById("companyTable");
	var index = 0;
	var uri = "/TaxiManagerServer/CompanyServlet?method=deleteCompany&";

	var con = confirm("确定要删除吗?"); // 在页面上弹出对话框
	if (con == true) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += index;
				uri += "=";
				uri += tableId.rows[i + 1].cells[0].innerHTML;
				uri += "&";
				index++;
			}
		}
		location.href = uri;
	}
	// alert(id);
}
function editFare() {
	var select = document.getElementsByName("fareSelected");
	var tableId = document.getElementById("fareTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/FareServlet?method=editFare&updateTime=";//
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[20].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行修改！！");
	}
}
function deleteFare() {
	var select = document.getElementsByName("fareSelected");
	var tableId = document.getElementById("fareTable");
	var index = 0;
	var uri = "/TaxiManagerServer/FareServlet?method=deleteFare&";

	var con = confirm("确定要删除吗?"); // 在页面上弹出对话框
	if (con == true) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += index;
				uri += "=";
				uri += tableId.rows[i + 1].cells[19].innerHTML;
				uri += "&";
				index++;
			}
		}
		location.href = uri;
	}
	// alert(id);
}

function queryCheatMonitor() {
	var select = document.getElementsByName("transactionSelected");
	var tableId = document.getElementById("transactionTable");
	var index = 0;
	var uri = "/TaxiManagerServer/OperationQueryServlet?method=queryCheatMonitor&license=";
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {// 选中的个数
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				if(tableId.rows[i + 1].cells[16].innerHTML == "无记录") {
					alert("无记录");
				} else {
					uri += encodeURI(tableId.rows[i + 1].cells[0].innerHTML);
					//uri += tableId.rows[i + 1].cells[0].innerHTML;
					uri = uri + "&getOnTime=" + tableId.rows[i + 1].cells[3].innerHTML;
				}
			}
		}//alert(uri);
		location.href = uri;
	} else {
		alert("请选择一项进行查询！！");
	}
}
function queryVerifyTrace() {
	var select = document.getElementsByName("carMeasureSelected");
	var tableId = document.getElementById("carMeasureTable");
	var index = 0;
	var uri = "/TaxiManagerServer/VerifyAdjustServlet?method=queryVerifyTrace&verifyTime=";
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {// 选中的个数
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[16].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行查询！！");
	}
}
function editStandardRoute() {
	var select = document.getElementsByName("standardRouteSelected");
	var tableId = document.getElementById("StandardRouteTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/VerifyAdjustServlet?method=editStandardRoute&num=";
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {// 选中的个数
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += tableId.rows[i + 1].cells[0].innerHTML;
			}
		}
		location.href = uri;
	} else {
		alert("请选择一项进行修改！！");
	}
}
function deleteStandardRoute() {
	var select = document.getElementsByName("standardRouteSelected");
	var tableId = document.getElementById("StandardRouteTable");
	var index = 0;
	var uri = "/TaxiManagerServer/VerifyAdjustServlet?method=deleteStandardRoute&";

	var con = confirm("确定要删除吗?"); // 在页面上弹出对话框
	if (con == true) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				uri += index;
				uri += "=";
				uri += tableId.rows[i + 1].cells[0].innerHTML;
				uri += "&";
				index++;
			}
		}
		location.href = uri;
	}
	// alert(id);
}
//发送标准路线到终端
function sendToISU() {
	var select = document.getElementsByName("standardRouteSelected");
	var tableId = document.getElementById("StandardRouteTable");//
	var index = 0;
	var uri = "/TaxiManagerServer/VerifyAdjustServlet?method=sendToISU&num=";
	var selectNum = new Array();
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;//选中的个数
			selectNum.push(tableId.rows[i + 1].cells[0].innerHTML);
		}
	}
	if (index >= 1) {//可以选多条路线，至少选择一条
		var con = confirm("确定要发送编号为 :" + selectNum.toString() + "的路线?"); //在页面上弹出对话框
		if (con == true) {
			uri += selectNum.toString();
			location.href = uri;
		}
	} else {
		alert("请选择一条路线进行校准！！");
	}
}
function queryDetail() {
	var queryLocationRunStateDetailForm = document.getElementById("queryLocationRunStateDetailForm");
	var select = document.getElementsByName("locationRunStateSelected");
	var tableId = document.getElementById("locationRunStateTable");
	var index = 0;
	var license = document.createElement("input"); // 创建input节点
	license.name = "license";
	license.type = "hidden";
	for (i = 0; i < select.length; i++) {
		if (select[i].checked == true) {
			index++;
		}
	}
	if (index == 1) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				license.value = tableId.rows[i + 1].cells[1].innerHTML;
				queryLocationRunStateDetailForm.appendChild(license);
				queryLocationRunStateDetailForm.submit();
			}
		}
	} else {
		alert("请选择一项进行查询！！");
	}
}
function traceReplay() {
	var traceReplayForm = document.getElementById("traceReplayForm");
	var select = document.getElementsByName("traceReplaySelected");
	var tableId = document.getElementById("locationRunStateTable");
	var index = 0;
	var license = document.createElement("input"); // 创建input节点
	license.name = "license";
	license.type = "hidden";
	if ((document.traceReplayFormName.traceTimeBegin.value != "")
			&& (document.traceReplayFormName.traceTimeEnd.value != "")) {
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				index++;
			}
		}
		if (index == 1) {
			for (i = 0; i < select.length; i++) {
				if (select[i].checked == true) {
					license.value = tableId.rows[i + 1].cells[1].innerHTML;
					traceReplayForm.appendChild(license);
					traceReplayForm.submit();
				}
			}
		} else {
			alert("请选择一项进行查询！！");
		}
	} else {
		alert("请填写完整时间！！");
	}
}
function remoteManage(sendType, msgID) {
	var remoteManageForm = document.getElementById("remoteManageForm");
	var type = document.createElement("input"); // 创建input节点，添加发送类型
	type.name = "sendType";
	type.value = sendType;
	type.type = "hidden";
	remoteManageForm.appendChild(type); // 添加到form中显示
	var content = document.createElement("input"); // 创建input节点，添加发送类型
	content.name = "msgID";
	content.value = msgID;
	content.type = "hidden";
	remoteManageForm.appendChild(content); // 添加到form中显示

	if (sendType == "all") {
		var con = confirm("确定要发送给全体成员吗?" + sendType); // 在页面上弹出对话框
		if (con == true) {
			remoteManageForm.submit();
		}
		// alert(msgID.value);
	} else if (sendType == "part") {

		var select = document.getElementsByName("remoteManageTaxi");
		var tableId = document.getElementById("remoteManageTaxiTable");
		var index = 0;
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				var license = document.createElement("input"); // 创建input节
				license.name = index;
				license.value = tableId.rows[i + 1].cells[0].innerHTML;
				license.type = "hidden";
				remoteManageForm.appendChild(license);
				index++;
			}
		}
		if (index == 0) {
			alert("请先选择要发送的成员！");
		} else {
			remoteManageForm.submit();
		}
	}
}
function sendMSg(sendType, location) {
	var messageArea = document.getElementById("messageArea").value;

	if (messageArea == null) {
		alert("发送的消息不能为空");
	} else {
		var messageForm = document.getElementById("messageForm");

		var msgID = document.createElement("input"); // 创建input节点，添加msgId
		msgID.name = "msgID";
		msgID.value = 109;
		msgID.type = "hidden";
		messageForm.appendChild(msgID); // 添加到form中显示

		var type = document.createElement("input"); // 创建input节点，添加发送类型
		type.name = "sendType";
		type.value = sendType;
		type.type = "hidden";
		messageForm.appendChild(type); // 添加到form中显示

		if (sendType == "all") {
			var con = confirm("确定要发送给全体成员吗?"); // 在页面上弹出对话框
			if (con == true) {
				messageForm.submit();
			}
			// alert(msgID.value);
		} else if (sendType == "part") {
			var select = document.getElementsByName("sendMsgSelected");
			var tableId = document.getElementById("allLicenseTable");
			var index = 0;
			for (i = 0; i < select.length; i++) {
				if (select[i].checked == true) {

					var sendLicense = document.createElement("input"); // 创建input节点
					sendLicense.name = index;
					sendLicense.value = tableId.rows[i + 1].cells[0].innerHTML;
					sendLicense.type = "hidden";
					messageForm.appendChild(sendLicense);
					index++;
				}
			}
			// alert(index);
			if (index == 0) {
				alert("请先选择要发送的成员！");
			} else {
				messageForm.submit();
			}
		}
	}

}

	window.onload = function() {
		var div = document.getElementsByTagName("div");
		var j = <%=request.getAttribute("pageLocation")%>;
		div[j].className = "dis";
		for (k = 0; k < 20; k++) {
			if (k != j) {
				div[k].className = "udis";
			}
		}
	}
</script>
<!-- <script language="javascript" type="text/javascript" src="/TaxiManagerServer/js/home.js"></script> -->
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath }/My97DatePicker/WdatePicker.js"></script>
</head>

<body bgcolor="#fff2cc">
	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
			<p>
				${sessionScope.sessionUser.identity }：${sessionScope.sessionUser.name }&nbsp;&nbsp;&nbsp; 
				<a	href="/TaxiManagerServer/UserServlet?method=exit" style="font-size:25px; font-weight:bold;text-align:right">退出</a>
			</p>
			<br>
			<h1 align="center">
				<b
					style="letter-spacing:5px; font-size:60px">出租汽车计价器远程在线计量校准管理系统</b>
			</h1>
			<br>
			<hr>
			<ul id="nav">
				<li id="li_a"><a href="#">基础信息管理</a>
					<ul id="base">
						<li onclick="base(this)"><a href="#">驾驶员基础信息管理</a></li>
						<li onclick="base(this)"><a href="#">车辆基础信息管理</a></li>
						<li onclick="base(this)"><a href="#">企业基础信息管理</a></li>
						<li onclick="base(this)"><a href="#">运价基础信息管理</a></li>
					</ul></li>
				<li id="li_a"><a href="#">应用信息管理</a>
					<ul id="app">
						<li onclick="base(this)"><a href="#">汽车卫星定位与运行状态</a></li>
						<li onclick="base(this)"><a href="#">汽车应急指挥管理信息</a></li>
						<li onclick="base(this)"><a href="#">汽车稽查管理信息</a></li>
						<li onclick="base(this)"><a href="#">汽车营运信息查询</a></li>
					</ul></li>
				<li id="li_a"><a href="#">监控指挥中心</a>
					<ul id="monitor">
						<li onclick="base(this)"><a href="#">车辆定位监控</a></li>
						<li onclick="base(this)"><a href="#">车辆轨迹回放</a></li>
						<li onclick="base(this)"><a href="#">车辆远程管理</a></li>
						<li onclick="base(this)"><a href="#">车辆报警提醒</a></li>
						<li onclick="base(this)"><a href="#">应急指挥车辆</a></li>
						<li onclick="base(this)"><a href="#">车辆投诉管理</a></li>
						<li onclick="base(this)"><a href="#">综合运行分析</a></li>
						<li onclick="base(this)"><a href="#">信息发布</a></li>
					</ul></li>
				<li id="li_a"><a href="#">检定校准中心</a>
					<ul id="verify">
						<li onclick="base(this)"><a href="#">标准路线</a></li>
						<li onclick="base(this)"><a href="#">终端校准</a></li>
						<li onclick="base(this)"><a href="#">参数查询</a></li>
					</ul></li>
			</ul>
			<br>
			<br>
			<br>
			<div id="main" class="dis">
				<br> <br> <br>
				<h3 align="center">
					<b
						style="letter-spacing:12px; font-size:80px; color:#6495ED ;font-weight:bold;font-family:楷体">欢迎！</b>
				</h3>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b
						style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.driverMsg}</b>
				</h1>
				<h2 align="center">驾驶员基础信息管理</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/DriverServlet" Method="post">
								<input type="hidden" name="method" value="queryDriver" /> 查询方式：<select
									name="queryMode" style="width:140px; height:30px">
									<option value="driverName">姓名</option>
									<option value="id">身份证号</option>
									<option value="employId">从业资格证号</option>
									<option value="companyId">所属企业组织机构代码</option>
								</select> &nbsp;&nbsp; <input type="text" name="queryContent"
									style="width:300px; height:30px;" /> &nbsp;&nbsp; <input
									type="submit" value="查询" style="width:60px; height:30px" />
							</form></td>
						<td>
							<button type="button" style="width:120px; height:30px"
								onclick="window.open('/TaxiManagerServer/baseInfo/addDriver.jsp')">添加驾驶员信息</button>
							&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="editDriver()"
								style="width:120px; height:30px">修改所选项</button>&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="deleteDriver()"
								style="width:120px; height:30px">删除所选项</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="driverTable" width="100%" height="10%" border="1"
								cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>姓名</td>
									<td>性别</td>
									<td>身份证号</td>
									<td>从业资格证号</td>
									<td>从业资格证发证机构</td>
									<td>有效日期</td>
									<td>联系电话</td>
									<td>所属企业组织机构代码</td>
									<td>学历</td>
									<td>服务质量信誉考核等级</td>
									<td>IC卡号</td>
									<td>驾驶员照片</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${drivers}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.driverName}</td>
										<td>${row.sex}</td>
										<td>${row.id}</td>
										<td>${row.employId}</td>
										<td>${row.employCardAgent}</td>
										<td>${row.timeBeginEnd}</td>
										<td>${row.tel}</td>
										<td>${row.companyId}</td>
										<td>${row.education}</td>
										<td>${row.serviceGrade}</td>
										<td>${row.ICID}</td>
										<td><button type="button" style="width:120px; height:30px" 
											onclick="window.open('/TaxiManagerServer/ProcessPhotoServlet?method=getPhoto&ICID=${row.ICID}')">显示照片</button>
										</td>
										<td><input type="checkbox" name="driverSelected" />
										</td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.taxiMsg}</b>
				</h1>
				<h2>车辆基础信息管理</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/TaxiServlet" Method="post">
								<input type="hidden" name="method" value="queryTaxi" /> 
							    查询方式：<select name="queryMode" style="width:140px; height:30px">
									<option value="license">车牌号</option>
									<option value="licenseColor">车牌颜色</option>
									<option value="companyId">所属企业组织机构代码</option>
								</select> &nbsp;&nbsp; <input type="text" name="queryContent"
									style="width:300px; height:30px;" /> &nbsp;&nbsp; <input
									type="submit" value="查询" style="width:60px; height:30px" />
							</form></td>
						<td>
							<button type="button" style="width:120px; height:30px"
								onclick="window.open('/TaxiManagerServer/baseInfo/addTaxi.jsp')">添加车辆信息</button>
							&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="editTaxi()"
								style="width:120px; height:30px">修改所选项</button>&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="deleteTaxi()"
								style="width:120px; height:30px">删除所选项</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="taxiTable" width="100%" height="10%" border="1"
								cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车牌号</td>
									<td>所属企业组织机构代码</td>
									<td>ISU标识</td>
									<td>车牌颜色</td>
									<td>行驶证号</td>
									<td>发动机号</td>
									<td>车架号</td>
									<td>厂牌型号</td>
									<td>燃料类型</td>
									<td>购车日期</td>
									<td>道路运输证号</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${taxi}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.license}</td>
										<td>${row.companyId}</td>
										<td>${row.ISUFlag}</td>
										<td>${row.licenseColor}</td>
										<td>${row.runLicense}</td>
										<td>${row.engineId}</td>
										<td>${row.frameId}</td>
										<td>${row.factoryId}</td>
										<td>${row.fuel}</td>
										<td>${row.buyDate}</td>
										<td>${row.transportId}</td>
										<td><input type="checkbox" name="taxiSelected" />
										</td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.companyMsg}</b>
				</h1>
				<h2>企业基础信息管理</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/CompanyServlet" Method="post">
								<input type="hidden" name="method" value="queryCompany" /> 查询方式：<select
									name="queryMode" style="width:140px; height:30px">
									<option value="companyName">企业名称</option>
									<option value="principal">负责人</option>
									<option value="companyId">企业组织机构代码</option>
								</select> &nbsp;&nbsp; <input type="text" name="queryContent"
									style="width:300px; height:30px;" /> &nbsp;&nbsp; <input
									type="submit" value="查询" style="width:60px; height:30px" />
							</form></td>
						<td>
							<button type="button" style="width:120px; height:30px"
								onclick="window.open('/TaxiManagerServer/baseInfo/addCompany.jsp')">添加企业信息</button>
							&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="editCompany()"
								style="width:120px; height:30px">修改所选项</button>&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="deleteCompany()"
								style="width:120px; height:30px">删除所选项</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="companyTable" width="100%" height="10%" border="1"
								cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>企业代码</td>
									<td>所在行政区代码</td>
									<td>企业名称</td>
									<td>负责人</td>
									<td>法人代表</td>
									<td>经济类型</td>
									<td>营业执照 </td>
									<td>经营许可证 </td>
									<td>经营范围 </td>
									<td>详细地址 </td>
									<td>联系电话 </td>
									<td>业务资质 </td>
									<td>服务质量信誉等级 </td>
									<td>注册资金 </td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${company}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.companyId}</td>
										<td>${row.areaId}</td>
										<td>${row.companyName}</td>
										<td>${row.principal}</td>
										<td>${row.corporateRepre}</td>
										<td>${row.economicType}</td>
										<td>${row.businessLicenseId}</td>
										<td>${row.tradePermission}</td>
										<td>${row.businessScope}</td>
										<td>${row.address}</td>
										<td>${row.companyTel}</td>
										<td>${row.businessQualification}</td>
										<td>${row.serviceQuality}</td>
										<td>${row.registeredFund}</td>
										<td><input type="checkbox" name="companySelected" />
										</td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.fareMsg}</b>
				</h1>
				<h2>运价基础信息管理</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/FareServlet" Method="post">
								<input type="hidden" name="method" value="queryFare" /> 
						    查询方式：<select name="queryMode" style="width:160px; height:30px">
										<option value="all">全部</option>
										<option value="areaId">运价执行行政区划代码</option>
										<option value="fareFrom">运价有效期起</option>
								</select> &nbsp;&nbsp; 
								<input type="text" name="queryContent" style="width:300px; height:30px;" /> &nbsp;&nbsp; 
								<input type="submit" value="查询" style="width:60px; height:30px" />
							</form>
						</td>
						<td>
							<button type="button" style="width:120px; height:30px" 
									onclick="window.open('/TaxiManagerServer/baseInfo/addFare.jsp')">添加运价信息</button>
							&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="editFare()" style="width:120px; height:30px">修改所选项</button>
							&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="deleteFare()" style="width:120px; height:30px">删除所选项</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="fareTable" width="100%" height="10%" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>运价执行行政区划代码</td>
									<td>运价类型</td>
									<td>运价有效期起</td>
									<td>昼间起步价(元)</td>
									<td>夜间起步价(元)</td>
									<td>候时单价(元/分钟)</td>
									<td>起步里程 (km)</td>
									<td>昼间单价(元/km)</td>
									<td>昼间单程加价单价(元/km)</td>
									<td>夜间单价(元)</td>
									<td>夜间单程加价单价(元/km)</td>
									<td>单程加价公里 (km)</td>
									<td>夜间时间起 </td>
									<td>夜间时间止 </td>
									<td>低速等候设置 </td>
									<td>切换速度 (km/h)</td>
									<td>运价备注说明 </td>
									<td>运价状态代码(0或1)</td>
									<td>燃油附加费(元)</td>
									<td>K值</td>
									<td>最后更新时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${fare}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.areaId}</td>
										<td>${row.fareType}</td>
										<td>${row.fareFrom}</td>
										<td>${row.fareDayDown}</td>
										<td>${row.fareNightDown}</td>
										<td>${row.fareWait}</td>
										<td>${row.downDistance}</td>
										<td>${row.priceDay}</td>
										<td>${row.priceDayAdd}</td>
										<td>${row.priceNight}</td>
										<td>${row.priceNightAdd}</td>
										<td>${row.addKiloPerTrip}</td>
										<td>${row.nigthFrom}</td>
										<td>${row.nightEnd}</td>
										<td>${row.lowSpeedWait}</td>
										<td>${row.switchSpeed}</td>
										<td>${row.fareRemark}</td>
										<td>${row.fareStatusCode}</td>
										<td>${row.fuelSurcharge}</td>
										<td>${row.k}</td>
										<td>${row.updateTime}</td>
										<td><input type="checkbox" name="fareSelected" />
										</td>
									</tr>
								</c:forEach>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.locationRunStateMsg}</b>
				</h1>
				<h2>汽车卫星定位与运行状态</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/LocationRunStateServlet"
								Method="post" name="locationRunStateForm">
								<input type="hidden" name="method" value="queryTaxi" />
								<input type="hidden" name="pageLocation" value="5" /> 
						    查询车辆：<select name="queryMode" style="width:140px; height:30px">
									<option value="all">全部</option>
									<option value="companyName">公司</option>
									<option value="license">车牌号</option>
								</select> &nbsp;&nbsp; 
								<input type="text" name="queryContent" value="${requestScope.queryContent }"
									style="width:300px; height:30px;" /> &nbsp;&nbsp; 
								<input type="submit" value="查询" name="control" style="width:60px; height:30px" />
							</form>
						</td>
						<td>
							<form action="/TaxiManagerServer/LocationRunStateServlet" id="queryLocationRunStateDetailForm" Method="post">
								<input type="hidden" name="method" value="queryDetail" /> 
								<input type="hidden" name="pageLocation" value="5" /> 
							</form>
							<button type="button" style="width:180px; height:30px" onclick="queryDetail()">查看车辆位置状态详细信息</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="locationRunStateTable" width="100%" align="center"
								border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车辆所属企业组织机构</td>
									<td>车牌号</td>
									<td>驾驶员姓名</td>
									<td>车辆状态</td>
									<td>是否在线</td>
									<td>最后汇报时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${locationRunState}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.companyName }</td>
										<td>${row.license }</td>
										<td>${row.driverName }</td>
										<td>${row.runState }</td>
										<td>${row.isOnline }</td>
										<td>${row.reportTime }</td>
										<td><input type="checkbox" name="locationRunStateSelected" /></td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h2>汽车应急指挥管理信息</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h2>汽车稽查管理信息</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.operationQueryMsg}</b>
				</h1>
				<h2>汽车营运信息查询</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/OperationQueryServlet" Method="post">
								<input type="hidden" name="method" value="queryOperation" /> 
								<input type="hidden" name="pageLocation" value="8" /> 
								查询车辆：<select name="queryMode" style="width:140px; height:30px">
									<option value="all">全部</option>
									<option value="companyName">公司</option>
									<option value="license">车牌号</option>
									<option value="license">驾驶员</option>
								</select> &nbsp;&nbsp; 
									<input type="text" name="queryContent" value="${requestScope.queryContent }" style="width:300px; height:30px;" /> &nbsp;&nbsp; 
									<input type="submit" value="查询" name="control" style="width:60px; height:30px" />
							</form>
						</td>
						<td><button type="button" style="width:100px; height:30px" onclick="queryCheatMonitor()">查询作弊信息</button></td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table  id="transactionTable" width="100%" align="center" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车牌号</td>
									<td>企业</td>
									<td>驾驶员</td>
									<td>上车时间</td>
									<td>下车时间</td>
									<td>用时<br/>(时：分)</td>
									<td>里程<br/>(千米)</td>
									<td>空驶里程<br/>(千米)</td>
									<td>单价<br/>(元/千米)</td>
									<td>金额<br/>(元)</td>
									<td>状态</td>
									<td>燃油附加费<br/>(元)</td>
									<td>等待计时<br/>(时：分)</td>
									<td>交易类型</td>
									<td>当前车次</td>
									<td>评价</td>
									<td>GPS计算里程<br/>(千米)</td>
									<td>误差</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<% List<OperationMsg> oM = (List<OperationMsg>) request.getAttribute("operationMsgs");
									if(oM != null) {
										for(OperationMsg o : oM) { 
											String ic = o.getIsCheat();
											String[] r = new String[2];
											if (!ic.equals("无记录")) {
												r = ic.split(" ");
											}
										%>
										<tr>
											<td><%=o.getLicense()%></td>
											<td><%=o.getCompanyName() %></td>
											<td><%=o.getDriverName() %></td>
											<td><%=o.getGetOnTime() %></td>
											<td><%=o.getGetOffTime() %></td>
											<td><%=o.getSpendTime() %></td>
											<td><%=o.getDistance() %></td>
											<td><%=o.getNullRun() %></td>
											<td><%=o.getPrice() %></td>
											<td><%=o.getMoney() %></td>
											<td><%=o.getJourney() %></td>
											<td><%=o.getFuelSurcharge() %></td>
											<td><%=o.getWaitTime() %></td>
											<td><%=o.getTransitionType() %></td>
											<td><%=o.getCurrentRunTime() %></td>
											<td><%=o.getEvaluate() %></td>
											<td><%=r[1] %></td>
											<td><%=r[0] %></td>
											<td><input type="checkbox" name="transactionSelected" /></td>
										</tr>
								<%} }%> 
<%-- 								<c:forEach items="${operationMsgs}" var="row">
									<tr>
										<td>${row.license }</td>
										<td>${row.companyName }</td>
										<td>${row.driverName }</td>
										<td>${row.getOnTime }</td>
										<td>${row.getOffTime }</td>
										<td>${row.spendTime }</td>
										<td>${row.distance }</td>
										<td>${row.nullRun }</td>
										<td>${row.price }</td>
										<td>${row.money }</td>
										<td>${row.journey }</td>
										<td>${row.fuelSurcharge }</td>
										<td>${row.waitTime }</td>
										<td>${row.transitionType }</td>
										<td>${row.currentRunTime }</td>
										<td>${row.evaluate }</td>
										<td>${row.isCheat }</td>
										<td><input type="checkbox" name="transactionSelected" /></td>
									</tr>
								</c:forEach> --%>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h2>车辆定位监控</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.traceReplayMsg}</b>
				</h1>
				<h2>车辆轨迹回放</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/LocationRunStateServlet" Method="post" name="locationRunStateForm">
								<input type="hidden" name="method" value="queryTaxi" />
								<input type="hidden" name="pageLocation" value="10" /> 
						    查询车辆：<select name="queryMode" style="width:140px; height:30px">
									<option value="all">全部</option>
									<option value="companyName">公司</option>
									<option value="license">车牌号</option>
								</select> &nbsp;&nbsp; 
								<input type="text" name="queryContent" value="${requestScope.queryContent }" style="width:300px; height:30px;" /> &nbsp;&nbsp; 
								<input type="submit" value="查询" name="control" style="width:60px; height:30px" />
							</form>
						</td>
					</tr>
					<tr height="60">
						<td width="100%">
							<form action="/TaxiManagerServer/LocationRunStateServlet" id="traceReplayForm" Method="post" name="traceReplayFormName">
								<input type="hidden" name="method" value="traceReplay" /> 
								<input type="hidden" name="pageLocation" value="10" /> 
						    开始时间：<input type="text" name="traceTimeBegin" style="width: 150px; height:30px;" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="2017-03-01 12:13:12"/>&nbsp;&nbsp;&nbsp;&nbsp;   
						    结束时间：<input type="text" name="traceTimeEnd" style="width: 150px; height:30px;" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="2017-03-01 12:13:12"/>
							</form>
						</td>
						<td>
							<button type="button" style="width:130px; height:30px" onclick="traceReplay()">查看车辆历史轨迹</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="locationRunStateTable" width="100%" align="center"
								border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车辆所属企业组织机构</td>
									<td>车牌号</td>
									<td>驾驶员姓名</td>
									<td>车辆状态</td>
									<td>是否在线</td>
									<td>最后汇报时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${locationRunState}" var="row">
									<tr>
										<td>${row.companyName }</td>
										<td>${row.license }</td>
										<td>${row.driverName }</td>
										<td>${row.runState }</td>
										<td>${row.isOnline }</td>
										<td>${row.reportTime }</td>
										<td><input type="checkbox" name="traceReplaySelected" /></td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b
						style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.remoteManageMsg}</b>
				</h1>
				<h2>车辆远程管理</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/RemoteManageServlet" Method="post" name="remoteManageQueryForm">
								<input type="hidden" name="method" value="queryTaxi" /> 
								<input type="hidden" name="sendType" value="part" />
								<!-- 位置不对需要修改 -->
								<input type="hidden" name="pageLocation" value="11" /> 
						    查询车辆：<select name="queryMode" style="width:140px; height:30px">
									<option value="all">全部</option>
									<option value="companyName">公司</option>
									<option value="license">车牌号</option>
								</select> &nbsp;&nbsp; 
								<input type="text" name="queryContent" value="${requestScope.queryContent }" style="width:300px; height:30px;" /> &nbsp;&nbsp; 
								<input type="submit" value="查询" name="control" style="width:60px; height:30px" />
							</form>
						</td>
						<td>
							<form action="/TaxiManagerServer/RemoteManageServlet" id="remoteManageForm" Method="post">
								<input type="hidden" name="method" value="sendCommand" /> 
								<!-- <input type="hidden" name="msgID" value="108" /> -->
							</form>
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('part', '108')">控制选中车辆拍照</button>&nbsp;&nbsp;
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('all', '108')">控制全部车辆拍照</button>
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('part', '115')">查询选中车辆参数</button>&nbsp;&nbsp;
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('all', '115')">查询全部车辆参数</button>
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('part', '117')">校准选中车辆</button>&nbsp;&nbsp;
							<button type="button" style="width:120px; height:30px" onclick="remoteManage('all', '117')">校准全部车辆</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="remoteManageTaxiTable" width="100%" align="center"
								border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车牌号</td>
									<td>所在企业</td>
									<td>驾驶员姓名</td>
									<td>车辆状态</td>
									<td>是否在线</td>
									<td>最后汇报时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${taxiManage}" var="row">
									<tr>
										<%-- <c:forEach items="${row}" var="e">
											<td>${e}</td>
										</c:forEach> --%>
										<td>${row.license }</td>
										<td>${row.companyName }</td>
										<td>${row.driverName }</td>
										<td>${row.runState }</td>
										<td>${row.isOnline }</td>
										<td>${row.reportTime }</td>
										<td><input type="checkbox" name="remoteManageTaxi" /></td>
									</tr>
								</c:forEach>
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h2>车辆报警提醒</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h2>应急指挥车辆</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h2>车辆投诉管理</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h2>综合运行分析</h2>
				<hr width="100%" align="center">
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b
						style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.sendMsgMsg}</b>
				</h1>
				<h2>消息发布</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="60">
						<td width="460">
							<form action="/TaxiManagerServer/RemoteManageServlet"
								Method="post">
								<input type="hidden" name="method" value="queryTaxi" /> <input
									type="hidden" name="pageLocation" value="16" /> 查询车辆：<select
									name="queryMode" style="width:140px; height:30px">
									<option value="all">全部</option>
									<option value="companyName">车辆所属公司</option>
									<option value="license">车牌号</option>
								</select> &nbsp;&nbsp; <input type="text" name="queryContent"
									style="width:300px; height:30px;" /> &nbsp;&nbsp; <input
									type="submit" value="查询" style="width:60px; height:30px" />
							</form></td>
						<td align="center"><button type="button"
								style="width:120px; height:30px"
								onclick="window.open('/TaxiManagerServer/RemoteManageServlet?method=queryHistoryMsg')">查看历史信息</button>
						</td>
					</tr>
					<tr height="120">
						<td>
							<form id="messageForm" action="/TaxiManagerServer/RemoteManageServlet" Method="post">
								<input type="hidden" name="method" value="sendMsg" />
								<textarea id="messageArea" name="message" rows="5" cols="120%px">请在此输入要发送的信息</textarea>
							</form>
						</td>
						<td align="center">
							<button type="button" style="width:120px; height:30px"
								onclick="sendMSg('all',16)">发送给全体</button> <br> <br>
							<button type="button" style="width:120px; height:30px"
								onclick="sendMSg('part',16)">发送给选中</button></td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="allLicenseTable" width="100%" align="center" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>车牌号</td>
									<td>所在企业</td>
									<td>驾驶员姓名</td>
									<td>车辆当前状态</td>
									<td>是否在线</td>
									<td>最后汇报时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
								<c:forEach items="${taxiManage}" var="row">
									<tr>
										<td>${row.license }</td>
										<td>${row.companyName }</td>
										<td>${row.driverName }</td>
										<td>${row.runState }</td>
										<td>${row.isOnline }</td>
										<td>${row.reportTime }</td>
										<td><input type="checkbox" name="sendMsgSelected" /></td>
									</tr>
								</c:forEach>
							</table>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.verifyAdjustMsg}</b>
				</h1>
				<h2>标准路线</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/VerifyAdjustServlet" Method="post">
								<input type="hidden" name="method" value="queryStandardRoute" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="submit" value="查看全部路线" style="width:120px; height:30px" />
							</form>
						</td>
						<td>
							&nbsp;&nbsp;&nbsp;
							<button type="button" style="width:120px; height:30px" onclick="window.open('/TaxiManagerServer/baseInfo/addStandardRoute.jsp')">添加标准路线信息</button>
							<button type="button" onclick="editStandardRoute()" style="width:120px; height:30px">修改所选项</button>&nbsp;&nbsp;&nbsp;
							<button type="button" onclick="deleteStandardRoute()" style="width:120px; height:30px">删除所选项</button>&nbsp;&nbsp;&nbsp;
							<!-- <button type="button" onclick="sendToISU()" style="width:120px; height:30px">发送到全部终端</button> -->
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="StandardRouteTable" width="100%" height="10%" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr>
									<td>编号</td>
									<td>A点经度</td>
									<td>A点纬度</td>
									<td>A点高度</td>
									<td>A点半径(单位：米)</td>
									<td>B点经度</td>
									<td>B点纬度</td>
									<td>B点高度</td>
									<td>B点半径(单位：米)</td>
									<td>AB测量距离(单位：米)</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
 								<%	List<StandardRoute> standardRoutes = (List<StandardRoute>) request.getAttribute("standardRoutes");
									if(standardRoutes != null) {
										for(StandardRoute sRoute : standardRoutes) { %>
										<tr>
											<td><%=sRoute.getNum()%></td>
											<td><%=sRoute.getAlongitude() %></td>
											<td><%=sRoute.getAlatitude() %></td>
											<td><%=sRoute.getAaltitude() %></td>
											<td><%=sRoute.getAradius() %></td>
											<td><%=sRoute.getBlongtitde() %></td>
											<td><%=sRoute.getBlatitude() %></td>
											<td><%=sRoute.getBaltitude() %></td>
											<td><%=sRoute.getBradius() %></td>
											<td><%=sRoute.getSmeasure() %></td>
											<td><input type="checkbox" name="standardRouteSelected" /></td>
										</tr>
								<%} }%> 
								
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.carMeasureMsg}</b>
				</h1>
				<h2>终端校准</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="600"></td>
						<td width="600">
							<form action="/TaxiManagerServer/VerifyAdjustServlet" Method="post">
								<input type="hidden" name="method" value="queryCarMeasure" />
								<input type="submit" value="查看终端测量结果" style="width:120px; height:30px" />
							</form>
						</td>
						<td>
							<button type="button" onclick="queryVerifyTrace()" style="width:120px; height:30px">查看位置回放</button>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign=top>
							<table id="carMeasureTable" width="100%" height="10%" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr>
									<td>ISU标识</td>
									<td>车牌号</td>
									<td>A点经度</td>
									<td>A点纬度</td>
									<td>A点高度</td>
									<td>A点速度</td>
									<td>B点经度</td>
									<td>B点纬度</td>
									<td>B点高度</td>
									<td>B点速度</td>
									<td>标准距离S</td>
									<td>计价器测量距离L</td>
									<td>AB实地测量距离</td>
									<td>GPS测量距离</td>
									<td>速度累加</td>
									<td>误差百分比</td>
									<td>校准时间</td>
									<td>选择</td>
								</tr>
								<!-- 数据 -->
 								<% List<CarMeasure> carMeasures = (List<CarMeasure>) request.getAttribute("carMeasures");
									if(carMeasures != null) {
										for(CarMeasure cMeasure : carMeasures) { 
											String[] r = cMeasure.getIsRegular().split(" ");
										%>
										<tr>
											<td><%=cMeasure.getISUFlag()%></td>
											<td><%=cMeasure.getLicense() %></td>
											<td><%=cMeasure.getAlongitude() %></td>
											<td><%=cMeasure.getAlatitude() %></td>
											<td><%=cMeasure.getAaltitude() %></td>
											<td><%=cMeasure.getAspeed() %></td>
											<td><%=cMeasure.getBlongtitde() %></td>
											<td><%=cMeasure.getBlatitude() %></td>
											<td><%=cMeasure.getBaltitude() %></td>
											<td><%=cMeasure.getBspeed() %></td>
											<td><%=cMeasure.getScarMeasure() %></td>
											<td><%=r[0] %></td>
											<td><%=r[1] %></td>
											<td><%=r[3] %></td>
											<td><%=r[4] %></td>
											<td><%=r[2] %></td>
											<td><%=cMeasure.getVerifyTime() %></td>
											<td><input type="checkbox" name="carMeasureSelected" /></td>
										</tr>
								<%} }%> 
							</table></td>
					</tr>
				</table>
			</div>
			<div id="main" class="udis">
				<h1 align="center">
					<b style="letter-spacing:12px; font-size:30px; color: red; font-weight:bold;font-family:楷体">${requestScope.parameterMsg}</b>
				</h1>
				<h2>参数查询</h2>
				<hr width="100%" align="center">
				<table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
					<tr height="80">
						<td width="660">
							<form action="/TaxiManagerServer/ParameterQueryServlet" Method="post">
								<input type="hidden" name="method" value="queryParameter" /> 
						    查询方式：<select name="queryMode" style="width:160px; height:30px">
										<option value="all">全部</option>
										<option value="license">运价执行行政区划代码</option>
								</select> &nbsp;&nbsp; 
								<input type="text" name="queryContent" style="width:300px; height:30px;" /> &nbsp;&nbsp; 
								<input type="submit" value="查询" style="width:60px; height:30px" />
							</form>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign=top><br>
							<table id="fareTable" width="100%" height="10%" border="1" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
								<tr height="30">
									<td>ISU</td>
									<td>车牌号</td>
									<td>运价有效期起</td>
									<td>昼间起步价(元)</td>
									<td>夜间起步价(元)</td>
									<td>候时单价(元/分钟)</td>
									<td>昼间单价(元/km)</td>
									<td>夜间单价(元/km)</td>
									<td>夜间时间起 </td>
									<td>夜间时间止 </td>
									<td>计价器时间</td>
									<td>计价器K值</td>
									<td>查询时间</td>
								</tr>
								<% List<ParameterQuery> parameterQueries = (List<ParameterQuery>) request.getAttribute("parameterQueries");
									if(parameterQueries != null) {
										for(ParameterQuery p : parameterQueries) { 
											Fare f = p.getFare();%>
										<tr>
											<td><%=p.getISUFlag() %></td>
											<td><%=p.getLicense() %></td>
											<td><%=f.getFareFrom() %></td>
											<td><%=f.getFareDayDown() %></td>
											<td><%=f.getFareNightDown() %></td>
											<td><%=f.getFareWait() %></td>
											<td><%=f.getPriceDay() %></td>
											<td><%=f.getPriceNight() %></td>
											<td><%=f.getNigthFrom() %></td>
											<td><%=f.getNightEnd() %></td>
											<td><%=p.getTaximeterTime() %></td>
											<td><%=f.getK() %></td>
											<td><%=p.getQueryTime() %></td>
										</tr>
								<%} }%>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>
