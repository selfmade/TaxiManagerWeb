<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>车辆位置及状态详细信息</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<style type="text/css">
 		body {
			text-align: center;
			align: center;
		}

		table td {
			text-align: center;
		}

		table tr {
			text-align: center;
		}
		#allmap {width: 100%; height: 100%; overflow:hidden; margin:0; font-family:"微软雅黑";}
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=XNdzRPvb86yzceu2kO34MCoBUbXEr4dF">
	</script>
	
	<script type="text/javascript">
		function test(){
			var latitude = <%=request.getAttribute("latitude")%>;
/* 			var longitude = parseFloat(locationRunState.longitude);*/
			//var latitude = parseFloat(locationRunState);
			alert(latitude+1);
		}
	</script>
  </head>
  
  <body>
  	<h1>车辆位置及状态详细信息</h1>
	<hr width="1200" align="center">
<table width="1100px" align="center" border="0">
	<tr><td><h3>基本信息</h3></td></tr>
	<tr><td><table  width="1100px" align="center" border="1" cellspacing="0" id="locationRunStateTable" cellpadding="0" bordercolor="#0099cc" border="0">
		<tr>
			<td>车辆所属企业组织机构</td>
			<td>车牌号</td>
			<td>驾驶员姓名</td>
			<td>运行状态</td>
			<td>在线状态</td>
		</tr>
		<tr>
			<td>${requestScope.locationRunState.companyName}</td>
			<td>${requestScope.locationRunState.license}</td>
			<td>${requestScope.locationRunState.driverName}</td>
			<td>${requestScope.locationRunState.runState}</td>
			<td>${requestScope.locationRunState.isOnline}</td>
		</tr>
	</table></td></tr>
	<tr><td><br><h3>车辆卫星定位数据</h3><br></td></tr>
	<tr><td><table width="1100px" align="center" border="1" cellspacing="0" id="locationRunStateTable" cellpadding="0" bordercolor="#0099cc">
		<tr>
			<td>定位时间</td>
			<td>经度</td>
			<td>纬度</td>
			<td>方向</td>
		</tr>
		<tr>
			<td>${requestScope.locationRunState.locationTime}</td>
			<td>${requestScope.locationRunState.longitude}</td>
			<td>${requestScope.locationRunState.latitude}</td>
			<td>${requestScope.locationRunState.direction}</td>
		</tr>
	</table></td></tr>
	<tr><td><br><h3>车辆当前位置</h3></td></tr>
	<tr><td width="600px" height="400" align="center"><br><div id="allmap"></div></td></tr>
	</table>

<script type="text/javascript">
	// 百度地图API功能，获取经纬度点展示在地图上
	var map = new BMap.Map("allmap");
	var point = new BMap.Point(<%=request.getAttribute("longitude")%>, <%=request.getAttribute("latitude")%>);
	map.centerAndZoom(point, 15);
	var marker = new BMap.Marker(point);  // 创建标注
	map.addOverlay(marker);               // 将标注添加到地图中
	map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	var bottom_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
	var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
	map.addControl(bottom_left_control);
	map.addControl(top_left_navigation);
	marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
</script>
</body>
</html>
