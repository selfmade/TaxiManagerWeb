<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>指令执行情况</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<style type="text/css">
body {
	text-align: center;
	align: center;
}

table td {
	text-align: center;
}

table tr {
	text-align: center;
}
</style>
</head>

<body>
	<h1>指令执行情况</h1>
	<hr width="1200" align="center">
	<table width="1100px" align="center" border="1" cellspacing="0"
		id="msgTable" cellpadding="0" bordercolor="#0099cc">
		<tr>
			<td width="70px">信息类型</td>
			<td>未执行指令的车辆</td>
		</tr>

		<c:forEach var="row" items="${noExecute}">
			<tr>
				<td>${row.key}</td>
				<td><c:forEach var="l" items="${row.value}">
						${l}
					</c:forEach>
				</td>
			</tr>
		</c:forEach>

	</table>

</body>
</html>
