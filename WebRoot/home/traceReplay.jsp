<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import= "com.bdy.lm.browser.domain.LocationTracks"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>历史轨迹查询</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<style type="text/css">
 		body {
			text-align: center;
			align: center;
		}

		table td {
			text-align: center;
		}

		table tr {
			text-align: center;
		}
		#allmap {width: 1200; height: 800; overflow:hidden; margin:0 auto; font-family:"微软雅黑"; text-align:center;}
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=XNdzRPvb86yzceu2kO34MCoBUbXEr4dF">
	</script>

  </head>
  
<body>
  	<h1>车辆历史轨迹查看</h1>
	<hr width="1200" align="center">
 	<div id="allmap"></div>
	<script type="text/javascript">
		var map = new BMap.Map("allmap");          // 创建地图实例  
		var ps = new Array();
		var points = new Array();
		var po = "坐标点：\n";
		var convertor = new BMap.Convertor();
		var l = 0, i = 0;
		<% List<LocationTracks> list = (List<LocationTracks>) request.getAttribute("traceReplayPoint");%>
		   l = <%=list.size() %>;
		   console.log('list.size', l);
		<% for(int i = 0; i < list.size(); i++) { //list.size()
		   		LocationTracks l = (LocationTracks) list.get(i);
		%>
  				ps[<%=i %>]=new BMap.Point(<%=l.getLongitude()%>, <%=l.getLatitude() %>);// % 5
  				po = po +  "(" + <%=l.getLongitude()%> + ", " + <%=l.getLatitude() %> + ")\n";
		<%	} %>
		
		console.log('ps', ps.length);
		while(ps.length != l){		
			i++;
			console.log('while', i);
		}
		var t = new Array();				
		//console.log('b', t);

		for(i = 0; i < 10; i++){
			//console.log('i', i);
			t = t.concat(ps[i]);
			if(t.length == 10){
				console.log('t', t);
			    setTimeout(function(){
					convertor.translate(t, 1, 5, translateCallback)
				}, 100);
				//t = [];
			}
			//var convertor = new BMap.Convertor();
			//var pointArr = [];
			//pointArr.push(ps[i]);
			//convertor.translate(pointArr, 1, 5, translateCallback);
			
			//	console.log('t.length', t.length);
			//	t = [];
  			
		}	
	//console.log('ttt', ttt);
		setTimeout(function(){
			//convertor.translate(ttt, 1, 5, translateCallback)
		}, 100);		
		//console.log('f', t);
		//坐标转换完之后的回调函数
    	translateCallback = function (data){
      		//alert("data.status = " + data.status);
			//alert(data.points.length);
			console.log('status', data.status);
      		if(data.status == 0) {
				points = points.concat(data.points);
				console.log('points', points);
				t = [];
				for(var v = points.length; v < (points.length + 10) && v < l; v++){
					t = t.concat(ps[v]);
				}
			    setTimeout(function(){
					convertor.translate(t, 1, 5, translateCallback)
				}, 100);
      		}
      		if(points.length == l){
			console.log('points == l', points.length);
      	if(l > 0){
			map.centerAndZoom(points[parseInt(l/2)], 15);	// 初始化地图，设置中心点坐标和地图级别
			map.addControl(new BMap.MapTypeControl());	//添加地图类型控件
		} else {
			var point = new BMap.Point(119.302605, 26.0803);
			map.centerAndZoom(point, 15);	// 初始化地图，设置中心点坐标和地图级别
			map.addControl(new BMap.MapTypeControl());	//添加地图类型控件
			//alert("您所查询的内容不存在！！");
		}
		map.setCurrentCity("福州");          			//设置地图显示的城市 此项是必须设置的
		map.enableScrollWheelZoom(true);     		//开启鼠标滚轮缩放
		var bottom_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
		var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
		map.addControl(bottom_left_control);
		map.addControl(top_left_navigation);

		var polyline = new BMap.Polyline(points, {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});   //创建折线
		map.addOverlay(polyline);   //增加折线
		
		var start = new BMap.Marker(points[0]);  // 创建标注
		var end = new BMap.Marker(points[points.length-1]);
		map.addOverlay(start);              // 将标注添加到地图中
		map.addOverlay(end);
		var startLabel = new BMap.Label("起点",{offset:new BMap.Size(20,-10)});
		var endLabel = new BMap.Label("终点",{offset:new BMap.Size(20,-10)});
		start.setLabel(startLabel);
		end.setLabel(endLabel);
      		}
    	}
		//alert(po);

	</script>
  </body>
</html>
