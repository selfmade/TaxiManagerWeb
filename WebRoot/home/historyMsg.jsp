<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>历史信息</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
body {
	text-align: center;
	align: center;
}

table td {
	text-align: center;
}

table tr {
	text-align: center;
}
</style>
<script type="text/javascript">
	function queryExecute() {
		var messageForm = document.getElementById("messageForm");
		
		var select = document.getElementsByName("msgSelected");
		var tableId = document.getElementById("msgTable");
		var index = 0;
		for (i = 0; i < select.length; i++) {
			if (select[i].checked == true) {
				var msg = document.createElement("input"); //创建input节点
				msg.name = index;
				msg.value = tableId.rows[i + 1].cells[1].innerHTML;
				msg.type = "hidden";
				messageForm.appendChild(msg);
				index++;
			}
		}
		if (index == 0) {
			alert("请先选择要查看的消息！");
		} else {
			messageForm.submit();
		}
	}
</script>

</head>

<body>
	<form id="messageForm" action="/TaxiManagerServer/RemoteManageServlet"
		Method="post">
		<input type="hidden" name="method" value="queryExecute" />
	</form>
	<h1>历史信息</h1>
	<hr width="1200" align="center">
	<table width="1100px" align="center" border="1" cellspacing="0"
		id="msgTable" cellpadding="0" bordercolor="#0099cc">
		<tr>
			<td width="65px">信息类型</td>
			<td>信息内容</td>
			<td width="80px">发布时间</td>
			<td width="120px"><button type="button"
					style="width:110px; height:30px" onclick="queryExecute()">查看执行情况</button>
			</td>
		</tr>
		<c:forEach items="${requestScope.historyMsg}" var="row">
			<tr>
				<td>${row.content}</td>
				<td>${row.msg}</td>
				<td>${row.publishTime}</td>
				<td><input type="checkbox" name="msgSelected" style="zoom:200%;" />选中</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
