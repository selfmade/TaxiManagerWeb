<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>修改企业信息</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<script type="text/javascript">
	function check() {
		 if(document.companyForm.companyId.value=="") {
    		alert('请输入企业组织机构代码 ！');
 			return false;
  		} else if(document.companyForm.companyName.value=="") {
    		alert('请输入企业名称！');
 			return false;
  		} else if(document.companyForm.areaId.value=="") {
    		alert('请输入企业所在行政区划代码！');
 			return false;
  		} else if(document.companyForm.principal.value==""){
 			alert('请输入负责人姓名！');
 			return false;
  		} else if(document.companyForm.corporateRepre.value==""){
    		alert('请选输入法人代表！');
 			return false;
  		} else if(document.companyForm.economicType.value==""){
    		alert('请输入经济类型！');
 			return false;
  		} else if(document.companyForm.businessLicenseId.value==""){
    		alert('请输入营业执照号！');
 			return false;
  		} else if(document.companyForm.tradePermission.value==""){
    		alert('请输入经营许可证号！');
 			return false;
  		} else if(document.companyForm.businessScope.value==""){
    		alert('请输入经营范围！');
 			return false;
  		} else if(document.companyForm.address.value==""){
    		alert('请输入详细地址！');
 			return false;
  		} else if(document.companyForm.companyTel.value==""){
    		alert('请输入联系电话！');
 			return false;
  		} else if(document.companyForm.businessQualification.value==""){
    		alert('请输入业务资质！');
 			return false;
  		} else if(document.companyForm.serviceQuality.value==""){
    		alert('请输入服务质量信誉考核等级！');
 			return false;
  		} else if(document.companyForm.registeredFund.value==""){
    		alert('请输入注册资金！');
 			return false;
  		}
	}
</script>
</head>

<body>
	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
			<h1 align="center">修改企业信息</h1>
			<hr>
			<form action="/TaxiManagerServer/CompanyServlet" Method="post"
				name="companyForm" onSubmit="return check();">
				<input type="hidden" name="method" value="modifyCompany" />
				<table width="500" height="600" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">

			<tr>
				<td>企业组织机构代码：${form.companyId }<input type="hidden" name=companyId value="${form.companyId } "
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>企业名称：<!-- <select name="sex" style="width:100px; height:30px">
					<option value="男">男</option>0.

					<option value="女">女</option>
			</select> --> <input type="text" name="companyName" value="${form.companyName }"
					style="width:200px; height:25px;" />
				</td>
			</tr>
			<tr>
				<td>企业所在行政区划代码：<input type="text" name="areaId" value="${form.areaId }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>负责人姓名：<input type="text" name="principal" value="${form.principal }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>法人代表：<input type="text" name="corporateRepre" value="${form.corporateRepre }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>经济类型：<input type="text" name="economicType" value="${form.economicType }"
					style="width:250px; height:25px;" /></td>
			</tr>
			<tr>
				<td>营业执照号：<input type="text" name="businessLicenseId" value="${form.businessLicenseId }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>经营许可证号：<input type="text" name="tradePermission" value="${form.tradePermission }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>经营范围：<input type="text" name="businessScope" value="${form.businessScope }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>详细地址：<input type="text" name="address" value="${form.address }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>联系电话：<input type="text" name="companyTel" value="${form.companyTel }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>业务资质：<input type="text" name="businessQualification" value="${form.businessQualification }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>服务质量信誉考核等级：<input type="text" name="serviceQuality" value="${form.serviceQuality }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>注册资金：<input type="text" name="registeredFund" value="${form.registeredFund }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="提交"
					style="width:120px; height:30px" /> <input type="reset" value="清除"
					style="width:120px; height:30px" /></td>
			</tr>
				</table>
			</form>
			<h1 align="center">
				<b
					style="letter-spacing:12px; font-size:40px; color: red; font-weight:bold;font-family:楷体">${requestScope.companyMsg}</b>
			</h1>
		</c:otherwise>
	</c:choose>
</body>
</html>
