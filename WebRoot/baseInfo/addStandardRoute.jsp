<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import= "com.bdy.lm.browser.domain.StandardRoute"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head> 
    <title>添加路线信息</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<script type="text/javascript">
	function check() {
		 if(document.standardRouteForm.Alongitude.value=="") {
    		alert('请输入A点经度 ！');
 			return false;
  		} else if(document.standardRouteForm.Alatitude.value=="") {
    		alert('请输入A点纬度！');
 			return false;
  		} else if(document.standardRouteForm.Aaltitude.value=="") {
    		alert('请输入A点高度！');
 			return false;
  		} else if(document.standardRouteForm.Aradius.value==""){
 			alert('请输入A点半径！');
 			return false;
  		} else if(document.standardRouteForm.Blongtitde.value==""){
    		alert('请选输入B点经度！');
 			return false;
  		} else if(document.standardRouteForm.Blatitude.value==""){
    		alert('请输入B点纬度！');
 			return false;
  		} else if(document.standardRouteForm.Bradius.value==""){
    		alert('请输入B点半径！');
 			return false;
  		} else if(document.standardRouteForm.Smeasure.value==""){
    		alert('请输入AB之间测量距离！');
 			return false;
  		}
	}
</script>
</head>

<body>
	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
	<h1 align="center">添加标准路线信息</h1>
	<hr>
	<form action="/TaxiManagerServer/VerifyAdjustServlet" Method="post" name="standardRouteForm" onSubmit="return check();">
		<input type="hidden" name="method" value="addStandardRoute" />
		<table width="500" height="600" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
			<%	StandardRoute sRoute = (StandardRoute) request.getAttribute("form");
				if(sRoute == null) { 
					sRoute = new StandardRoute();
				}%>
			<tr>
				<td>A点经度：<input type="text" name="Alongitude" value="<%=sRoute.getAlongitude() %>" style="width:200px; height:25px;" />（GPS）（数值较大的）</td>
			</tr>
			<tr>
				<td>A点纬度：<input type="text" name="Alatitude" value="<%=sRoute.getAlatitude() %>" style="width:200px; height:25px;" />（GPS）（数值较小的）</td>
			</tr>
			<tr>
				<td>A点高度：<input type="text" name="Aaltitude" value="<%=sRoute.getAaltitude() %>" style="width:200px; height:25px;" />(单位：米)</td>
			</tr>
			<tr>
				<td>A点半径：<input type="text" name="Aradius" value="<%=sRoute.getAradius() %>" style="width:200px; height:25px;" />(单位：米)</td>
			</tr>
			<tr>
				<td>B点经度：<input type="text" name="Blongtitde" value="<%=sRoute.getBlongtitde() %>" style="width:200px; height:25px;" />（GPS）</td>
			</tr>
			<tr>
				<td>B点纬度：<input type="text" name="Blatitude" value="<%=sRoute.getBlatitude() %>" style="width:200px; height:25px;" />（GPS）</td>
			</tr>
			<tr>
				<td>B点高度：<input type="text" name="Baltitude" value="<%=sRoute.getBaltitude() %>" style="width:200px; height:25px;" />(单位：米)</td>
			</tr>
			<tr>
				<td>B点半径：<input type="text" name="Bradius" value="<%=sRoute.getBradius() %>" style="width:200px; height:25px;" />(单位：米)</td>
			</tr>
			<tr>
				<td>AB测量距离：<input type="text" name="Smeasure" value="<%=sRoute.getSmeasure() %>" style="width:200px; height:25px;" />(单位：米)</td>
			</tr>
			<tr>
				<td><input type="submit" value="提交" style="width:120px; height:30px" /> 
					<input type="reset" value="清除" style="width:120px; height:30px" /></td>
			</tr>
		</table>
	</form>
	<h1 align="center">
		<b
			style="letter-spacing:12px; font-size:40px; color: red; font-weight:bold;font-family:楷体">${requestScope.verifyAdjustMsg}</b>
	</h1>
				</c:otherwise>
	</c:choose>
</body>
</html>
