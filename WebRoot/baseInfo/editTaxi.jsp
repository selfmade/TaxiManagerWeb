<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>修改出租车辆信息</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript">
	function check() {
		 if(document.taxiForm.license.value=="") {
    		alert('请输入车牌号！');
 			return false;
  		} else if(document.taxiForm.companyId.value=="") {
    		alert('请输入所属企业组织机构代码！');
 			return false;
  		} else if(document.taxiForm.ISUFlag.value==""){
    		alert('请输入ISU标识！');
 			return false;
  		} else if(document.taxiForm.licenseColor.value=="") {
    		alert('请输入车牌颜色！');
 			return false;
  		} else if(document.taxiForm.runLicense.value==""){
 			alert('请输入行驶证号！');
 			return false;
  		} else if(document.taxiForm.engineId.value==""){
    		alert('请选输入发动机号！');
 			return false;
  		} else if(document.taxiForm.frameId.value==""){
    		alert('请输入车架号！');
 			return false;
  		} else if(document.taxiForm.factoryId.value==""){
    		alert('请输入厂牌型号！');
 			return false;
  		} else if(document.taxiForm.fuel.value==""){
    		alert('请输入燃料类型！');
 			return false;
  		} else if(document.taxiForm.buyDate.value==""){
    		alert('请输入购车日期！');
 			return false;
  		} else if(document.taxiForm.transportId.value==""){
    		alert('请输入道路运输证号！');
 			return false;
  		}
	}
</script>
</head>

<body>
	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
			<h1 align="center">修改出租车辆信息</h1>
			<hr>
			<form action="/TaxiManagerServer/TaxiServlet" Method="post"
				name="taxiForm" onSubmit="return check();">
				<input type="hidden" name="method" value="modifyTaxi" />
				<table width="500" height="600" align="center" border="0"
					cellspacing="0" cellpadding="0" bordercolor="#0099cc">

					<tr>
						<td>车牌号：${form.license }<input type="hidden" name="license"
							value="${form.license }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td>所属企业组织机构代码：<!-- <select name="sex" style="width:100px; height:30px">
					<option value="男">男</option>0.

					<option value="女">女</option>
			</select> --> <input type="text" name="companyId"
							value="${form.companyId }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td>ISU标识：<input type="text" name="ISUFlag"
							value="${form.ISUFlag }" style="width:200px; height:25px;" /></td>
					</tr>
					<tr>
						<td>车牌颜色：<input type="text" name="licenseColor"
							value="${form.licenseColor }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td>行驶证号：<input type="text" name="runLicense"
							value="${form.runLicense }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td>发动机号：<input type="text" name="engineId"
							value="${form.engineId }" style="width:250px; height:25px;" /></td>
					</tr>
					<tr>
						<td>车架号：<input type="text" name="frameId"
							value="${form.frameId }" style="width:200px; height:25px;" /></td>
					</tr>
					<tr>
						<td>厂牌型号：<input type="text" name="factoryId"
							value="${form.factoryId }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td>燃料类型：<input type="text" name="fuel" value="${form.fuel }"
							style="width:200px; height:25px;" /></td>
					</tr>
					<tr>
						<td>购车日期：<input type="text" name="buyDate"
							value="${form.buyDate }" style="width:200px; height:25px;" /></td>
					</tr>
					<tr>
						<td>道路运输证号：<input type="text" name="transportId"
							value="${form.transportId }" style="width:200px; height:25px;" />
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="提交"
							style="width:120px; height:30px" /> <input type="reset"
							value="恢复修改" style="width:120px; height:30px" /></td>
					</tr>
				</table>
			</form>
			<h1 align="center">
				<b
					style="letter-spacing:12px; font-size:40px; color: red; font-weight:bold;font-family:楷体">${requestScope.taxiMsg}</b>
			</h1>
		</c:otherwise>
	</c:choose>
</body>
</html>
