<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   
    <title>修改运价信息</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<style type="text/css">
tr { 
		height:"40";
}
</style>
<script type="text/javascript">
	function check() {
		 if(document.fareForm.areaId.value=="") {
    		alert('运价执行行政区划代码 ！');
 			return false;
  		} else if(document.fareForm.fareType.value=="") {
    		alert('运价类型！');
 			return false;
  		} else if(document.fareForm.fareFrom.value=="") {
    		alert('运价有效期起！');
 			return false;
  		} else if(document.fareForm.fareDayDown.value==""){
 			alert('昼间起步价！');
 			return false;
  		} else if(document.fareForm.fareNightDown.value==""){
    		alert('夜间起步价！');
 			return false;
  		} else if(document.fareForm.fareWait.value==""){
    		alert('候时单价！');
 			return false;
  		} else if(document.fareForm.downDistance.value==""){
    		alert('起步里程！');
 			return false;
  		} else if(document.fareForm.priceDay.value==""){
    		alert('昼间单价！');
 			return false;
  		} else if(document.fareForm.priceDayAdd.value==""){
    		alert('昼间单程加价单价！');
 			return false;
  		} else if(document.fareForm.priceNight.value==""){
    		alert('夜间单价！');
 			return false;
  		} else if(document.fareForm.priceNightAdd.value==""){
    		alert('夜间单程加价单价！');
 			return false;
  		} else if(document.fareForm.addKiloPerTrip.value==""){
    		alert('单程加价公里！');
 			return false;
  		} else if(document.fareForm.nigthFrom.value==""){
    		alert('夜间时间起！');
 			return false;
  		} else if(document.fareForm.nightEnd.value==""){
    		alert('夜间时间止！');
 			return false;
  		} else if(document.fareForm.lowSpeedWait.value==""){
    		alert('低速等候设置！');
 			return false;
  		} else if(document.fareForm.switchSpeed.value==""){
    		alert('切换速度！');
 			return false;
  		} else if(document.fareForm.fareRemark.value==""){
    		alert('运价备注说明 ！');
 			return false;
  		} else if(document.fareForm.fareStatusCode.value==""){
    		alert('运价状态代码！');
 			return false;
  		} else if(document.fareForm.fuelSurcharge.value==""){
    		alert('燃油附加费！');
 			return false;
  		} else if(document.fareForm.k.value==""){
    		alert('K值不为空！');
 			return false;
  		}
	}
	
	window.onload = function() {
		var msg = <%=request.getAttribute("fareMsg")%>;
		//alert(msg);
		if(msg == 1){
			alert("修改成功！！！");
		} else if(msg == 0) {
			alert("修改失败，运价信息不存在！！！");
		}
	}
</script>
  </head>
  
  <body>
    	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
	<h1 align="center">修改运价信息</h1>
	<hr>
	<form action="/TaxiManagerServer/FareServlet" Method="post" name="fareForm" onSubmit="return check();">
		<input type="hidden" name="method" value="modifyFare" />
		<table width="500" height="600" align="center" border="0" cellspacing="0" cellpadding="0" bordercolor="#0099cc">
			<tr height="40">
				<td>运价执行行政区划代码：<input type="text" name="areaId" value="${form.areaId }" style="width:200px; height:25px;"/></td>
			</tr>
			<tr height="40">
				<td>运价类型：<input type="text" name="fareType" value="${form.fareType }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>运价有效期起：<input type="text" name="fareFrom" value="${form.fareFrom }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>昼间起步价(元)：<input type="text" name="fareDayDown" value="${form.fareDayDown }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>夜间起步价(元)：<input type="text" name="fareNightDown" value="${form.fareNightDown }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>候时单价(元/分钟)：<input type="text" name="fareWait" value="${form.fareWait }" style="width:250px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>起步里程 (km)：<input type="text" name="downDistance" value="${form.downDistance }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>昼间单价(元/km)：<input type="text" name="priceDay" value="${form.priceDay }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>昼间单程加价单价(元/km)：<input type="text" name="priceDayAdd" value="${form.priceDayAdd }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>夜间单价(元)：<input type="text" name="priceNight" value="${form.priceNight }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>夜间单程加价单价(元/km)：<input type="text" name="priceNightAdd" value="${form.priceNightAdd }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>单程加价公里 (km)：<input type="text" name="addKiloPerTrip" value="${form.addKiloPerTrip }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>夜间时间起：<input type="text" name="nigthFrom" value="${form.nigthFrom }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>夜间时间止：<input type="text" name="nightEnd" value="${form.nightEnd }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>低速等候设置：<input type="text" name="lowSpeedWait" value="${form.lowSpeedWait }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>切换速度 (km/h)：<input type="text" name="switchSpeed" value="${form.switchSpeed }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>运价备注说明：<input type="text" name="fareRemark" value="${form.fareRemark }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>运价状态代码(0或1)：<input type="text" name="fareStatusCode" value="${form.fareStatusCode }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>燃油附加费(元)：<input type="text" name="fuelSurcharge" value="${form.fuelSurcharge }" style="width:200px; height:25px;" /></td>
			</tr>
						<tr height="40">
				<td>计价器K值：<input type="text" name="k" value="${form.k }" style="width:200px; height:25px;" />（0~65535）</td>
			</tr>
			<tr height="40">
				<td>上次修改时间：${form.updateTime }
					<input type="hidden" name="updateTime" value="${form.updateTime }" style="width:200px; height:25px;" />
				</td>
			</tr>
			<tr height="40">
				<td><input type="hidden" name="fareNum" value="${form.fareNum }" style="width:200px; height:25px;" /></td>
			</tr>
			<tr height="40">
				<td>
					<input type="submit" value="提交" style="width:120px; height:30px" /> 
					<input type="reset" value="清除" style="width:120px; height:30px" />
				</td>
			</tr>
		</table>
	</form>
	<h1 align="center">
		<b style="letter-spacing:12px; font-size:40px; color: red; font-weight:bold;font-family:楷体">${requestScope.fareMsg}</b>
	</h1>
		</c:otherwise>
	</c:choose>
  </body>
</html>

