<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>添加驾驶员信息</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript">
	function check() {
		 if(document.driverForm.driverName.value=="") {
    		alert('请输入姓名 ！');
 			return false;
  		} else if(document.driverForm.sex.value=="") {
    		alert('请输入性别！');
 			return false;
  		} else if(document.driverForm.id.value=="") {
    		alert('请输入身份证号！');
 			return false;
  		} else if(document.driverForm.employId.value==""){
 			alert('请输入从业资格证号！');
 			return false;
  		} else if(document.driverForm.employCardAgent.value==""){
    		alert('请选输入从业资格证发证机构！');
 			return false;
  		} else if(document.driverForm.timeBeginEnd.value==""){
    		alert('请输入从业资格证有效期起止时间！');
 			return false;
  		} else if(document.driverForm.tel.value==""){
    		alert('请输入联系电话！');
 			return false;
  		} else if(document.driverForm.companyId.value==""){
    		alert('请输入所属企业组织机构代码！');
 			return false;
  		} else if(document.driverForm.education.value==""){
    		alert('请输入学历！');
 			return false;
  		} else if(document.driverForm.serviceGrade.value==""){
    		alert('请输入服务质量信誉考核等级！');
 			return false;
  		} else if(document.driverForm.ICID.value==""){
    		alert('请输入IC卡号！');
 			return false;
  		} else if(/.*[\u4e00-\u9fa5]+.*$/.test(document.driverForm.id.value)){
  			alert('身份证号不能有汉字！');
 			return false;
  		} else if(/.*[\u4e00-\u9fa5]+.*$/.test(document.driverForm.ICID.value)){
  			alert('IC卡号不能有汉字！');
 			return false;
  		}
	}
</script>
</head>

<body>
	<c:choose>
		<c:when test="${empty sessionScope.sessionUser }">
			<br>
			<br>
			<br>
			<br>
			<br>
			<h1 align="center">
				<b style=" font-size:72px">您还没有登陆，请登陆！！！</b><br> <a
					href="/TaxiManagerServer/index.jsp"
					style="font-size:40px; font-weight:bold;font-family:楷体; align:center">点此登录</a>
			</h1>

		</c:when>
		<c:otherwise>
	<h1 align="center">添加驾驶员信息</h1>
	<hr>
	<form action="/TaxiManagerServer/DriverServlet" Method="post" name="driverForm" onSubmit="return check();" enctype="multipart/form-data">
		<input type="hidden" name="method" value="addDriver" />
		<table width="500" height="600" align="center" border="0"
			cellspacing="0" cellpadding="0" bordercolor="#0099cc">

			<tr>
				<td>姓名：<input type="text" name="driverName" value="${form.driverName }"
					style="width:200px; height:25px;" />
				</td>
			</tr>
			<tr>
				<td>照片：<input name="driverPhoto" type="file" size="20" >
				</td>
			</tr>
			<tr>
				<td>性别：<!-- <select name="sex" style="width:100px; height:30px">
					<option value="男">男</option>0.

					<option value="女">女</option>
			</select> --> <input type="text" name="sex" value="${form.sex }"
					style="width:200px; height:25px;" />
				</td>
			</tr>
			<tr>
				<td>身份证号：<input type="text" name="id" value="${form.id }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>从业资格证号：<input type="text" name="employId" value="${form.employId }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>从业资格证发证机构：<input type="text" name="employCardAgent" value="${form.employCardAgent }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>从业资格证有效期起止时间：<input type="text" name="timeBeginEnd" value="${form.timeBeginEnd }"
					style="width:250px; height:25px;" /></td>
			</tr>
			<tr>
				<td>联系电话：<input type="text" name="tel" value="${form.tel }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>所属企业组织机构代码：<input type="text" name="companyId" value="${form.companyId }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>学历：<input type="text" name="education" value="${form.education }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>服务质量信誉考核等级：<input type="text" name="serviceGrade" value="${form.serviceGrade }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td>IC卡号：<input type="text" name="ICID" value="${form.ICID }"
					style="width:200px; height:25px;" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="提交" style="width:120px; height:30px" /> 
					<input type="reset" value="清除" style="width:120px; height:30px" />
				</td>
			</tr>
		</table>
	</form>
	<h1 align="center">
		<b
			style="letter-spacing:12px; font-size:40px; color: red; font-weight:bold;font-family:楷体">${requestScope.driverMsg}</b>
	</h1>
			</c:otherwise>
	</c:choose>
</body>
</html>
