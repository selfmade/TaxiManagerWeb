import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.dbutils.QueryRunner;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import cn.itcast.jdbc.JdbcUtils;
import cn.itcast.jdbc.TxQueryRunner;

import com.bdy.lm.browser.dao.TaxiDao;
import com.bdy.lm.browser.dao.VerifyAdjustDao;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.Command;
import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.browser.domain.StandardRoute;
import com.bdy.lm.browser.domain.Taxi;
import com.bdy.lm.browser.domain.User;
import com.bdy.lm.browser.service.OperationQueryService;
import com.bdy.lm.browser.service.RemoteManageService;
import com.bdy.lm.browser.service.VerifyAdjustService;
import com.bdy.lm.isu.dao.ProcessISURequestDao;
import com.bdy.lm.isu.domain.ISUMessage;
import com.bdy.lm.isu.service.ProcessISURequestService;
import com.bdy.lm.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UnitTest {
	private String driverClassName = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/taximanagerdb";
	private String username = "root";
	private String password = "123456";

	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;

	private QueryRunner qr = new TxQueryRunner();

	@Test
	public void queryCheatMonitor() throws SQLException{
		String sql = "SELECT locationTracks FROM scar_tb WHERE verifyTime=1513692842243";
		byte[] locationTracks = null;
        try {
            con = JdbcUtils.getConnection();
			ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
            	locationTracks = new byte[rs.getBinaryStream("locationTracks").available()];
                rs.getBinaryStream("locationTracks").read(locationTracks);
            }
            String result = new String(locationTracks);
			//System.out.println("reslut:" + result);
			List<LocationTracks> list = new Gson().fromJson(result, new TypeToken<List<LocationTracks>>(){}.getType());
			System.out.println(calculateDistance(list));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
			 if (rs != null)
			 rs.close();
			 if (con != null)
			 con.close();
			 if (ps != null)
			 con.close();
        }
	}
    private String calculateDistance(List<LocationTracks> locationTracks) {
        double claculateDistance = 0.0000000001;
        double calculateSpeed = 0, s = 1852.0 / 3600.0;
        LocationTracks begin = locationTracks.get(0);
        for(LocationTracks lTracks : locationTracks) {
            claculateDistance += distance(begin.getLatitude(), begin.getLongitude(), lTracks.getLatitude(), lTracks.getLongitude());
            begin = lTracks;
            calculateSpeed += lTracks.getSpeed() * 2 * 1000.0 / 3600;
            //System.out.println("re " + 1852 / 3600 + " " + calculateSpeed);
        }
        calculateSpeed = calculateSpeed - locationTracks.get(0).getSpeed() * s;
        //System.out.println("计价器" + taximeterCount + "GPS" + claculateDistance + " 误差" + (taximeterCount - claculateDistance) / claculateDistance);
        return Utils.doubleFour(claculateDistance * 1000) + " " + Utils.doubleFour(calculateSpeed);
    }
	private double rad(double d) {
        return d * Math.PI / 180.0;
    }
    private double distance(double beginLat, double beginLng, double endLat, double endLng) {
        final double EARTH_RADIUS = 6378.137;//地球半径,单位千米
        double a = rad(beginLat) - rad(endLat);
        double b = rad(beginLng) - rad(endLng);
        double radLat1 = rad(beginLat);
        double radLat2 = rad(endLat);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        return s;
    }
	static double pi = 3.14159265358979324;  
	double a = 6378245.0;  
	double ee = 0.00669342162296594323;  
	static boolean outOfChina(double lat, double lon) {   
		if (lon < 72.004 || lon > 137.8347)  
			return true;   
		if (lat < 0.8293 || lat > 55.8271)  
			return true;   
		return false;  
	}   
	static double transformLat(double x, double y) {   
		double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));  
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;   
		ret += (20.0 * Math.sin(y * pi) + 40.0 * Math.sin(y / 3.0 * pi)) * 2.0 / 3.0;   
		ret += (160.0 * Math.sin(y / 12.0 * pi) + 320 * Math.sin(y * pi / 30.0)) * 2.0 / 3.0;   
		return ret;  
	}   
	static double transformLon(double x, double y) {   
		double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));   
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;   
		ret += (20.0 * Math.sin(x * pi) + 40.0 * Math.sin(x / 3.0 * pi)) * 2.0 / 3.0;  
		ret += (150.0 * Math.sin(x / 12.0 * pi) + 300.0 * Math.sin(x / 30.0 * pi)) * 2.0 / 3.0;   
		return ret;  
	}   
	/* 参数  wgLat:WGS-84纬度wgLon:WGS-84经度 
		返回值：  mgLat：GCJ-02纬度mgLon：GCJ-02经度 
	 */  
	void gps_transform(double wgLon, double wgLat) {   
		double mgLat,mgLon;
		if (outOfChina(wgLat, wgLon)) {   
			mgLat = wgLat;  
			mgLon = wgLon;  
			return;   
		}   
		double dLat = transformLat(wgLon - 105.0, wgLat - 35.0);   
		double dLon = transformLon(wgLon - 105.0, wgLat - 35.0);   
		double radLat = wgLat / 180.0 * pi; 
		double magic = Math.sin(radLat);  
		magic = 1 - ee * magic * magic; 
		double sqrtMagic = Math.sqrt(magic);  
		dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);  
		dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);  
		mgLat = wgLat + dLat; 
		mgLon = wgLon + dLon; 
		System.out.println("" + mgLon + " \n" + mgLat);
	} 
	
	@Test
	public void tempTest() { 
		//System.out.print(Arrays.toString("否 123".split(" ")));
        String ss = "{\"evaluate\":\"null\",\"messageSerialNum\":395,\"waitTime\":\"0:0\",\"checkCode\":-34,\"flagBitEnd\":126,\"journey\":\"single\",\"transitionType\":\"cash\",\"ISUFlag_company_2\":16,\"locationTracks\":[{\"speed\":1.0270270109176636,\"time\":1512995640925,\"longitude\":119.25750500000001,\"latitude\":26.060895},{\"speed\":1.4810811281204224,\"time\":1512995641925,\"longitude\":119.25749499999999,\"latitude\":26.060905},{\"speed\":1.7081080675125122,\"time\":1512995642926,\"longitude\":119.25747833333335,\"latitude\":26.06091666666667},{\"speed\":2.3297297954559326,\"time\":1512995643926,\"longitude\":119.25745666666666,\"latitude\":26.06093166666667},{\"speed\":3.0216217041015625,\"time\":1512995644926,\"longitude\":119.25742999999999,\"latitude\":26.060954999999996},{\"speed\":3.8216216564178467,\"time\":1512995645926,\"longitude\":119.25739999999999,\"latitude\":26.060985},{\"speed\":4.7675676345825195,\"time\":1512995646926,\"longitude\":119.25736166666665,\"latitude\":26.06101833333333},{\"speed\":5.6270270347595215,\"time\":1512995647926,\"longitude\":119.25731666666667,\"latitude\":26.061056666666666},{\"speed\":6.194594383239746,\"time\":1512995648927,\"longitude\":119.25727166666665,\"latitude\":26.061100000000003},{\"speed\":6.6270270347595215,\"time\":1512995649927,\"longitude\":119.25722833333333,\"latitude\":26.061149999999998},{\"speed\":7.15675687789917,\"time\":1512995650927,\"longitude\":119.25718,\"latitude\":26.061196666666667},{\"speed\":7.194594383239746,\"time\":1512995651927,\"longitude\":119.25713499999999,\"latitude\":26.061241666666668},{\"speed\":7.340540409088135,\"time\":1512995652927,\"longitude\":119.25709,\"latitude\":26.06128833333333},{\"speed\":7.129729747772217,\"time\":1512995653927,\"longitude\":119.25704499999999,\"latitude\":26.06133833333333},{\"speed\":7.151351451873779,\"time\":1512995654928,\"longitude\":119.257,\"latitude\":26.061383333333332},{\"speed\":6.908108234405518,\"time\":1512995655928,\"longitude\":119.25695499999999,\"latitude\":26.061426666666662},{\"speed\":6.681081295013428,\"time\":1512995656928,\"longitude\":119.25691,\"latitude\":26.061465},{\"speed\":6.859459400177002,\"time\":1512995657928,\"longitude\":119.25686166666668,\"latitude\":26.061500000000002},{\"speed\":6.1891889572143555,\"time\":1512995658928,\"longitude\":119.25682166666665,\"latitude\":26.061538333333335},{\"speed\":5.92972993850708,\"time\":1512995659929,\"longitude\":119.25678333333332,\"latitude\":26.061576666666664},{\"speed\":5.859459400177002,\"time\":1512995660929,\"longitude\":119.25674666666667,\"latitude\":26.061613333333337},{\"speed\":5.951351165771484,\"time\":1512995661929,\"longitude\":119.25671166666667,\"latitude\":26.061653333333332},{\"speed\":5.9783782958984375,\"time\":1512995662929,\"longitude\":119.25667,\"latitude\":26.061695},{\"speed\":6.643243312835693,\"time\":1512995663929,\"longitude\":119.25662666666668,\"latitude\":26.061741666666663},{\"speed\":6.92972993850708,\"time\":1512995664929,\"longitude\":119.25658166666666,\"latitude\":26.061788333333332},{\"speed\":7.491891860961914,\"time\":1512995665930,\"longitude\":119.25653333333334,\"latitude\":26.06184166666667},{\"speed\":8.070270538330078,\"time\":1512995666930,\"longitude\":119.25648166666666,\"latitude\":26.061898333333335},{\"speed\":8.529729843139648,\"time\":1512995667930,\"longitude\":119.25642666666666,\"latitude\":26.061954999999998},{\"speed\":8.745945930480957,\"time\":1512995668930,\"longitude\":119.25636833333334,\"latitude\":26.062013333333333},{\"speed\":9.00540542602539,\"time\":1512995669930,\"longitude\":119.25631,\"latitude\":26.062068333333333},{\"speed\":8.881080627441406,\"time\":1512995670931,\"longitude\":119.25624833333335,\"latitude\":26.062121666666666},{\"speed\":8.654053688049316,\"time\":1512995671931,\"longitude\":119.25619,\"latitude\":26.062175000000003},{\"speed\":8.45945930480957,\"time\":1512995672931,\"longitude\":119.25613666666668,\"latitude\":26.062228333333337},{\"speed\":8.297297477722168,\"time\":1512995673931,\"longitude\":119.25608666666668,\"latitude\":26.062283333333333},{\"speed\":8.183783531188965,\"time\":1512995674931,\"longitude\":119.25603666666666,\"latitude\":26.06233666666667},{\"speed\":8.118919372558594,\"time\":1512995675932,\"longitude\":119.25598999999998,\"latitude\":26.062386666666665},{\"speed\":7.891891956329346,\"time\":1512995676932,\"longitude\":119.25594833333335,\"latitude\":26.06244},{\"speed\":7.654054164886475,\"time\":1512995677932,\"longitude\":119.25590500000001,\"latitude\":26.062493333333336},{\"speed\":7.643243312835693,\"time\":1512995678932,\"longitude\":119.25586166666666,\"latitude\":26.06254333333333},{\"speed\":7.567567348480225,\"time\":1512995679932,\"longitude\":119.25581500000001,\"latitude\":26.062593333333332},{\"speed\":7.524324417114258,\"time\":1512995680932,\"longitude\":119.25577333333334,\"latitude\":26.06264666666667},{\"speed\":7.437837600708008,\"time\":1512995681933,\"longitude\":119.255735,\"latitude\":26.062701666666666},{\"speed\":7.61621618270874,\"time\":1512995682933,\"longitude\":119.25569500000002,\"latitude\":26.06275666666667},{\"speed\":7.5837836265563965,\"time\":1512995683933,\"longitude\":119.25565499999999,\"latitude\":26.06281},{\"speed\":7.45405387878418,\"time\":1512995684933,\"longitude\":119.255615,\"latitude\":26.06286},{\"speed\":7.124324321746826,\"time\":1512995685934,\"longitude\":119.25557666666667,\"latitude\":26.062906666666663},{\"speed\":6.756756782531738,\"time\":1512995686934,\"longitude\":119.25553833333333,\"latitude\":26.06294666666667},{\"speed\":6.183784008026123,\"time\":1512995687934,\"longitude\":119.25550333333334,\"latitude\":26.062985000000005},{\"speed\":5.664865016937256,\"time\":1512995688934,\"longitude\":119.25547166666665,\"latitude\":26.06302333333333},{\"speed\":5.605405330657959,\"time\":1512995689934,\"longitude\":119.25543833333334,\"latitude\":26.063063333333336},{\"speed\":5.8810811042785645,\"time\":1512995690935,\"longitude\":119.25540000000001,\"latitude\":26.063105000000004},{\"speed\":6.432432651519775,\"time\":1512995691935,\"longitude\":119.25536166666667,\"latitude\":26.063153333333336},{\"speed\":6.972972869873047,\"time\":1512995692935,\"longitude\":119.25532166666666,\"latitude\":26.063205},{\"speed\":7.432432651519775,\"time\":1512995693935,\"longitude\":119.25527833333332,\"latitude\":26.063258333333337},{\"speed\":7.800000190734863,\"time\":1512995694935,\"longitude\":119.25523,\"latitude\":26.063314999999996},{\"speed\":8.335135459899902,\"time\":1512995695935,\"longitude\":119.25518333333333,\"latitude\":26.063373333333335},{\"speed\":8.443243026733398,\"time\":1512995696936,\"longitude\":119.25513666666666,\"latitude\":26.063431666666663},{\"speed\":8.508108139038086,\"time\":1512995697936,\"longitude\":119.25509166666667,\"latitude\":26.063488333333332},{\"speed\":8.189188957214355,\"time\":1512995698936,\"longitude\":119.25505833333334,\"latitude\":26.063551666666665},{\"speed\":8.22702693939209,\"time\":1512995699936,\"longitude\":119.255015,\"latitude\":26.06361},{\"speed\":8.275675773620605,\"time\":1512995700937,\"longitude\":119.25497166666668,\"latitude\":26.06366666666667},{\"speed\":8.010810852050781,\"time\":1512995701937,\"longitude\":119.25493,\"latitude\":26.063719999999996},{\"speed\":7.621621608734131,\"time\":1512995702937,\"longitude\":119.25488666666666,\"latitude\":26.06377166666667},{\"speed\":7.556756973266602,\"time\":1512995703937,\"longitude\":119.25484166666665,\"latitude\":26.063824999999998},{\"speed\":7.794594764709473,\"time\":1512995704937,\"longitude\":119.25478999999999,\"latitude\":26.06387666666667},{\"speed\":8.059459686279297,\"time\":1512995705937,\"longitude\":119.25474333333335,\"latitude\":26.063935},{\"speed\":8.45945930480957,\"time\":1512995706938,\"longitude\":119.25469333333334,\"latitude\":26.06399666666667},{\"speed\":8.794594764709473,\"time\":1512995707938,\"longitude\":119.25464166666667,\"latitude\":26.06405833333333},{\"speed\":8.91891860961914,\"time\":1512995708938,\"longitude\":119.25459000000001,\"latitude\":26.064121666666665},{\"speed\":8.962162017822266,\"time\":1512995709938,\"longitude\":119.25453999999999,\"latitude\":26.064186666666668},{\"speed\":9.162161827087402,\"time\":1512995710939,\"longitude\":119.25448833333333,\"latitude\":26.06425166666667},{\"speed\":9.329730033874512,\"time\":1512995711939,\"longitude\":119.25443500000002,\"latitude\":26.064318333333336},{\"speed\":9.410810470581055,\"time\":1512995712939,\"longitude\":119.25437666666667,\"latitude\":26.064379999999996},{\"speed\":9.270270347595215,\"time\":1512995713939,\"longitude\":119.25432833333335,\"latitude\":26.064445},{\"speed\":9.129729270935059,\"time\":1512995714940,\"longitude\":119.25427833333333,\"latitude\":26.064510000000002},{\"speed\":9.097297668457031,\"time\":1512995715940,\"longitude\":119.25422666666667,\"latitude\":26.064573333333335},{\"speed\":9.237837791442871,\"time\":1512995716940,\"longitude\":119.25417333333334,\"latitude\":26.06463666666667},{\"speed\":9.199999809265137,\"time\":1512995717940,\"longitude\":119.25412666666668,\"latitude\":26.064700000000002},{\"speed\":8.902702331542969,\"time\":1512995718940,\"longitude\":119.25408166666666,\"latitude\":26.06475833333333},{\"speed\":8.362162590026855,\"time\":1512995719940,\"longitude\":119.25403,\"latitude\":26.064811666666667},{\"speed\":8.302702903747559,\"time\":1512995720941,\"longitude\":119.25398833333333,\"latitude\":26.06486},{\"speed\":7.010810852050781,\"time\":1512995721941,\"longitude\":119.253955,\"latitude\":26.064896666666662},{\"speed\":5.405405521392822,\"time\":1512995722941,\"longitude\":119.25392666666667,\"latitude\":26.064929999999997},{\"speed\":5.07027006149292,\"time\":1512995723941,\"longitude\":119.25389833333332,\"latitude\":26.06496666666667},{\"speed\":5.216216087341309,\"time\":1512995724941,\"longitude\":119.25386833333332,\"latitude\":26.065009999999997},{\"speed\":6.037837982177734,\"time\":1512995725941,\"longitude\":119.25383333333333,\"latitude\":26.065061666666665},{\"speed\":6.913513660430908,\"time\":1512995726942,\"longitude\":119.253795,\"latitude\":26.06511666666667},{\"speed\":7.740540504455566,\"time\":1512995727942,\"longitude\":119.25374833333332,\"latitude\":26.06517666666667},{\"speed\":8.513513565063477,\"time\":1512995728942,\"longitude\":119.25369833333335,\"latitude\":26.065240000000003},{\"speed\":9.248648643493652,\"time\":1512995729942,\"longitude\":119.25363999999999,\"latitude\":26.065306666666668},{\"speed\":9.881080627441406,\"time\":1512995730942,\"longitude\":119.25357666666666,\"latitude\":26.065375000000003},{\"speed\":10.448648452758789,\"time\":1512995731943,\"longitude\":119.25351500000001,\"latitude\":26.065448333333332},{\"speed\":10.562162399291992,\"time\":1512995732943,\"longitude\":119.25345166666666,\"latitude\":26.06552},{\"speed\":10.627026557922363,\"time\":1512995733943,\"longitude\":119.25338833333332,\"latitude\":26.065591666666666},{\"speed\":10.805405616760254,\"time\":1512995734943,\"longitude\":119.25332666666667,\"latitude\":26.065665},{\"speed\":10.654053688049316,\"time\":1512995735943,\"longitude\":119.25326500000001,\"latitude\":26.065736666666666},{\"speed\":10.54594612121582,\"time\":1512995736944,\"longitude\":119.25320166666667,\"latitude\":26.065808333333333},{\"speed\":10.443243026733398,\"time\":1512995737944,\"longitude\":119.25314166666666,\"latitude\":26.06587833333333},{\"speed\":10.248648643493652,\"time\":1512995738944,\"longitude\":119.25308500000001,\"latitude\":26.065945000000003},{\"speed\":9.891891479492188,\"time\":1512995739944,\"longitude\":119.25302833333332,\"latitude\":26.066006666666667},{\"speed\":9.427026748657227,\"time\":1512995740944,\"longitude\":119.252975,\"latitude\":26.066066666666664},{\"speed\":9.08108139038086,\"time\":1512995741944,\"longitude\":119.25292499999999,\"latitude\":26.066123333333334},{\"speed\":8.351351737976074,\"time\":1512995742945,\"longitude\":119.25287666666667,\"latitude\":26.066176666666667},{\"speed\":8.09189224243164,\"time\":1512995743945,\"longitude\":119.25282833333334,\"latitude\":26.066228333333335},{\"speed\":7.789189338684082,\"time\":1512995744945,\"longitude\":119.25278333333333,\"latitude\":26.06628},{\"speed\":7.745945930480957,\"time\":1512995745945,\"longitude\":119.252735,\"latitude\":26.066331666666663},{\"speed\":7.918919086456299,\"time\":1512995746946,\"longitude\":119.25268499999999,\"latitude\":26.066385},{\"speed\":8.108108520507813,\"time\":1512995747946,\"longitude\":119.25263666666666,\"latitude\":26.066439999999997},{\"speed\":8.102703094482422,\"time\":1512995748946,\"longitude\":119.25259,\"latitude\":26.06649333333333},{\"speed\":7.832432270050049,\"time\":1512995749946,\"longitude\":119.25254666666666,\"latitude\":26.066544999999998},{\"speed\":7.589189052581787,\"time\":1512995750946,\"longitude\":119.25250333333334,\"latitude\":26.066593333333337},{\"speed\":7.237837791442871,\"time\":1512995751946,\"longitude\":119.252465,\"latitude\":26.06664166666667},{\"speed\":6.827026844024658,\"time\":1512995752947,\"longitude\":119.25242833333331,\"latitude\":26.066685},{\"speed\":6.38918924331665,\"time\":1512995753947,\"longitude\":119.25239833333332,\"latitude\":26.06673},{\"speed\":6.027027130126953,\"time\":1512995754947,\"longitude\":119.25237,\"latitude\":26.066775},{\"speed\":5.940540313720703,\"time\":1512995755947,\"longitude\":119.25234333333334,\"latitude\":26.06682},{\"speed\":5.902702808380127,\"time\":1512995756947,\"longitude\":119.25231000000001,\"latitude\":26.066856666666663},{\"speed\":5.713513374328613,\"time\":1512995757947,\"longitude\":119.25226833333333,\"latitude\":26.066886666666665},{\"speed\":5.702702522277832,\"time\":1512995758947,\"longitude\":119.25221833333332,\"latitude\":26.066913333333332},{\"speed\":6.081080913543701,\"time\":1512995759948,\"longitude\":119.25216166666665,\"latitude\":26.06693833333333},{\"speed\":6.5837836265563965,\"time\":1512995760948,\"longitude\":119.252105,\"latitude\":26.066968333333328},{\"speed\":6.918919086456299,\"time\":1512995761948,\"longitude\":119.25204333333335,\"latitude\":26.067003333333332},{\"speed\":7.572972774505615,\"time\":1512995762948,\"longitude\":119.25198166666665,\"latitude\":26.067041666666668},{\"speed\":8.075675964355469,\"time\":1512995763948,\"longitude\":119.25191500000001,\"latitude\":26.067085},{\"speed\":8.475675582885742,\"time\":1512995764949,\"longitude\":119.25185166666667,\"latitude\":26.067135},{\"speed\":8.85405445098877,\"time\":1512995765949,\"longitude\":119.25178166666667,\"latitude\":26.06718},{\"speed\":9.037837982177734,\"time\":1512995766949,\"longitude\":119.251715,\"latitude\":26.06722333333333},{\"speed\":8.600000381469727,\"time\":1512995767949,\"longitude\":119.25164333333332,\"latitude\":26.067263333333337},{\"speed\":8.675675392150879,\"time\":1512995768949,\"longitude\":119.25157499999999,\"latitude\":26.06730166666667},{\"speed\":8.448648452758789,\"time\":1512995769949,\"longitude\":119.25150333333335,\"latitude\":26.067336666666666},{\"speed\":8.513513565063477,\"time\":1512995770950,\"longitude\":119.25143000000001,\"latitude\":26.067369999999997},{\"speed\":8.31891918182373,\"time\":1512995771950,\"longitude\":119.25135833333334,\"latitude\":26.0674},{\"speed\":8.345946311950684,\"time\":1512995772950,\"longitude\":119.25128833333335,\"latitude\":26.067429999999998},{\"speed\":8.09189224243164,\"time\":1512995773950,\"longitude\":119.25121833333333,\"latitude\":26.067459999999997},{\"speed\":8.064865112304688,\"time\":1512995774950,\"longitude\":119.251145,\"latitude\":26.067486666666664},{\"speed\":8.399999618530273,\"time\":1512995775950,\"longitude\":119.25107000000001,\"latitude\":26.067511666666665},{\"speed\":8.508108139038086,\"time\":1512995776951,\"longitude\":119.25099166666665,\"latitude\":26.067533333333337},{\"speed\":8.627026557922363,\"time\":1512995777951,\"longitude\":119.25091166666668,\"latitude\":26.067551666666667},{\"speed\":8.632431983947754,\"time\":1512995778951,\"longitude\":119.25083333333332,\"latitude\":26.06757},{\"speed\":8.443243026733398,\"time\":1512995779951,\"longitude\":119.25075666666667,\"latitude\":26.067585},{\"speed\":8.324324607849121,\"time\":1512995780951,\"longitude\":119.25068000000002,\"latitude\":26.067596666666667},{\"speed\":8.481081008911133,\"time\":1512995781952,\"longitude\":119.25060333333334,\"latitude\":26.067608333333336},{\"speed\":8.394594192504883,\"time\":1512995782952,\"longitude\":119.25052166666667,\"latitude\":26.067616666666662},{\"speed\":8.443243026733398,\"time\":1512995783952,\"longitude\":119.25044000000001,\"latitude\":26.067623333333337},{\"speed\":8.518918991088867,\"time\":1512995784952,\"longitude\":119.25035333333332,\"latitude\":26.06763},{\"speed\":8.68108081817627,\"time\":1512995785952,\"longitude\":119.250265,\"latitude\":26.067635000000003},{\"speed\":8.735135078430176,\"time\":1512995786953,\"longitude\":119.25018166666665,\"latitude\":26.067641666666667},{\"speed\":8.567567825317383,\"time\":1512995787953,\"longitude\":119.25009999999999,\"latitude\":26.067648333333334},{\"speed\":8.356757164001465,\"time\":1512995788953,\"longitude\":119.25002333333332,\"latitude\":26.06766},{\"speed\":8.016216278076172,\"time\":1512995789953,\"longitude\":119.24994999999998,\"latitude\":26.06767},{\"speed\":7.800000190734863,\"time\":1512995790953,\"longitude\":119.24988166666665,\"latitude\":26.067681666666665},{\"speed\":7.291892051696777,\"time\":1512995791954,\"longitude\":119.249815,\"latitude\":26.067686666666667},{\"speed\":7.037837982177734,\"time\":1512995792954,\"longitude\":119.24975,\"latitude\":26.067684999999997},{\"speed\":6.848648548126221,\"time\":1512995793954,\"longitude\":119.24967999999998,\"latitude\":26.067676666666664},{\"speed\":7.394594669342041,\"time\":1512995794954,\"longitude\":119.249605,\"latitude\":26.06767166666667},{\"speed\":8.054054260253906,\"time\":1512995795954,\"longitude\":119.24952499999999,\"latitude\":26.067663333333332},{\"speed\":8.556756973266602,\"time\":1512995796955,\"longitude\":119.24943999999999,\"latitude\":26.067655},{\"speed\":9.070270538330078,\"time\":1512995797955,\"longitude\":119.24935166666667,\"latitude\":26.067646666666665},{\"speed\":9.254054069519043,\"time\":1512995798955,\"longitude\":119.24926500000001,\"latitude\":26.06764333333333},{\"speed\":9.064865112304688,\"time\":1512995799956,\"longitude\":119.24918000000001,\"latitude\":26.067639999999997},{\"speed\":8.881080627441406,\"time\":1512995800956,\"longitude\":119.24909333333332,\"latitude\":26.067636666666665},{\"speed\":9.14054012298584,\"time\":1512995801956,\"longitude\":119.24900833333335,\"latitude\":26.067638333333335},{\"speed\":9.016216278076172,\"time\":1512995802956,\"longitude\":119.24892333333334,\"latitude\":26.067636666666665},{\"speed\":8.69189167022705,\"time\":1512995803957,\"longitude\":119.24884166666668,\"latitude\":26.067631666666664},{\"speed\":8.491891860961914,\"time\":1512995804957,\"longitude\":119.24875833333333,\"latitude\":26.06762833333333},{\"speed\":8.529729843139648,\"time\":1512995805957,\"longitude\":119.248675,\"latitude\":26.06762},{\"speed\":8.718918800354004,\"time\":1512995806957,\"longitude\":119.24859333333333,\"latitude\":26.067621666666664},{\"speed\":8.600000381469727,\"time\":1512995807957,\"longitude\":119.24851166666667,\"latitude\":26.06762666666667},{\"speed\":8.670269966125488,\"time\":1512995808957,\"longitude\":119.24843000000001,\"latitude\":26.06762833333333},{\"speed\":8.54054069519043,\"time\":1512995809958,\"longitude\":119.24835,\"latitude\":26.06762666666667},{\"speed\":8.394594192504883,\"time\":1512995810958,\"longitude\":119.24826999999999,\"latitude\":26.067623333333337},{\"speed\":8.210810661315918,\"time\":1512995811958,\"longitude\":119.248195,\"latitude\":26.06762},{\"speed\":8.021621704101563,\"time\":1512995812958,\"longitude\":119.24811999999999,\"latitude\":26.06762},{\"speed\":7.756756782531738,\"time\":1512995813959,\"longitude\":119.24804666666665,\"latitude\":26.067621666666664},{\"speed\":7.6270270347595215,\"time\":1512995814959,\"longitude\":119.24797500000001,\"latitude\":26.067623333333337},{\"speed\":7.38918924331665,\"time\":1512995815959,\"longitude\":119.24790666666668,\"latitude\":26.067623333333337},{\"speed\":7.167567729949951,\"time\":1512995816959,\"longitude\":119.24784166666669,\"latitude\":26.06762666666667},{\"speed\":6.859459400177002,\"time\":1512995817959,\"longitude\":119.24778333333333,\"latitude\":26.06763},{\"speed\":5.994594573974609,\"time\":1512995818960,\"longitude\":119.24773666666667,\"latitude\":26.06763}],\"ISUFlag_1\":16,\"ICID\":\"195996CE\",\"fuelSurcharge\":\"0.0\",\"getOnTime\":\"2017-12-11 20:33\",\"distance\":\"1.3\",\"messageID\":110,\"price\":\"2\",\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"spendTime\":\"0:3\",\"ISUFlag_device_3\":0,\"currentRunTime\":\"24578\",\"money\":\"10.0\",\"messageSize\":15,\"getOffTime\":\"2017-12-11 20:36\",\"nullRun\":\"0.1\"}\n";

        String t = "{\"evaluate\":\"null\",\"messageSerialNum\":316,\"waitTime\":\"0:0\",\"checkCode\":-34,\"flagBitEnd\":126,\"journey\":\"single\",\"transitionType\":\"cash\",\"ISUFlag_company_2\":16,\"locationTracks\":[{\"speed\":0.4000000059604645,\"time\":1512996603650,\"longitude\":119.24754333333334,\"latitude\":26.06762},{\"speed\":0.4000000059604645,\"time\":1512996604650,\"longitude\":119.24754333333334,\"latitude\":26.06762},{\"speed\":0.0324324332177639,\"time\":1512996605650,\"longitude\":119.24754499999999,\"latitude\":26.06763},{\"speed\":0,\"time\":1512996606650,\"longitude\":119.24754166666668,\"latitude\":26.067641666666667},{\"speed\":0,\"time\":1512996607650,\"longitude\":119.24754166666668,\"latitude\":26.067641666666667},{\"speed\":0,\"time\":1512996608650,\"longitude\":119.24754166666668,\"latitude\":26.067641666666667},{\"speed\":0,\"time\":1512996609651,\"longitude\":119.24754166666668,\"latitude\":26.067641666666667},{\"speed\":0,\"time\":1512996610651,\"longitude\":119.24754166666668,\"latitude\":26.067641666666667},{\"speed\":0.5567567348480225,\"time\":1512996611651,\"longitude\":119.24753000000001,\"latitude\":26.06763},{\"speed\":0.5567567348480225,\"time\":1512996612651,\"longitude\":119.24753000000001,\"latitude\":26.06763},{\"speed\":0.6972972750663757,\"time\":1512996613651,\"longitude\":119.24752333333332,\"latitude\":26.067618333333332},{\"speed\":1.0864864587783813,\"time\":1512996614652,\"longitude\":119.24752333333332,\"latitude\":26.067608333333336},{\"speed\":1.0864864587783813,\"time\":1512996615652,\"longitude\":119.24752333333332,\"latitude\":26.067608333333336},{\"speed\":0.9783783555030823,\"time\":1512996616652,\"longitude\":119.24751833333333,\"latitude\":26.067596666666667},{\"speed\":0.9783783555030823,\"time\":1512996617652,\"longitude\":119.24751833333333,\"latitude\":26.067596666666667},{\"speed\":0.9783783555030823,\"time\":1512996618652,\"longitude\":119.24751833333333,\"latitude\":26.067596666666667},{\"speed\":0.9783783555030823,\"time\":1512996619652,\"longitude\":119.24751833333333,\"latitude\":26.067596666666667},{\"speed\":0.5027027130126953,\"time\":1512996620653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":0.5027027130126953,\"time\":1512996621653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":0.5027027130126953,\"time\":1512996622653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":0.5027027130126953,\"time\":1512996623653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":0.5027027130126953,\"time\":1512996624653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":0.5027027130126953,\"time\":1512996625653,\"longitude\":119.24751499999999,\"latitude\":26.067606666666666},{\"speed\":1.113513469696045,\"time\":1512996626654,\"longitude\":119.24753333333335,\"latitude\":26.067608333333336},{\"speed\":1.2162162065505981,\"time\":1512996627654,\"longitude\":119.24755166666667,\"latitude\":26.06761},{\"speed\":1.3891892433166504,\"time\":1512996628654,\"longitude\":119.24756833333335,\"latitude\":26.06761},{\"speed\":1.329729676246643,\"time\":1512996629654,\"longitude\":119.24758333333332,\"latitude\":26.067611666666668},{\"speed\":1.2162162065505981,\"time\":1512996630655,\"longitude\":119.24759833333334,\"latitude\":26.067615},{\"speed\":1.2432432174682617,\"time\":1512996631655,\"longitude\":119.24760999999998,\"latitude\":26.067618333333332},{\"speed\":1.3621621131896973,\"time\":1512996632655,\"longitude\":119.24762333333335,\"latitude\":26.067616666666662},{\"speed\":1.345945954322815,\"time\":1512996633655,\"longitude\":119.24763333333334,\"latitude\":26.067616666666662},{\"speed\":1.1189188957214355,\"time\":1512996634655,\"longitude\":119.24764333333333,\"latitude\":26.067616666666662}],\"ISUFlag_1\":16,\"ICID\":\"195996CE\",\"fuelSurcharge\":\"0.0\",\"getOnTime\":\"2017-12-11 21:1\",\"distance\":\"0.0\",\"messageID\":110,\"price\":\"2\",\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"spendTime\":\"0:0\",\"ISUFlag_device_3\":0,\"currentRunTime\":\"24581\",\"money\":\"10.0\",\"messageSize\":15,\"getOffTime\":\"2017-12-11 21:1\",\"nullRun\":\"1.1\"}";
		
        String q = "{\"reportTime\":1513241247273,\"messageID\":113,\"messageSerialNum\":3,\"flagBitBegin\":126,\"ISUFlag\":\"1616010526880\",\"checkCode\":-34,\"runState\":\"none\",\"flagBitEnd\":126,\"messageSize\":15,\"ICID\":\"195996CE\"}";
        //System.out.println(new TaxiDao().queryTaxi("ISUFlag", "1616010526880").get(0));
        //System.out.println(new ProcessISURequestService().chooseMethod(q.getBytes()));   
        
        DecimalFormat    df   = new DecimalFormat("######0.00"); 
        //System.out.println(df.format(178782.2));        
        //System.out.println(Arrays.toString(gcj02_To_Bd09(119.2670368567, 26.0568814072)));// \"ISUFlag_company_2\":16
        //System.out.println(Arrays.toString(gcj02_To_Bd09(119.2730974825, 26.0540551505)));

        //System.out.println(new String(Base64.getDecoder().decode("MTE5LjI3ODI1MTE2NTkx")));//,"y":"
        //System.out.println(new String(Base64.getDecoder().decode("MjYuMDYwMDk1MDQ3MTIx")));
        
        //System.out.println(new String(Base64.getDecoder().decode("MTE5LjI3ODI1MDA4MjI3")));
        //System.out.println(new String(Base64.getDecoder().decode("MjYuMDYwMDk0NDk2MDM4")));
        //","y":"
        
        //gps_transform(119.2670368567, 26.0568814072);
        
        Taxi taxi = new Taxi();
        taxi.setBuyDate("2017");
        //System.out.println(new Gson().fromJson(new Gson().toJson(taxi).toString(), Taxi.class));  
        String d = "{\"verifyTime\":\"123456781\",\"messageID\":114,\"messageSerialNum\":386,\"flagBitBegin\":126,\"ISUFlag\":\"1616010526880\",\"checkCode\":-34,\"flagBitEnd\":126,\"messageSize\":15,\"locationTracks\":[{\"speed\":10.048649,\"time\":1513478297112,\"longitude\":119.267058,\"latitude\":26.056903},{\"speed\":10.086487,\"time\":1513478298113,\"longitude\":119.267145,\"latitude\":26.056863},{\"speed\":10.2,\"time\":1513478299113,\"longitude\":119.267232,\"latitude\":26.056825},{\"speed\":10.064865,\"time\":1513478300114,\"longitude\":119.26732,\"latitude\":26.056785},{\"speed\":10.064865,\"time\":1513478301114,\"longitude\":119.26732,\"latitude\":26.056785},{\"speed\":10.151351,\"time\":1513478302115,\"longitude\":119.267408,\"latitude\":26.056745},{\"speed\":10.151351,\"time\":1513478303115,\"longitude\":119.267497,\"latitude\":26.056707},{\"speed\":10.135135,\"time\":1513478304116,\"longitude\":119.267668,\"latitude\":26.056627},{\"speed\":10.135135,\"time\":1513478305117,\"longitude\":119.267668,\"latitude\":26.056627},{\"speed\":9.94054,\"time\":1513478306117,\"longitude\":119.267747,\"latitude\":26.056588},{\"speed\":9.475676,\"time\":1513478307117,\"longitude\":119.267825,\"latitude\":26.056552},{\"speed\":8.929729,\"time\":1513478308118,\"longitude\":119.267975,\"latitude\":26.05648},{\"speed\":8.929729,\"time\":1513478309118,\"longitude\":119.267975,\"latitude\":26.05648},{\"speed\":9.021622,\"time\":1513478310119,\"longitude\":119.268125,\"latitude\":26.056413},{\"speed\":9.021622,\"time\":1513478311120,\"longitude\":119.268125,\"latitude\":26.056413},{\"speed\":9,\"time\":1513478312120,\"longitude\":119.268202,\"latitude\":26.05638},{\"speed\":8.848649,\"time\":1513478313120,\"longitude\":119.268278,\"latitude\":26.056345},{\"speed\":9,\"time\":1513478314121,\"longitude\":119.268355,\"latitude\":26.05631},{\"speed\":9.335135,\"time\":1513478315121,\"longitude\":119.268512,\"latitude\":26.056235},{\"speed\":9.524324,\"time\":1513478316122,\"longitude\":119.268593,\"latitude\":26.056197},{\"speed\":9.524324,\"time\":1513478317122,\"longitude\":119.268593,\"latitude\":26.056197},{\"speed\":9.735135,\"time\":1513478318123,\"longitude\":119.26868,\"latitude\":26.056158},{\"speed\":10.41081,\"time\":1513478319123,\"longitude\":119.268862,\"latitude\":26.056077},{\"speed\":10.41081,\"time\":1513478320124,\"longitude\":119.268862,\"latitude\":26.056077},{\"speed\":10.902702,\"time\":1513478321125,\"longitude\":119.269048,\"latitude\":26.05599},{\"speed\":11.097298,\"time\":1513478322125,\"longitude\":119.269142,\"latitude\":26.055945},{\"speed\":11.151351,\"time\":1513478323126,\"longitude\":119.269237,\"latitude\":26.055902},{\"speed\":11.135135,\"time\":1513478324127,\"longitude\":119.269328,\"latitude\":26.05586},{\"speed\":10.891891,\"time\":1513478325127,\"longitude\":119.269418,\"latitude\":26.055818},{\"speed\":10.691892,\"time\":1513478326128,\"longitude\":119.269507,\"latitude\":26.055778},{\"speed\":10.513514,\"time\":1513478327128,\"longitude\":119.269595,\"latitude\":26.055738},{\"speed\":10.427027,\"time\":1513478328128,\"longitude\":119.269682,\"latitude\":26.055698},{\"speed\":10.281081,\"time\":1513478329129,\"longitude\":119.269767,\"latitude\":26.055655},{\"speed\":10.113514,\"time\":1513478330129,\"longitude\":119.269848,\"latitude\":26.055613},{\"speed\":9.951351,\"time\":1513478331130,\"longitude\":119.26993,\"latitude\":26.055572},{\"speed\":9.951351,\"time\":1513478332131,\"longitude\":119.26993,\"latitude\":26.055572},{\"speed\":9.848649,\"time\":1513478333132,\"longitude\":119.270097,\"latitude\":26.055497},{\"speed\":9.810811,\"time\":1513478334132,\"longitude\":119.27018,\"latitude\":26.05546},{\"speed\":9.827027,\"time\":1513478335133,\"longitude\":119.270263,\"latitude\":26.055422},{\"speed\":9.897297,\"time\":1513478336133,\"longitude\":119.270348,\"latitude\":26.055382},{\"speed\":10.048649,\"time\":1513478337134,\"longitude\":119.270433,\"latitude\":26.05534},{\"speed\":10.221622,\"time\":1513478338134,\"longitude\":119.270522,\"latitude\":26.055298},{\"speed\":10.378378,\"time\":1513478339135,\"longitude\":119.27061,\"latitude\":26.055258},{\"speed\":10.52973,\"time\":1513478340135,\"longitude\":119.270702,\"latitude\":26.055217},{\"speed\":10.648648,\"time\":1513478341135,\"longitude\":119.270792,\"latitude\":26.055175},{\"speed\":10.832433,\"time\":1513478342136,\"longitude\":119.270885,\"latitude\":26.055132},{\"speed\":10.908108,\"time\":1513478343137,\"longitude\":119.270978,\"latitude\":26.055088},{\"speed\":10.85946,\"time\":1513478344137,\"longitude\":119.271072,\"latitude\":26.055047},{\"speed\":10.891891,\"time\":1513478345138,\"longitude\":119.271165,\"latitude\":26.055003},{\"speed\":10.875675,\"time\":1513478346138,\"longitude\":119.271258,\"latitude\":26.054962},{\"speed\":11,\"time\":1513478347138,\"longitude\":119.271352,\"latitude\":26.05492},{\"speed\":11.016216,\"time\":1513478348139,\"longitude\":119.271443,\"latitude\":26.054877},{\"speed\":11.070271,\"time\":1513478349139,\"longitude\":119.271537,\"latitude\":26.054833},{\"speed\":11.081081,\"time\":1513478350140,\"longitude\":119.27163,\"latitude\":26.05479},{\"speed\":11.086487,\"time\":1513478351140,\"longitude\":119.271723,\"latitude\":26.054748},{\"speed\":11.005405,\"time\":1513478352140,\"longitude\":119.271818,\"latitude\":26.054703},{\"speed\":11.05946,\"time\":1513478353141,\"longitude\":119.271913,\"latitude\":26.054662},{\"speed\":11.048649,\"time\":1513478354141,\"longitude\":119.272008,\"latitude\":26.054622},{\"speed\":11.010811,\"time\":1513478355142,\"longitude\":119.272102,\"latitude\":26.054582},{\"speed\":10.929729,\"time\":1513478356142,\"longitude\":119.272195,\"latitude\":26.05454},{\"speed\":10.85946,\"time\":1513478357143,\"longitude\":119.272287,\"latitude\":26.0545},{\"speed\":10.778378,\"time\":1513478358143,\"longitude\":119.272378,\"latitude\":26.054458},{\"speed\":10.713513,\"time\":1513478359144,\"longitude\":119.272468,\"latitude\":26.054417},{\"speed\":10.583784,\"time\":1513478360144,\"longitude\":119.272557,\"latitude\":26.054373},{\"speed\":10.632432,\"time\":1513478361144,\"longitude\":119.272647,\"latitude\":26.05433},{\"speed\":10.654054,\"time\":1513478362145,\"longitude\":119.272735,\"latitude\":26.054287},{\"speed\":10.508108,\"time\":1513478363145,\"longitude\":119.272822,\"latitude\":26.054243},{\"speed\":10.545946,\"time\":1513478364146,\"longitude\":119.27291,\"latitude\":26.0542},{\"speed\":10.52973,\"time\":1513478365146,\"longitude\":119.273,\"latitude\":26.05416}],\"carMeasure\":{\"isRegular\":\"708.6 -1.0\",\"Alongitude\":119.266975,\"Aaltitude\":21.2,\"ScarMeasure\":0,\"Baltitude\":19,\"Bspeed\":10.497297,\"Blongtitde\":119.27309,\"Alatitude\":26.056943,\"Aspeed\":9.929729,\"Blatitude\":26.054123}}";

        int i = 10;
        //while (i > 0) {
        	String re = new ProcessISURequestService().chooseMethod(d.getBytes());
            //System.out.println(re);
            i--;
		//}
        //System.out.println(Utils.stampToDate(Long.toString(Long.parseLong("") / 1000 * 1000)));

		System.out.println(new VerifyAdjustService().queryVerifyTrace(""));
	}
	
	@Test
	public void xmlTest() { 
		System.out.println("检定" + new VerifyAdjustDao().queryVerifyTrace("1616010526880").toString());
		System.out.println(Utils.getDistance(26.0569266667, 119.2669668333, 26.0568971667, 119.2670300000));
		
		Fare fare = new Fare();
		fare.setK("222");
		fare.setAreaId("112");
		System.out.println("fare:" + new Gson().toJson(fare));
	} 
	
	public static class test {

		private int num;
		private String name;
		
		public test(int num, String name) {
			super();
			this.num = num;
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
		@Override
		public String toString() {
			return "test [num=" + num + ", name=" + name + "]";
		}
	}
	@Test
	public void testDB() {
		String data = "{\"evaluate\":\"dissatisfaction\",\"messageSerialNum\":6,\"waitTime\":\"0:0\",\"checkCode\":-34,\"flagBitEnd\":126,\"journey\":\"single\",\"transitionType\":\"cash\",\"ISUFlag_company_2\":16,\"locationTracks\":[null,null,null,null],\"ISUFlag_1\":16,\"ICID\":\"19 59 96 CE\",\"fuelSurcharge\":\"0.0\",\"getOnTime\":\"2017-7-16 21:8\",\"distance\":\"0.0\",\"messageID\":110,\"price\":\"2\",\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"spendTime\":\"0:1\",\"ISUFlag_device_3\":0,\"currentRunTime\":\"610\",\"money\":\"5.0\",\"messageSize\":15,\"getOffTime\":\"2017-7-16 21:9\",\"nullRun\":\"0.0\"}\n";
		String s = "{\"messageID\":114,\"messageSerialNum\":4,\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"checkCode\":-34,\"ISUFlag_device_3\":0,\"flagBitEnd\":126,\"messageSize\":15,\"ISUFlag_company_2\":16,\"locationTracks\":[{\"speed\":3.3,\"time\":4,\"longitude\":1.1,\"latitude\":2.2},{\"speed\":3.3,\"time\":4,\"longitude\":1.1,\"latitude\":2.2},{\"speed\":3.3,\"time\":4,\"longitude\":1.1,\"latitude\":2.2}],\"ISUFlag_1\":16,\"carMeasure\":{\"isRegular\":\"yes\",\"Alongitude\":0,\"Aaltitude\":0,\"ScarMeasure\":0,\"Baltitude\":0,\"Bspeed\":0,\"Blongtitde\":0,\"Alatitude\":0,\"Aspeed\":0,\"Blatitude\":0}}";
		String login = "{\"messageSerialNum\":2,\"direction\":90,\"alarm_byte_1\":0,\"state\":\"login\",\"alarm_byte_2\":0,\"alarm_byte_3\":0,\"ISUFlag_company_2\":16,\"alarm_byte_4\":0,\"messageID\":101,\"ISUFlag_serialNum_4\":10526880,\"ISUFlag_device_3\":0,\"longitude\":116.4166,\"status_3\":0,\"checkCode\":-34,\"speed\":40.1,\"status_4\":0,\"status_1\":0,\"status_2\":0,\"flagBitEnd\":126,\"ISUFlag_1\":16,\"ICID\":\"19 59 96 CE\",\"flagBitBegin\":126,\"loginOutTime\":1500256624976,\"messageSize\":15,\"latitude\":39.9166,\"locationTime\":1500256224975}\n";

		//String string = null;
		System.out.println(new ProcessISURequestService().chooseMethod(login.getBytes()));
		Map<String, Object> map = new HashMap<>();
		List<test> list = new ArrayList<>();
		list.add(new test(1, "zs"));
		list.add(new test(2, "ls"));
		for (int i = 0; i < 80; i++) {
			//list.add(new test(2, "ls"));
		}
		map.put("list", new Gson().toJson(list));
		map.put("key", "value");
		String string = new Gson().toJson(map);
		System.out.println(string);
		List<test> list2 = new Gson().fromJson(new Gson().toJson(list), new TypeToken<List<test>>() {}.getType());
		System.out.println("result" + list2.size());
	}
	
	
	@Test
	public void testGson() {
		String[] s = {"a", "b"};
		List<test> ts = new ArrayList<UnitTest.test>();
/*		for (int i = 0; i < 2; i++) {
			test t = new test();
			t.setNum(i);
			for (int j = 0; j < 2; j++) {
				t.setName(s[j]);
				ts.add(t);
				System.out.println(s[j]);
				System.out.println(t.toString() + t.hashCode());
			}
		}
		System.out.println(ts.toString());*/
		test a = new test(1, "a");
		ts.add(a);
		System.out.println(ts.toString());
		a.setNum(2);
		a.setName("b");
		//ts.add(a);
		System.out.println(ts.toString());
		
		Map<String, test> map = new HashMap<String, test>();
		map.put("1", a);
		System.out.print(map.toString());
		
		a.setNum(3);
		
		System.out.print(map.toString());
		
		System.out.print(new Gson().toJson(new ISUMessage()));
		
/*		a.setName("c");
		ts.add(a);
		System.out.println(ts.toString());*/
/*		List<LocationTracks> locationTracks = new ArrayList<LocationTracks>();
        locationTracks.add(new LocationTracks(1.1, 1.2, 1.3, 1));
        locationTracks.add(new LocationTracks(2.1, 2.2, 2.3, 2));
        //System.out.println(Arrays.toString(cheatMonitors.toArray()));
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("zs", "ls");
        map.put("cheatMonitors", locationTracks);
        String gr = new Gson().toJson(map);
        System.out.println(gr);
        ISUMessage bean = new Gson().fromJson(gr, ISUMessage.class);
        System.out.println("bean:" + bean);
        String re = new Gson().toJson(locationTracks);
        List<LocationTracks> r = new Gson().fromJson(re, new TypeToken<List<LocationTracks>>(){}.getType());
        System.out.println(re + r.toString());
		
		new OperationQueryService().queryCheatMonitor("闂礎 876DW", "2017-6-2 18:6");
		*/
		new RemoteManageService().sendVerifyCommand("101,102");
	}
	
	@Test
	public void testStringToBlob() {
		List<LocationTracks> lTracks = new ArrayList<LocationTracks>();
		lTracks.add(new LocationTracks(119.26715166666666, 26.056871666666666, 1.3, 1));
		lTracks.add(new LocationTracks(119.26790666666666, 26.056516666666667, 2.3, 2));
		lTracks.add(new LocationTracks(119.26884833333334, 26.05608666666667, 3.3, 3));
		lTracks.add(new LocationTracks(119.26979333333333, 26.05563833333333, 3.3, 3));
		String string = new Gson().toJson(lTracks);
		InputStream longitudes = new ByteArrayInputStream(string.getBytes());
		byte[] re = null;
		try {
			con = JdbcUtils.getConnection();
			ps = con.prepareStatement("UPDATE transaction_tb SET locationTracks=? WHERE getOnTime='2017-7-16 21:48'");
			ps.setBinaryStream(1, longitudes, longitudes.available());
			ps.execute();
			ps = con.prepareStatement("SELECT locationTracks FROM transaction_tb WHERE getOnTime='2017-7-16 21:48'");
			rs = ps.executeQuery();
			if(rs.next()) {
				re = new byte[rs.getBinaryStream("locationTracks").available()];
				rs.getBinaryStream("locationTracks").read(re);
			}
			System.out.println(new String(re));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
/*	[37, -28, -70, -78, -25, -120, -79, -25, -102, -124, 42, 119, 111, 108, 101, 103, 101, 113, 117, 97]%浜茬埍鐨�*wolegequa
	@org.junit.Test
	public void test() {

		try {
			con = JdbcUtils.getConnection();
			ps = con.prepareStatement("SELECT license FROM car_tb WHERE ISUFlag=?");
			ps.setString(1, "1616010526880");
			rs = ps.executeQuery();
			System.out.println(rs.getString(0));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		User user = new User("琛屼笟鐩戠", "鏉庡洓", "123456");
		Command c1 = new Command("1", 12, "21", "no", null, "90");
		Command c2 = new Command("2", 12, "21", "no", null, "90");
		List<Command> c = new ArrayList<Command>();
		c.add(c1);
		c.add(c2);
		
		StandardRoute sRoute = new StandardRoute();
		sRoute.setNum(1000002);
		sRoute.setAaltitude(12.1);
		sRoute.setAlongitude(21.1);
		sRoute.setAlatitude(21.21);
		sRoute.setAradius(21.32);
		sRoute.setBlongtitde(32.43);
		sRoute.setBlatitude(32.43);
		sRoute.setBaltitude(32.43);
		sRoute.setBradius(32.54);
		sRoute.setSmeasure(21.2);
		sRoute.setStandard(32.43);
		
		char[] ch = {0x21, 0x22};
		System.out.println(ch);
		ch = null;
		System.out.println(ch == null);
		//new VerifyAdjustDao().modifyStandardRoute(sRoute);
		System.out.println(new VerifyAdjustService().queryCarMeasure().get(0).toString());
//		try {
////			new DriverDao().addDriverPhoto("195996C1");
//			byte[] photo = new DriverDao().queryDriverPhoto("195996C1");
//			if (photo != null) {
//				System.out.println("闀垮害锛�" + photo.length);
//			} else {
//				System.out.println("鏈ㄦ湁鏌ュ埌鍟�");
//			}
//		} catch (SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		ISUMessage i = new ISUMessage();
		i.setAreaId("01");
		//System.out.println(new ProcessISURequestService().queryFare(i));
		
		String ab = "agbgdge ";
		System.out.println(ab.replaceAll("g", " "));
		String ICID = "195996C1";
		String after = ICID.substring(0, 2) + " " + ICID.subSequence(2, 4) + " " + ICID.subSequence(4, 6) + " " + ICID.substring(6, 8);
		System.out.println("after锛�" + after);
		// try {
		// String sql = "insert into user_tb values(?,?,?)";
		// Object[] params = { user.getIdentity(), user.getPassword(),
		// user.getName()};
		// qr.update(sql, params);
		// } catch(SQLException e) {
		// throw new RuntimeException(e);
		// }
        
        Calendar calendar = Calendar.getInstance();
        // 杈撳嚭鏃ユ湡淇℃伅锛岃繕鏈夎澶氬父閲忓瓧娈碉紝鎴戝氨涓嶅啀鍐欏嚭鏉ヤ簡
        System.out.println(calendar.get(Calendar.YEAR) + "骞�"
                                 + calendar.get(Calendar.MONTH) + "鏈�"
                                 + calendar.get(Calendar.DAY_OF_MONTH) + "鏃�"
                                 + calendar.get(Calendar.HOUR_OF_DAY) + "鏃�"
                                 + calendar.get(Calendar.MINUTE) + "鍒�"
                                 + calendar.get(Calendar.SECOND) + "绉�" + "\n浠婂ぉ鏄槦鏈�"
                                 + calendar.get(Calendar.DAY_OF_WEEK) + "鏄粖骞寸殑绗�"
                                 + calendar.get(Calendar.WEEK_OF_YEAR) + "鍛�");
		
		String sql = "SELECT car_tb.license,company_tb.companyName,driver_tb.driverName,transaction_tb.* FROM car_tb,company_tb,driver_tb,transaction_tb" +
				" WHERE car_tb.ISUFlag=transaction_tb.ISUFlag AND car_tb.companyId=company_tb.companyId AND driver_tb.ICID=transaction_tb.ICID AND driverName='璧典竴'";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
		try {
			Date date = simpleDateFormat.parse("23:00:00");
			System.out.println(date.getHours());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		try {
//			System.out.println(qr.query(sql, new BeanListHandler<OperationMsg>(OperationMsg.class)).toString());
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		// try {
		// String sqlQueryAllCar =
		// "SELECT company_tb.companyName,car_tb.license FROM car_tb,company_tb where car_tb.companyId=company_tb.companyId";
		// String sqlOnline =
		// "SELECT car_tb.license,driver_tb.driverName FROM online_tb,driver_tb,car_tb where online_tb.ICID=driver_tb.ICID and car_tb.ISUFlag=online_tb.ISUFlag";
		// String sql =
		// "SELECT car_tb.license,company_tb.companyName,driver_tb.driverName,online_tb.state,online_tb.reportTime "
		// +
		// "FROM car_tb,company_tb,online_tb,driver_tb " +
		// "WHERE car_tb.companyId=company_tb.companyId AND online_tb.ICID=driver_tb.ICID AND car_tb.ISUFlag=online_tb.ISUFlag AND company_tb.companyName='绂忓缓A鍑虹杞﹀叕鍙�'";
		// //String sql = "select * from driver_tb where ? = ?";
		// //Object[] params = { "driverName", "鏉庡洓" };
		// System.out.println(qr.query(sql, new
		// BeanListHandler<RemoteManageQueryTaxi>(RemoteManageQueryTaxi.class)).toString());
		//
		// } catch (SQLException e) {
		// throw new RuntimeException(e);
		// }
		// System.out.println(new RemoteManageService().queryTaxi("companyName",
		// "绂忓缓A鍑虹杞﹀叕鍙�").toString()+"\n"+System.currentTimeMillis());

		List<String> license = new ArrayList<String>();
		license.add("闂礎 876DW");
		//license.add("鎺у埗鎷嶇収");
		//System.out.println(new RemoteManageDao().queryPartISUFlag(license));

		// System.out.println(new RemoteManageDao().addCommand(c));
		// System.out.println(new
		// ProcessISURequestDao().queryCommand(isuMessage).toString());
		// String sql = "SELECT ISUFlag FROM car_tb WHERE license=?";

		// try {
		// String sql =
		// "SELECT publishTime FROM command_tb WHERE isExecute='no' AND ISUFlag=?";
		// Object[] param = {"1616010526881"};
		// System.out.println(qr.query(sql, new MapListHandler(),
		// param).toString());//new BeanListHandler<Command>(Command.class))
		// } catch (SQLException e) {
		// throw new RuntimeException(e);
		// }


		// try {
		// Connection con = JdbcUtils.getConnection();
		// PreparedStatement ps = con.prepareStatement(sql);
		// ps.setString(1, "闂礎 876DW");
		// rs = ps.executeQuery();
		// ps.clearParameters();
		// rs.next();
		// System.out.println(rs.getString(1));
		// } catch (SQLException e1) {
		// e1.printStackTrace();
		// }

		// try {
		// Class.forName(driverClassName);
		// con = DriverManager.getConnection(url, username, password);
		// stmt = con.createStatement();
		//
		// String sql =
		// "SELECT car_tb.license, company_tb.companyName, driver_tb.driverName, online_tb.state , online_tb.reportTime "
		// +
		// "FROM online_tb,driver_tb,car_tb,company_tb " +
		// "WHERE online_tb.ICID=driver_tb.ICID AND car_tb.ISUFlag=online_tb.ISUFlag "
		// +
		// "AND car_tb.companyId=company_tb.companyId";
		// rs = stmt.executeQuery(sql);
		// while (rs.next()) {
		//
		// System.out.println(rs.getString(1)+rs.getString(2)+rs.getString(3)+rs.getString(4)+rs.getString(5));//+rs.getString(3)+rs.getString(4)+rs.getString(5)+rs.getString(6)+rs.getString(7)+rs.getString(8)+rs.getString(9)
		// }
		// } catch (Exception e) {
		// // System.out.print("error");
		// e.printStackTrace();
		// // throw new RuntimeException(e);
		// }

		// List<SendMessageCar> c = new SendCommandDao().queryCar("all", "");
		// for(SendMessageCar s : c){
		// System.out.println(s.getLicense()+s.getCompany()+s.getDriverName()+s.getCarState());
		// }
		// System.out.println("error");
		String res = "{\"messageID\":6,\"messageSerialNum\":70,\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"checkCode\":-34,\"ISUFlag_device_3\":0,\"flagBitEnd\":126,\"messageSize\":15,\"ISUFlag_company_2\":16,\"ISUFlag_1\":16,\"responseSerialNum\":70,\"responseID\":2,\"ICID\":\"19 58 DD CE\",\"state\":\"signOut\",\"loginOutTime\":\"1479997926970\",\"publishiTime\":\"1479979018188\",\"runState\":\"绌洪┒\",\"reportTime\":\"1479979018188\"}";
		//Gson gson = new GsonBuilder().create();
		//ISUMessage isuMessage = gson.fromJson(res, ISUMessage.class);
		//System.out.println(new ProcessISURequestService().chooseMethod(new Gson().fromJson(new String(res), ISUMessage.class)));
		//System.out.println(System.currentTimeMillis());
		//System.out.println(new Gson().fromJson(res, ISUMessage.class));
		//System.out.println(isuMessage.toString());
		String a = "1h:1m:12s";
		//System.out.println(a.replaceAll("[^0-9^.^]", "瑙�"));
		//System.out.println(a.replace("h", "灏忔椂").replace("m", "鍒�").replace("m", "绉�").replace(":", ""));
		

//		try {
//			con = JdbcUtils.getConnection();
//			String sql = "SELECT * FROM command_tb WHERE ISUFlag=?";// and publishTime=? and isExecute=?
//			ps = con.prepareStatement(sql);
//			ps.setString(1, "1616010526880");
////			ps.setString(2, "1479918807205");
////			ps.setString(3, "no");
//			rs = ps.executeQuery();
//			while (rs.next()) {
//				System.out.println(rs.getInt("messageID") + rs.getString("msg"));
//			}
//			 if (rs != null)
//				 rs.close();
//				 if (con != null)
//				 con.close();
//				 if (ps != null)
//				 con.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		

	// String sql = "insert into driver_tb values('" + driver.getDriverName()
	// + "','" + driver.getSex() + "','" + driver.getId() + "','"
	// + driver.getEmployId() + "','" + driver.getEmployCardAgent()
	// + "','" + driver.getTimeBeginEnd() + "','" + driver.getTel()
	// + "','" + driver.getCompanyId() + "','" + driver.getEducation()
	// + "')";
	// List<String> loginMessage = new ArrayList<String>();
	// loginMessage.add("123");
	// loginMessage.add("312");
	// System.out.println(loginMessage.get(0));
	// System.out.println(Integer.toString(111));
	// System.out.println(123);
	// String res =
	// "{\"messageID\":32769,\"messageSerialNum\":70,\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"checkCode\":-34,\"ISUFlag_device_3\":0,\"flagBitEnd\":126,\"messageSize\":15,\"ISUFlag_company_2\":16,\"ISUFlag_1\":16,\"responseSerialNum\":70,\"responseID\":2,\"ICID\":\"19 58 DD CE\",\"state\":\"signOut\",\"loginOutTime\":\"1478404379636\"}";

	// //System.(new ProcessISURequestService().processISUsignOutMessage(new
	// Gson().fromJson(new String(res), ISUMessage.class)));
	long time1 = Long.parseLong("1480325780040");
	long time2 = Long.parseLong("1499239053576");
	//System.out.println("鏃堕棿杞崲" + Utils.stampToDate("1499239045743"));

	// List<Long> list = new ArrayList<Long>();
	// list.add(Long.parseLong("1478349906122"));
	// list.add(Long.parseLong("1478349916122"));
	// System.out.println("鏈�澶у��: " + Collections.max(list));
	// System.out.println("鏈�灏忓��: " + Collections.min(list));

	// try {
	// List<Long> timeStampList = new
	// ProcessISURequestDao().queryLoginOutTime("19 58 DD CE");
	// if (Long.parseLong("1478349225425") - Collections.max(timeStampList) <
	// 60000) {
	// System.out.println(Collections.max(timeStampList));
	// }
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	
	//System.out.println(new LocationRunStateService().queryDetail("闂礎 876DW").toString());
}*/
}
