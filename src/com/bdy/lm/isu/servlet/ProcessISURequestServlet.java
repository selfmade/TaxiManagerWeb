package com.bdy.lm.isu.servlet;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.dao.DriverDao;
import com.bdy.lm.isu.service.ProcessISURequestService;

public class ProcessISURequestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("application/Json;charset=utf-8");
		
		//Gson gson = new Gson();
		ProcessISURequestService processISURequestService = new ProcessISURequestService();

		// System.out.println("信息："+request.getParameter("messageID"));
		ServletInputStream sis = request.getInputStream();
		DataInputStream dataInStream = new DataInputStream(sis);
		byte[] buf = new byte[1024];
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		int n = dataInStream.read(buf);
		while (n != -1) {
			os.write(buf, 0, n);
			n = dataInStream.read(buf);
		}
		byte[] res = os.toByteArray();
		//request.getParameter("ICID");
		//ISUMessage isuMessage = gson.fromJson(new String(res), ISUMessage.class);
		//response.getWriter().print("{\"messageID\":32769,\"messageSerialNum\":70,\"flagBitBegin\":126,\"ISUFlag_serialNum_4\":10526880,\"checkCode\":-34,\"ISUFlag_device_3\":0,\"flagBitEnd\":126,\"messageSize\":15,\"ISUFlag_company_2\":16,\"ISUFlag_1\":16,\"responseSerialNum\":70,\"responseID\":2,\"driverName\":\"liming\",\"driverCompany\":\"福建*#￥%出租车公司\",\"ICID\":\"19 58 DD CE\"}");
		//System.out.println("ceshi"+isuMessage.toString());
		String s = processISURequestService.chooseMethod(res);
		response.getWriter().print(s);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
