package com.bdy.lm.isu.service;

public class MsgID {

	public static final int ISULogin = 101;	
	public static final int ISUExit = 102;
	public static final int ISUQueryCommand = 103;
	public static final int ISUExecuteCommandSuccess = 104;
	public static final int ISUExecuteCommandFail = 105;
	public static final int ISUHeartBit = 106;
	public static final int ISU = 107;
	public static final int takePhoto = 108;
	public static final int sendMsg = 109;
	public static final int ISUTransaction = 110;
	public static final int ISULocation = 111;
	public static final int ISUQueryFare = 112;
	public static final int ISUQueryVerifyRoute = 113;
	public static final int ISUVerifyResult = 114;
	public static final int queryISUParameter = 115;
	public static final int queryTaxiInfo = 116;
	

	public static final int ISUVerify = 117;

}
