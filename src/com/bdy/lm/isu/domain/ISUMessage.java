package com.bdy.lm.isu.domain;

import java.util.List;

import com.bdy.lm.browser.domain.CarMeasure;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.ParameterQuery;
import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.browser.domain.StandardRoute;

public class ISUMessage {
	//设备基本信息
	private int flagBitBegin; //一字节，开始标识位
	private int messageID; //两字节，消息ID
	private int messageSize; // 两字节，消息包大小
	private int ISUFlag_1; // 一字节，ISU标识，第一字节为10
	private int ISUFlag_company_2; // 一字节，ISU标识，厂商编号
	private int ISUFlag_device_3; // 一字节，ISU标识，设备类型
	private int ISUFlag_serialNum_4; // 三字节，ISU标识，序列号
	private int messageSerialNum; //两字节，消息体流水号
	private int checkCode; //一字节，校验码
	private int flagBitEnd; //一字节，结束标识位
	private String ISUFlag;
	
	//登录相关信息
	private String ICID;
	private String loginOutTime;//Unix时间戳
	private String driverName;
	private String driverCompany;
	private String license;
	private String state;
	private String areaId;
	
	//查询指令相关
	private String message;
	private String publishTime;
	
	//心跳相关
	private String reportTime;
	private String runState;
	
	//交易相关
	private String getOnTime;
	private String getOffTime;
    private String money;
    private String price;
    private String distance;
    private String spendTime;
    private String nullRun;
    private String journey;
    private String evaluate;
    private String longitudes;
    private String latitudes;
    
    //新计价器附加
    private String fuelSurcharge;
    private String waitTime;
    private String transitionType;
    private String currentRunTime;
	
	//定位相关信息
    private int status_1;
    private int status_2;
    private int status_3;
    private int status_4;    
    private int alarm_byte_1;
    private int alarm_byte_2;
    private int alarm_byte_3;
    private int alarm_byte_4;   
    private double latitude;
    private double longitude;  
    private double speed;
    private double direction;//0~179，正北为0，正南90
    private String locationTime;//Unix时间戳
    
    //运价相关
    private Fare fare;
    
	private List<StandardRoute> standardRoutes;
	private String standardRouteNum;
	private CarMeasure carMeasure;
	private String verifyTime;
	
	//防作弊
	private List<LocationTracks> locationTracks;
	
	//计价器参数查询
	private String parameter;
	private String taximeterTime;
	private String queryTime;
	

	public String getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(String verifyTime) {
		this.verifyTime = verifyTime;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getTaximeterTime() {
		return taximeterTime;
	}

	public void setTaximeterTime(String taximeterTime) {
		this.taximeterTime = taximeterTime;
	}

	public String getQueryTime() {
		return queryTime;
	}

	public void setQueryTime(String queryTime) {
		this.queryTime = queryTime;
	}

	public List<LocationTracks> getLocationTracks() {
		return locationTracks;
	}

	public void setLocationTracks(List<LocationTracks> locationTracks) {
		this.locationTracks = locationTracks;
	}

	public String getLongitudes() {
		return longitudes;
	}

	public void setLongitudes(String longitudes) {
		this.longitudes = longitudes;
	}

	public String getLatitudes() {
		return latitudes;
	}

	public void setLatitudes(String latitudes) {
		this.latitudes = latitudes;
	}

	public String getStandardRouteNum() {
		return standardRouteNum;
	}

	public void setStandardRouteNum(String standardRouteNum) {
		this.standardRouteNum = standardRouteNum;
	}

	public CarMeasure getCarMeasure() {
		return carMeasure;
	}

	public void setCarMeasure(CarMeasure carMeasure) {
		this.carMeasure = carMeasure;
	}

	public List<StandardRoute> getStandardRoutes() {
		return standardRoutes;
	}

	public void setStandardRoutes(List<StandardRoute> standardRoutes) {
		this.standardRoutes = standardRoutes;
	}

	public Fare getFare() {
		return fare;
	}

	public void setFare(Fare fare) {
		this.fare = fare;
	}

	public String getGetOnTime() {
        return getOnTime;
    }

    public void setGetOnTime(String getOnTime) {
        this.getOnTime = getOnTime;
    }

    public String getGetOffTime() {
        return getOffTime;
    }

    public void setGetOffTime(String getOffTime) {
        this.getOffTime = getOffTime;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSpendTime() {
        return spendTime;
    }

    public void setSpendTime(String spendTime) {
        this.spendTime = spendTime;
    }

    public String getJourney() {
        return journey;
    }

    public void setJourney(String journey) {
        this.journey = journey;
    }

    public String getNullRun() {
        return nullRun;
    }

    public void setNullRun(String nullRun) {
        this.nullRun = nullRun;
    }
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	public String getRunState() {
		return runState;
	}
	public void setRunState(String runState) {
		this.runState = runState;
	}

	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getFlagBitBegin() {
		return flagBitBegin;
	}
	public void setFlagBitBegin(int flagBitBegin) {
		this.flagBitBegin = flagBitBegin;
	}
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public int getMessageSize() {
		return messageSize;
	}
	public void setMessageSize(int messageSize) {
		this.messageSize = messageSize;
	}
	public int getISUFlag_1() {
		return ISUFlag_1;
	}
	public void setISUFlag_1(int iSUFlag_1) {
		ISUFlag_1 = iSUFlag_1;
	}
	public int getISUFlag_company_2() {
		return ISUFlag_company_2;
	}
	public void setISUFlag_company_2(int iSUFlag_company_2) {
		ISUFlag_company_2 = iSUFlag_company_2;
	}
	public int getISUFlag_device_3() {
		return ISUFlag_device_3;
	}
	public void setISUFlag_device_3(int iSUFlag_device_3) {
		ISUFlag_device_3 = iSUFlag_device_3;
	}
	public int getMessageSerialNum() {
		return messageSerialNum;
	}
	public void setMessageSerialNum(int messageSerialNum) {
		this.messageSerialNum = messageSerialNum;
	}
	public int getCheckCode() {
		return checkCode;
	}
	public void setCheckCode(int checkCode) {
		this.checkCode = checkCode;
	}
	public int getFlagBitEnd() {
		return flagBitEnd;
	}
	public void setFlagBitEnd(int flagBitEnd) {
		this.flagBitEnd = flagBitEnd;
	}
	
	

	public String getISUFlag() {
		return ISUFlag;
	}

	public void setISUFlag(String iSUFlag) {
		ISUFlag = iSUFlag;
	}

	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public String getICID() {
		return ICID;
	}
	public void setICID(String iCID) {
		ICID = iCID;
	}

	public String getLoginOutTime() {
		return loginOutTime;
	}
	public void setLoginOutTime(String loginOutTime) {
		this.loginOutTime = loginOutTime;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getDriverCompany() {
		return driverCompany;
	}
	public void setDriverCompany(String driverCompany) {
		this.driverCompany = driverCompany;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	

	public int getStatus_1() {
		return status_1;
	}
	public void setStatus_1(int status_1) {
		this.status_1 = status_1;
	}
	public int getStatus_2() {
		return status_2;
	}
	public void setStatus_2(int status_2) {
		this.status_2 = status_2;
	}
	public int getStatus_3() {
		return status_3;
	}
	public void setStatus_3(int status_3) {
		this.status_3 = status_3;
	}
	public int getStatus_4() {
		return status_4;
	}
	public void setStatus_4(int status_4) {
		this.status_4 = status_4;
	}
	public int getAlarm_byte_1() {
		return alarm_byte_1;
	}
	public void setAlarm_byte_1(int alarm_byte_1) {
		this.alarm_byte_1 = alarm_byte_1;
	}
	public int getAlarm_byte_2() {
		return alarm_byte_2;
	}
	public void setAlarm_byte_2(int alarm_byte_2) {
		this.alarm_byte_2 = alarm_byte_2;
	}
	public int getAlarm_byte_3() {
		return alarm_byte_3;
	}
	public void setAlarm_byte_3(int alarm_byte_3) {
		this.alarm_byte_3 = alarm_byte_3;
	}
	public int getAlarm_byte_4() {
		return alarm_byte_4;
	}
	public void setAlarm_byte_4(int alarm_byte_4) {
		this.alarm_byte_4 = alarm_byte_4;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getLocationTime() {
		return locationTime;
	}
	public void setLocationTime(String locationTime) {
		this.locationTime = locationTime;
	}
	public int getISUFlag_serialNum_4() {
		return ISUFlag_serialNum_4;
	}

	public void setISUFlag_serialNum_4(int iSUFlag_serialNum_4) {
		ISUFlag_serialNum_4 = iSUFlag_serialNum_4;
	}

	public String getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
    public String getFuelSurcharge() {
		return fuelSurcharge;
	}

	public void setFuelSurcharge(String fuelSurcharge) {
		this.fuelSurcharge = fuelSurcharge;
	}

	public String getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}

	public String getTransitionType() {
		return transitionType;
	}

	public void setTransitionType(String transitionType) {
		this.transitionType = transitionType;
	}

	public String getCurrentRunTime() {
		return currentRunTime;
	}

	public void setCurrentRunTime(String currentRunTime) {
		this.currentRunTime = currentRunTime;
	}

	@Override
	public String toString() {
		return "ISUMessage [messageID=" + messageID + ", ISUFlag=" + ISUFlag + ", ICID=" + ICID + ", driverName="
				+ driverName + ", driverCompany=" + driverCompany + ", license=" + license + ", message=" + message
				+ ", runState=" + runState + ", getOnTime=" + getOnTime + ", getOffTime=" + getOffTime + ", money="
				+ money + ", price=" + price + ", distance=" + distance + ", spendTime=" + spendTime + ", nullRun="
				+ nullRun + ", journey=" + journey + ", evaluate=" + evaluate + ", fuelSurcharge=" + fuelSurcharge
				+ ", waitTime=" + waitTime + ", transitionType=" + transitionType + ", currentRunTime=" + currentRunTime
				+ ", carMeasure=" + carMeasure + ", parameter=" + parameter + ", taximeterTime=" + taximeterTime
				+ ", queryTime=" + queryTime + "]";
	}

}
