package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.domain.LocationRunState;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.service.LocationRunStateService;

import cn.itcast.servlet.BaseServlet;

public class LocationRunStateServlet extends BaseServlet {

	private LocationRunStateService locationRunStateService = new LocationRunStateService();
	
	public void queryTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("queryContent").equals("") && !request.getParameter("queryMode").equals("all")) {
			request.setAttribute("locationRunStateMsg", "请输入要查询的内容！！");
			request.setAttribute("traceReplayMsg", "请输入要查询的内容！！");
		} else {
			List<LocationRunState> locationRunState = locationRunStateService.queryTaxi(request.getParameter("queryMode"), request.getParameter("queryContent"));
			if(locationRunState.size() > 0) {
				request.setAttribute("locationRunState", locationRunState);
			} else {
				request.setAttribute("locationRunStateMsg", "您要查询您的内容不存在！");
				request.setAttribute("traceReplayMsg", "您要查询您的内容不存在！！");
			}
		}
		request.setAttribute("queryContent", request.getParameter("queryContent"));
		request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void queryDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String license = request.getParameter("license");
		LocationRunState locationRunState = locationRunStateService.queryDetail(license);
		if (locationRunState == null) {
			request.setAttribute("locationRunStateMsg", "未查询到相关数据！！");
			request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		} else {
			request.setAttribute("locationRunState", locationRunState);
			request.setAttribute("longitude", locationRunState.getLongitude());
			request.setAttribute("latitude", locationRunState.getLatitude());
			//System.out.println(locationRunState.getLocationTime());
			request.getRequestDispatcher("/home/locationRunStateDetail.jsp").forward(request, response);
		}
	}
	
	public void traceReplay(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String license = request.getParameter("license");
		String traceTimeBegin = request.getParameter("traceTimeBegin");
		String traceTimeEnd = request.getParameter("traceTimeEnd");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			long timeBegin = format.parse(traceTimeBegin).getTime();
			long timeEnd = format.parse(traceTimeEnd).getTime();
			if (timeBegin > timeEnd) {//开始时间如果不小于结束时间，转回主页
				request.setAttribute("traceReplayMsg", "开始时间必须小于结束时间！！！");
				request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
				request.getRequestDispatcher("/home/home.jsp").forward(request, response);
			} else {
				List<LocationTracks> traceReplayPoint = locationRunStateService.traceReplay(license, timeBegin, timeEnd);
				if (traceReplayPoint == null) {//查询到的数据为空，转回主页
					request.setAttribute("traceReplayMsg", "未查询到相关数据！！");
					request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
					request.getRequestDispatcher("/home/home.jsp").forward(request, response);
				} else {
					request.setAttribute("traceReplayPoint", traceReplayPoint);//查询到数据后转到traceReplay.jsp
					request.setAttribute("lengthPoint", traceReplayPoint.size());
					request.getRequestDispatcher("/home/traceReplay.jsp").forward(request, response);
				}
			}
		} catch (ParseException e) {
			request.setAttribute("traceReplayMsg", "时间转换出错！！！");//request，转回主页
			request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
}
