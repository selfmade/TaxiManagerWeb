package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.domain.ParameterQuery;
import com.bdy.lm.browser.service.ParameterQueryService;

public class ParameterQueryServlet extends HttpServlet {

	private ParameterQueryService parameterQueryService = new ParameterQueryService();
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String queryMode = request.getParameter("queryMode");
		String queryContent = request.getParameter("queryContent");
		if (queryContent.equals("") && !queryMode.equals("all")) {
			request.setAttribute("parameterMsg", "请输入要查询的内容！！！");
		} else {
			List<ParameterQuery> parameterQueries = parameterQueryService.queryParameter(queryMode, queryContent);
			System.out.println(parameterQueries);
			if (parameterQueries.toString() == "[]") {
				request.setAttribute("parameterMsg", "无此记录！！！");
			} else {
				request.setAttribute("parameterQueries", parameterQueries);
			}
		}

		// System.out.println(driver.toString());
		request.setAttribute("pageLocation", 19);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}

}
