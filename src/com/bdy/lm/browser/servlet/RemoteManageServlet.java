package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.servlet.BaseServlet;

import com.bdy.lm.browser.domain.RemoteManageQueryTaxi;
import com.bdy.lm.browser.service.RemoteManageService;

public class RemoteManageServlet extends BaseServlet {

	private RemoteManageService remoteManageService = new RemoteManageService();
	
	public void queryTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("queryContent").equals("") && !request.getParameter("queryMode").equals("all")) {
			request.setAttribute("remoteManageMsg", "请输入要查询的内容！！");//远程管理和发送信息两者共用
			request.setAttribute("sendMsgMsg", "请输入要查询的内容！！");
		} else {
			List<RemoteManageQueryTaxi> taxiManage = remoteManageService.queryTaxi(request.getParameter("queryMode"), request.getParameter("queryContent"));
			if(taxiManage.size() > 0) {
				request.setAttribute("taxiManage", taxiManage);
			} else {
				request.setAttribute("remoteManageMsg", "您要查询您的内容不存在！");
				request.setAttribute("sendMsgMsg", "您要查询您的内容不存在！");
			}
		}
		request.setAttribute("queryContent", request.getParameter("queryContent"));
		request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void sendCommand(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int messageID = Integer.parseInt(request.getParameter("msgID").toString());
		String sendType = (String) request.getParameter("sendType");
		System.out.println("sendType:"+sendType);
		List<String> license = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			license.add(request.getParameter(Integer.toString(i)));
			//System.out.println("testLicense:"+request.getParameter(Integer.toString(i)));
			i++;
		}
		if(remoteManageService.sendCommand(messageID, license, "", sendType)) {
			request.setAttribute("remoteManageMsg", "发送成功！");
		} else {
			request.setAttribute("remoteManageMsg", "发送失败！");
		}
		request.setAttribute("pageLocation", 11);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}

	public void sendMsg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String sendType = (String) request.getParameter("sendType");
		int messageID = Integer.parseInt(request.getParameter("msgID").toString());
		String msg = (String) request.getParameter("message");
		List<String> license = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			license.add(request.getParameter(Integer.toString(i)));
			// System.out.println("testLicense:"+request.getParameter(Integer.toString(i)));
			i++;
		}
		if(remoteManageService.sendCommand(messageID, license, msg, sendType)) {
			request.setAttribute("sendMsgMsg", "发送成功！");
		} else {
			request.setAttribute("sendMsgMsg", "发送失败！");
		}
		request.setAttribute("pageLocation", 16);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void queryHistoryMsg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("historyMsg", remoteManageService.queryHistoryMsg());
		request.getRequestDispatcher("/home/historyMsg.jsp").forward( request, response);
	}
	public void queryExecute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<String> msg = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			msg.add(request.getParameter(Integer.toString(i)));
			System.out.println("testMsg:"+request.getParameter(Integer.toString(i)));
			i++;
		}
		request.setAttribute("noExecute", remoteManageService.queryExecute(msg));
		request.getRequestDispatcher("/home/executeState.jsp").forward( request, response);
	}
	
}
