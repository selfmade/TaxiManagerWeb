package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.servlet.BaseServlet;

import com.bdy.lm.browser.domain.CarMeasure;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.StandardRoute;
import com.bdy.lm.browser.service.RemoteManageService;
import com.bdy.lm.browser.service.VerifyAdjustService;

public class VerifyAdjustServlet extends BaseServlet {

	private VerifyAdjustService verifyAdjustService = new VerifyAdjustService();
	private RemoteManageService remoteManageService = new RemoteManageService();
	
	public void queryStandardRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<StandardRoute> standardRoutes = verifyAdjustService.queryStandardRoutes();
		if (standardRoutes.size() == 0) {
			request.setAttribute("verifyAdjustMsg", "无记录");
		} else {
			//System.out.println(standardRoutes.get(0));
			request.setAttribute("standardRoutes", standardRoutes);
		}
		request.setAttribute("pageLocation", 17);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void addStandardRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		StandardRoute standardRoute = new StandardRoute();
		standardRoute.setAlongitude(Double.valueOf(request.getParameter("Alongitude")));
		standardRoute.setAlatitude(Double.valueOf(request.getParameter("Alatitude")));
		standardRoute.setAaltitude(Double.valueOf(request.getParameter("Aaltitude")));
		standardRoute.setAradius(Double.valueOf(request.getParameter("Aradius")));
		standardRoute.setBlongtitde(Double.valueOf(request.getParameter("Blongtitde")));
		standardRoute.setBlatitude(Double.valueOf(request.getParameter("Blatitude")));
		standardRoute.setBaltitude(Double.valueOf(request.getParameter("Baltitude")));
		standardRoute.setBradius(Double.valueOf(request.getParameter("Bradius")));
		standardRoute.setSmeasure(Double.valueOf(request.getParameter("Smeasure")));
		//standardRoute.setStandard(Double.valueOf(request.getParameter("standard")));
		//System.out.println(standardRoute.toString());
		if (verifyAdjustService.addStandardRoute(standardRoute)) {
			request.setAttribute("verifyAdjustMsg", "添加成功！！！");
		} else {
			request.setAttribute("verifyAdjustMsg", "添加失败！！！");
		}
		request.setAttribute("form", standardRoute);
		request.getRequestDispatcher("/baseInfo/addStandardRoute.jsp").forward(request, response);
	}
	
	public void editStandardRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("form", verifyAdjustService.editStandardRoute(Integer.valueOf(request.getParameter("num"))));
		request.getRequestDispatcher("/baseInfo/editStandardRoute.jsp").forward(request, response);
	}
	
	public void modifyStandardRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		StandardRoute standardRoute = new StandardRoute();
		standardRoute.setNum(Integer.valueOf(request.getParameter("num")));
		standardRoute.setAlongitude(Double.valueOf(request.getParameter("Alongitude")));
		standardRoute.setAlatitude(Double.valueOf(request.getParameter("Alatitude")));
		standardRoute.setAaltitude(Double.valueOf(request.getParameter("Aaltitude")));
		standardRoute.setAradius(Double.valueOf(request.getParameter("Aradius")));
		standardRoute.setBlongtitde(Double.valueOf(request.getParameter("Blongtitde")));
		standardRoute.setBlatitude(Double.valueOf(request.getParameter("Blatitude")));
		standardRoute.setBaltitude(Double.valueOf(request.getParameter("Baltitude")));
		standardRoute.setBradius(Double.valueOf(request.getParameter("Bradius")));
		standardRoute.setSmeasure(Double.valueOf(request.getParameter("Smeasure")));

		if (verifyAdjustService.modifyStandardRoute(standardRoute)) {
			request.setAttribute("verifyAdjustMsg", "修改成功");
		} else {
			request.setAttribute("verifyAdjustMsg", "修改失败");
		}
		request.setAttribute("form", standardRoute);
		request.getRequestDispatcher("/baseInfo/editStandardRoute.jsp").forward(request, response);
	}
	
	public void deleteStandardRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Integer> nums = new ArrayList<Integer>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			nums.add(Integer.valueOf(request.getParameter(Integer.toString(i))));
			i++;
		}
		if (verifyAdjustService.deleteStandardRoute(nums)) {
			request.setAttribute("standardRoutes", verifyAdjustService.queryStandardRoutes());
			request.setAttribute("verifyAdjustMsg", "删除成功！！！");
			request.setAttribute("pageLocation", 17);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
	
	public void sendToISU(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String num = request.getParameter("num");
		System.out.println("sendToISU" + num);
		if (remoteManageService.sendVerifyCommand(num)) {
			request.setAttribute("standardRoutes", verifyAdjustService.queryStandardRoutes());
			request.setAttribute("verifyAdjustMsg", "发送成功！！！");
			request.setAttribute("pageLocation", 17);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
	
	public void queryCarMeasure(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<CarMeasure> carMeasures = verifyAdjustService.queryCarMeasure();
		//System.out.println(carMeasures.get(0).toString());
		if (carMeasures.size() == 0) {
			request.setAttribute("carMeasureMsg", "无记录");
		} else {
			request.setAttribute("carMeasures", carMeasures);
		}
		request.setAttribute("pageLocation", 18);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void queryVerifyTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String verifyTime = request.getParameter("verifyTime");
		//System.out.println("verifyTime queryVerifyTrace" + verifyTime);
		List<LocationTracks> locationTracks = verifyAdjustService.queryVerifyTrace(verifyTime);
		if (locationTracks != null && locationTracks.size() != 0) {
			request.setAttribute("traceReplayPoint", locationTracks);//查询到数据后转到traceReplay.jsp
			request.setAttribute("lengthPoint", locationTracks.size());
			request.getRequestDispatcher("/home/traceReplay.jsp").forward(request, response);
		} else {
			request.setAttribute("carMeasureMsg", "无记录！！");
			request.setAttribute("pageLocation", 18);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
}
