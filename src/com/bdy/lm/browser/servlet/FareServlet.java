package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;

import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.browser.service.FareService;

public class FareServlet extends BaseServlet {

	private FareService fareService = new FareService();
	
	public void queryFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String queryMode = request.getParameter("queryMode");
		String queryContent = request.getParameter("queryContent");
		if (queryContent.equals("") && !queryMode.equals("all")) {
			request.setAttribute("fareMsg", "请输入要查询的内容！！！");
		} else {
			List<Fare> fares = fareService.queryFare(queryMode, queryContent);
			if (fares.toString() == "[]") {
				request.setAttribute("fareMsg", "无此记录！！！");
			} else {
				request.setAttribute("fare", fares);
			}
		}
		request.setAttribute("pageLocation", 4);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void addFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Fare form = CommonUtils.toBean(request.getParameterMap(), Fare.class);
		//System.out.println("表单信息：" + form.toString());
		if (fareService.addFare(form)) {
			request.setAttribute("fareMsg", "添加成功！！！");
			//System.out.println("成功！！"+request.getContextPath());
		} else {
			//System.out.println("失败!!");
			request.setAttribute("fareMsg", "运价数据已存在！！！");
		}
		request.setAttribute("form", form);
		request.getRequestDispatcher("/baseInfo/addFare.jsp").forward(request, response);
	}
	
	public void editFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("form", fareService.editFare(request.getParameter("updateTime")));
		//response.sendRedirect("/TaxiManagerServer/baseInfo/editDriver.jsp");//怎样打开新的网页？？
		request.getRequestDispatcher("/baseInfo/editFare.jsp").forward(request, response);
	}
	
	public void modifyFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Fare form = CommonUtils.toBean(request.getParameterMap(), Fare.class);
		String updateTime = fareService.modifyFare(form);
		if(!updateTime.equals("")) {
			request.setAttribute("fareMsg", "修改成功");
			form.setUpdateTime(updateTime);
		} else {
			request.setAttribute("fareMsg", "修改失败");
		}
		request.setAttribute("form", form);
		request.getRequestDispatcher("/baseInfo/editFare.jsp").forward(request, response);
	}
	
	public void deleteFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> updateTimes = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			updateTimes.add(request.getParameter(Integer.toString(i)));
			i++;
		}
		if (fareService.deleteFare(updateTimes)) {
			request.setAttribute("fareMsg", "删除成功！！！");
			request.setAttribute("pageLocation", 4);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
}
