package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.domain.Driver;
import com.bdy.lm.browser.domain.Taxi;
import com.bdy.lm.browser.service.DriverService;
import com.bdy.lm.browser.service.TaxiService;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;

public class TaxiServlet extends BaseServlet {

	private TaxiService taxiService = new TaxiService();
	
	public void queryTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// System.out.println("QueryDriverServlet");

		String queryMode = request.getParameter("queryMode");
		String queryContent = request.getParameter("queryContent");
		if (queryContent.equals("")) {
			request.setAttribute("taxiMsg", "请输入要查询的内容！！！");
		} else {
			List<Taxi> taxi = taxiService.queryTaxi(queryMode, queryContent);
			if (taxi.toString() == "[]") {
				request.setAttribute("taxiMsg", "无此记录！！！");
			} else {
				request.setAttribute("taxi", taxi);
			}
		}

		// System.out.println(driver.toString());
		request.setAttribute("pageLocation", 2);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}

	public void addTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Taxi form = CommonUtils.toBean(request.getParameterMap(), Taxi.class);
		System.out.println("表单信息：" + form.toString());
		if (taxiService.addTaxi(form)) {
			request.setAttribute("taxiMsg", "添加成功！！！");
			request.setAttribute("form", form);
			request.getRequestDispatcher("/baseInfo/addTaxi.jsp").forward(request, response);
			// System.out.println("成功！！"+request.getContextPath());
		} else {
			//System.out.println("失败!!");
			request.setAttribute("taxiMsg", "用户数据已存在！！！");
			request.setAttribute("form", form);
			request.getRequestDispatcher("/baseInfo/addTaxi.jsp").forward(request, response);
		}
	}
	
	public void deleteTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> license = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			license.add(new String(request.getParameter(Integer.toString(i)).getBytes("ISO-8859-1"), "UTF-8"));
			// System.out.println("GETid:"+i+"|"+Integer.toString(i)+
			// request.getParameter(Integer.toString(i)));
			i++;
		}
		if (taxiService.deleteTaxi(license)) {
			request.setAttribute("taxiMsg", "删除成功！！！");
			request.setAttribute("pageLocation", 2);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
	
	public void editTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("form", taxiService.editTaxi(new String(request.getParameter("license").getBytes("ISO-8859-1"), "UTF-8")));
		//response.sendRedirect("/TaxiManagerServer/baseInfo/editDriver.jsp");//怎样打开新的网页？？
		request.getRequestDispatcher("/baseInfo/editTaxi.jsp").forward(request, response);
	}
	
	public void modifyTaxi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Taxi form = CommonUtils.toBean(request.getParameterMap(), Taxi.class);
		//System.out.println("表单信息：" + form.toString());
		if(taxiService.modifyTaxi(form)) {
			request.setAttribute("form", form);
			request.setAttribute("taxiMsg", "修改成功！！！");
		} else {
			request.setAttribute("form", form);
			request.setAttribute("taxiMsg", "修改失败，车牌号不存在！！！");
		}
		request.getRequestDispatcher("/baseInfo/editTaxi.jsp").forward(request, response);
	}
}
