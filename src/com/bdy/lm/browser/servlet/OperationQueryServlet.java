package com.bdy.lm.browser.servlet;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.OperationMsg;
import com.bdy.lm.browser.service.OperationQueryService;
import com.mchange.v2.c3p0.impl.NewPooledConnection;

import cn.itcast.servlet.BaseServlet;

public class OperationQueryServlet extends BaseServlet {
	
	private OperationQueryService operationQueryService = new OperationQueryService();
	public void queryOperation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("queryContent").equals("") && !request.getParameter("queryMode").equals("all")) {
			request.setAttribute("operationQueryMsg", "请输入要查询的内容！！");
		} else {
			List<OperationMsg> operationMsgs = operationQueryService.queryOperation(request.getParameter("queryMode"), request.getParameter("queryContent"));
			if(operationMsgs.size() > 0) {
				request.setAttribute("operationMsgs", operationMsgs);
			} else {
				request.setAttribute("operationQueryMsg", "您要查询您的内容不存在！");
			}
		}
		request.setAttribute("queryContent", request.getParameter("queryContent"));
		request.setAttribute("pageLocation", Integer.parseInt(request.getParameter("pageLocation").toString()));
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void queryCheatMonitor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String license = new String(request.getParameter("license").getBytes("iso8859-1"), "utf-8");
		//String license = URLDecoder.decode(request.getParameter("license"), "utf-8");
		String getOnTime = request.getParameter("getOnTime");
		System.out.println("2车牌" + license + "  getOnTime:" + getOnTime + "  license:" + license);
		List<LocationTracks> locationTracks = operationQueryService.queryCheatMonitor(license, getOnTime);
		if (locationTracks != null && locationTracks.size() != 0) {
			request.setAttribute("traceReplayPoint", locationTracks);//查询到数据后转到traceReplay.jsp
			request.setAttribute("lengthPoint", locationTracks.size());
			request.getRequestDispatcher("/home/traceReplay.jsp").forward(request, response);
		} else {
			request.setAttribute("operationQueryMsg", "无记录！！");
			request.setAttribute("pageLocation", 8);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
}
