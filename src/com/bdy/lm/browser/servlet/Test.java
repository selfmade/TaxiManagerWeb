package com.bdy.lm.browser.servlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class Test extends HttpServlet {

    private static final String GIF = "image/gif;charset=GB2312";// 设定输出的类型

    private static final String JPG = "image/jpeg;charset=GB2312";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		//request.setCharacterEncoding("ISO-8859-1");
//		request.setCharacterEncoding("utf-8");
//		String[] title = {"姓名","性别","民族","年龄","爱好","癖好","热爱"};
//		List<String[]> data = new ArrayList<String[]>();
//		data.add(new String[]{"02","张三","37132619910620047X","235647","1211","7年","13145678877","121","专科","B"});
//		data.add(new String[]{"03","李四","37132619910620047X","235874","1241","6年","13145678876","121","专科","C"});
//		data.add(new String[]{"04","王五","37132619910620047X","235725","1215","5年","13145678879","121","专科","D"});
//		request.setAttribute("title", title);
//		request.setAttribute("data", data);
//		//request.getRequestDispatcher("/home/home.jsp").forward( request, response);
//		String s = request.getParameter("license");
//        s = new String(s.getBytes("ISO-8859-1"), "UTF-8");
//		System.out.println("id"+s);
		
		//System.out.println("ICID" + request.getParameter("ICID"));
        String imagePath = "C:\\Users\\Administrator\\Desktop\\Tempt\\IMG_8320_副本.jpg";
        FileInputStream fis = new FileInputStream(imagePath);
        int size =fis.available();//得到文件大小
        byte data[]=new byte[size];
        fis.read(data);//读数据
        fis.close();
        //response.setContentType("image/gif"); //设置返回的文件类型
        OutputStream os = response.getOutputStream();
        os.write(data);
        os.flush();
        os.close();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
  
        // 创建文件项目工厂对象  
        DiskFileItemFactory factory = new DiskFileItemFactory();  
  
        // 设置文件上传路径  
        String upload = this.getServletContext().getRealPath("/upload/");  
        // 获取系统默认的临时文件保存路径，该路径为Tomcat根目录下的temp文件夹  
        String temp = System.getProperty("java.io.tmpdir");  
        // 设置缓冲区大小为 5M  
        factory.setSizeThreshold(1024 * 1024 * 5);  
        // 设置临时文件夹为temp  
        factory.setRepository(new File(temp));  
        // 用工厂实例化上传组件,ServletFileUpload 用来解析文件上传请求  
        ServletFileUpload servletFileUpload = new ServletFileUpload(factory);  
  
        // 解析结果放在List中  
        try {  
            List<FileItem> list = servletFileUpload.parseRequest(request);  
  
            for (FileItem item : list) {  
                String name = item.getFieldName();  
                InputStream is = item.getInputStream();  
  
                if (name.contains("content")){  
                    System.out.println(inputStream2String(is));  
                } else if(name.contains("file")) {  
                    try {  
                        inputStream2File(is, upload + "\\" + item.getName());  
                    } catch (Exception e) {  
                        e.printStackTrace();  
                    }  
                }  
            }  
              
            out.write("success");  
        } catch (FileUploadException e) {  
            e.printStackTrace();  
            out.write("failure");  
        }  
  
        out.flush();  
        out.close();  
    }  
  
    // 流转化成字符串  
    public static String inputStream2String(InputStream is) throws IOException {  
        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        int i = -1;  
        while ((i = is.read()) != -1) {  
            baos.write(i);  
        }  
        return baos.toString();  
    }  
  
    // 流转化成文件  
    public static void inputStream2File(InputStream is, String savePath) throws Exception {  
        System.out.println("文件保存路径为:" + savePath);  
        File file = new File(savePath);  
        InputStream inputSteam = is;  
        BufferedInputStream fis = new BufferedInputStream(inputSteam);  
        FileOutputStream fos = new FileOutputStream(file);  
        int f;  
        while ((f = fis.read()) != -1) {  
            fos.write(f);  
        }  
        fos.flush();  
        fos.close();  
        fis.close();  
        inputSteam.close();
    }  
	
}

