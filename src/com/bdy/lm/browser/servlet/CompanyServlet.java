package com.bdy.lm.browser.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;

import com.bdy.lm.browser.domain.Company;
import com.bdy.lm.browser.domain.Driver;
import com.bdy.lm.browser.service.CompanyService;

public class CompanyServlet extends BaseServlet {

	private CompanyService companyService = new CompanyService();
	
	public void queryCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("QueryDriverServlet");

		String queryMode = request.getParameter("queryMode");
		String queryContent = request.getParameter("queryContent");
		if (queryContent.equals("")) {
			request.setAttribute("companyMsg", "请输入要查询的内容！！！");
		} else {
			List<Company> company = companyService.queryCompany(queryMode, queryContent);
			if (company.toString() == "[]") {
				request.setAttribute("companyMsg", "无此记录！！！");
			} else {
				request.setAttribute("company", company);
			}
		}
		
		//System.out.println(driver.toString());
		request.setAttribute("pageLocation", 3);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void addCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Company form = CommonUtils.toBean(request.getParameterMap(), Company.class);
		System.out.println("表单信息：" + form.toString());
		if (companyService.addCompany(form)) {
			request.setAttribute("companyMsg", "添加成功！！！");
			//System.out.println("成功！！"+request.getContextPath());
		} else {
			//System.out.println("失败!!");
			request.setAttribute("companyMsg", "企业数据已存在！！！");
		}
		request.setAttribute("form", form);
		request.getRequestDispatcher("/baseInfo/addCompany.jsp").forward(request, response);
	}
	
	public void deleteCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> CompanyId = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			CompanyId.add(request.getParameter(Integer.toString(i)));
			// System.out.println("GETid:"+i+"|"+Integer.toString(i)+
			// request.getParameter(Integer.toString(i)));
			i++;
		}
		if (companyService.deleteCompany(CompanyId)) {
			request.setAttribute("companyMsg", "删除成功！！！");
			request.setAttribute("pageLocation", 3);
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
	
	public void editCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("form", companyService.editCompany(request.getParameter("companyId")));
		//response.sendRedirect("/TaxiManagerServer/baseInfo/editDriver.jsp");//怎样打开新的网页？？
		request.getRequestDispatcher("/baseInfo/editCompany.jsp").forward(request, response);
	}
	
	public void modifyCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Company form = CommonUtils.toBean(request.getParameterMap(), Company.class);
		System.out.println("表单信息：" + form.toString());
		if(companyService.modifyCompany(form)) {
			request.setAttribute("form", form);
			request.setAttribute("companyMsg", "修改成功！！！");
		} else {
			request.setAttribute("form", form);
			request.setAttribute("companyMsg", "修改失败，企业代码不存在！！！");
		}
		request.getRequestDispatcher("/baseInfo/editCompany.jsp").forward(request, response);
	}
}
