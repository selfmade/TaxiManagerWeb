package com.bdy.lm.browser.servlet;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;

import com.bdy.lm.browser.dao.DriverDao;
import com.bdy.lm.browser.domain.Driver;
import com.bdy.lm.browser.service.DriverService;

public class DriverServlet extends BaseFileServlet {

	private DriverService driverService = new DriverService();
	
	public void queryDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("QueryDriverServlet");

		String queryMode = request.getParameter("queryMode");
		String queryContent = request.getParameter("queryContent");
		List<Driver> drivers = null;
		if (queryContent.equals("")) {
			request.setAttribute("driverMsg", "请输入要查询的内容！！！");
		} else {
			drivers= driverService.queryDriver(queryMode, queryContent);
			if (drivers.size() == 0) {
				request.setAttribute("driverMsg", "无此记录！！！");
			} else {
				request.setAttribute("drivers", drivers);
			}
		}
		response.setContentType("text/html;charset=UTF-8");//处理响应编码
		request.setAttribute("pageLocation", 1);
		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
	}
	
	public void addDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//form含有InputStream，成功转换为Driver中的byte[]类型，没报错，why？这回知道了吧！
		Driver form = CommonUtils.toBean(super.form, Driver.class);
		System.out.println("表单信息：" + form.getDriverPhoto().length);
		if (driverService.addDriver(form)) {
			request.setAttribute("driverMsg", "添加成功！！！");
			//System.out.println("成功！！"+request.getContextPath());
		} else {
			System.out.println("失败!!");
			request.setAttribute("driverMsg", "用户数据已存在！！！");
		}
		request.setAttribute("form", form);
		response.setContentType("text/html;charset=UTF-8");//处理响应编码
		request.getRequestDispatcher("/baseInfo/addDriver.jsp").forward(request, response);
	}
	
	public void deleteDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> id = new ArrayList<String>();
		int i = 0;
		while (request.getParameter(Integer.toString(i)) != null) {
			id.add(request.getParameter(Integer.toString(i)));
			// System.out.println("GETid:"+i+"|"+Integer.toString(i)+
			// request.getParameter(Integer.toString(i)));
			i++;
		}
		if (driverService.deleteDriver(id)) {
			request.setAttribute("driverMsg", "删除成功！！！");
			request.setAttribute("pageLocation", 1);
			response.setContentType("text/html;charset=UTF-8");//处理响应编码
			request.getRequestDispatcher("/home/home.jsp").forward(request, response);
		}
	}
	
	public void editDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("form", driverService.editDriver(request.getParameter("id")));
		//response.sendRedirect("/TaxiManagerServer/baseInfo/editDriver.jsp");//怎样打开新的网页？？
		request.getRequestDispatcher("/baseInfo/editDriver.jsp").forward(request, response);
	}
	
	public void modifyDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Driver form = CommonUtils.toBean(super.form, Driver.class);
		System.out.println("表单信息：" + form.toString());
		if(driverService.modifyDriver(form)) {
			request.setAttribute("form", form);
			request.setAttribute("driverMsg", "修改成功！！！");
		} else {
			request.setAttribute("form", form);
			request.setAttribute("driverMsg", "修改失败！！！");
		}
		response.setContentType("text/html;charset=UTF-8");//处理响应编码
		request.getRequestDispatcher("/baseInfo/editDriver.jsp").forward(request, response);
	}
	
}
