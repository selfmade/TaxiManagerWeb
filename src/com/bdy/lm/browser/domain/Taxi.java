package com.bdy.lm.browser.domain;

public class Taxi {

	private String license;//车牌号
	private String licenseColor;//车牌颜色 	
	private String companyId;//所属企业组织机构代码
	private String ISUFlag;//ISU标识
	private String runLicense;//行驶证号
	private String engineId;//发动机号
	private String frameId;//车架号
	private String factoryId;//厂牌型号
	private String fuel;//燃料类型
	private String buyDate;//购车日期
	private String transportId;//道路运输证号
	
	
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getLicenseColor() {
		return licenseColor;
	}
	public void setLicenseColor(String licenseColor) {
		this.licenseColor = licenseColor;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getISUFlag() {
		return ISUFlag;
	}
	public void setISUFlag(String iSUFlag) {
		ISUFlag = iSUFlag;
	}
	public String getRunLicense() {
		return runLicense;
	}
	public void setRunLicense(String runLicense) {
		this.runLicense = runLicense;
	}
	public String getEngineId() {
		return engineId;
	}
	public void setEngineId(String engineId) {
		this.engineId = engineId;
	}
	public String getFrameId() {
		return frameId;
	}
	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}
	public String getFactoryId() {
		return factoryId;
	}
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public String getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(String buyDate) {
		this.buyDate = buyDate;
	}
	public String getTransportId() {
		return transportId;
	}
	public void setTransportId(String transportId) {
		this.transportId = transportId;
	}
	@Override
	public String toString() {
		return "Taxi [license=" + license + ", licenseColor=" + licenseColor
				+ ", companyId=" + companyId + ", ISUFlag=" + ISUFlag
				+ ", runLicense=" + runLicense + ", engineId=" + engineId
				+ ", frameId=" + frameId + ", factoryId=" + factoryId
				+ ", fuel=" + fuel + ", buyDate=" + buyDate + ", transportId="
				+ transportId + "]";
	}
	
	
}
