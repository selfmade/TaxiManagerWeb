package com.bdy.lm.browser.domain;

public class OperationMsg {

	//交易相关
	private String license;
	private String companyName;
	private String driverName;
	
	private String getOnTime;
    private String getOffTime;
    private String money;
    private String price;
    private String distance;
    private String spendTime;
    private String nullRun;
    private String journey;
    private String evaluate;
    //新计价器附加
    private String fuelSurcharge;
    private String waitTime;
    private String transitionType;
    private String currentRunTime;
    
    //是否作弊
	private String isCheat;
    
    public String getGetOnTime() {
        return getOnTime;
    }

    public void setGetOnTime(String getOnTime) {
        this.getOnTime = getOnTime;
    }

    public String getGetOffTime() {
        return getOffTime;
    }

    public void setGetOffTime(String getOffTime) {
        this.getOffTime = getOffTime;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSpendTime() {
        return spendTime;
    }

    public void setSpendTime(String spendTime) {
        this.spendTime = spendTime;
    }

    public String getJourney() {
        return journey;
    }

    public void setJourney(String journey) {
        this.journey = journey;
    }

    public String getNullRun() {
        return nullRun;
    }

    public void setNullRun(String nullRun) {
        this.nullRun = nullRun;
    }

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

	public String getFuelSurcharge() {
		return fuelSurcharge;
	}

	public void setFuelSurcharge(String fuelSurcharge) {
		this.fuelSurcharge = fuelSurcharge;
	}

	public String getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}

	public String getTransitionType() {
		return transitionType;
	}

	public void setTransitionType(String transitionType) {
		this.transitionType = transitionType;
	}

	public String getCurrentRunTime() {
		return currentRunTime;
	}

	public void setCurrentRunTime(String currentRunTime) {
		this.currentRunTime = currentRunTime;
	}
	public String getIsCheat() {
		return isCheat;
	}
	public void setIsCheat(String isCheat) {
		this.isCheat = isCheat;
	}

	@Override
	public String toString() {
		return "OperationMsg [license=" + license + ", companyName="
				+ companyName + ", driverName=" + driverName + ", getOnTime="
				+ getOnTime + ", getOffTime=" + getOffTime + ", money=" + money
				+ ", price=" + price + ", distance=" + distance
				+ ", spendTime=" + spendTime + ", nullRun=" + nullRun
				+ ", journey=" + journey + ", evaluate=" + evaluate
				+ ", fuelSurcharge=" + fuelSurcharge + ", waitTime=" + waitTime
				+ ", transitionType=" + transitionType + ", currentRunTime="
				+ currentRunTime + ", isCheat=" + isCheat + "]";
	}

}
