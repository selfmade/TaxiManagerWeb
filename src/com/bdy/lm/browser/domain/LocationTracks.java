package com.bdy.lm.browser.domain;


public class LocationTracks {

	private double longitude;
	private double latitude;
	private double speed;
	private long time;
	
	public LocationTracks() {

	}
	
	public LocationTracks(double longitude, double latitude, double speed, long time) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
		this.speed = speed;
		this.time = time;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "LocationTracks [longitude=" + longitude + ", latitude="
				+ latitude + ", speed=" + speed + ", time=" + time + "]";
	}

}
