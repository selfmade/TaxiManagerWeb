package com.bdy.lm.browser.domain;

public class Command {

	// 控制相关信息
	private String ISUFlag;
	private int messageID; // 两字节，消息ID
	private String publishTime;
	private String isExecute;
	private String msg;
	private String content;
	
	public Command() {
		super();
	}
	public Command(String iSUFlag, int messageID, String publishTime, String isExecute, String msg, String content) {
		super();
		ISUFlag = iSUFlag;
		this.messageID = messageID;
		this.publishTime = publishTime;
		this.isExecute = isExecute;
		this.msg = msg;
		this.content = content;
	}
	public String getISUFlag() {
		return ISUFlag;
	}
	public void setISUFlag(String iSUFlag) {
		ISUFlag = iSUFlag;
	}
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getIsExecute() {
		return isExecute;
	}
	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "Command [ISUFlag=" + ISUFlag + ", messageID=" + messageID
				+ ", publishTime=" + publishTime + ", isExecute=" + isExecute
				+ ", msg=" + msg + ", content=" + content + "]\n";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ISUFlag == null) ? 0 : ISUFlag.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((isExecute == null) ? 0 : isExecute.hashCode());
		result = prime * result + messageID;
		result = prime * result + ((msg == null) ? 0 : msg.hashCode());
		result = prime * result + ((publishTime == null) ? 0 : publishTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Command other = (Command) obj;
		if (ISUFlag == null) {
			if (other.ISUFlag != null)
				return false;
		} else if (!ISUFlag.equals(other.ISUFlag))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (isExecute == null) {
			if (other.isExecute != null)
				return false;
		} else if (!isExecute.equals(other.isExecute))
			return false;
		if (messageID != other.messageID)
			return false;
		if (msg == null) {
			if (other.msg != null)
				return false;
		} else if (!msg.equals(other.msg))
			return false;
		if (publishTime == null) {
			if (other.publishTime != null)
				return false;
		} else if (!publishTime.equals(other.publishTime))
			return false;
		return true;
	}
	
}
