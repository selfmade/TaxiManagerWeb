package com.bdy.lm.browser.domain;


public class Fare {

	private String areaId;//运价执行行政区划代码
	private String fareType;//运价类型
	private String fareFrom;//运价有效期起
	private String fareDayDown;//昼间起步价(元)
	private String fareNightDown;//夜间起步价(元)
	private String fareWait;//候时单价(元/分钟)
	private String downDistance;//起步里程 (km)
	private String priceDay;//昼间单价(元/km)
	private String priceDayAdd;//昼间单程加价单价(元/km)
	private String priceNight;//夜间单价(元)
	private String priceNightAdd;//夜间单程加价单价(元/km)
	private String addKiloPerTrip;//单程加价公里 (km)
	private String nigthFrom;//夜间时间起
	private String nightEnd;//夜间时间止
	private String lowSpeedWait;//低速等候设置
	private String switchSpeed;//切换速度 (km/h)
	private String fareRemark;//运价备注说明
	private String fareStatusCode;//运价状态代码(0或1)
	private String fuelSurcharge;//燃油附加费(元)
	private String updateTime;//最后更新时间
	private String k;
	private int fareNum;
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getFareType() {
		return fareType;
	}
	public void setFareType(String fareType) {
		this.fareType = fareType;
	}
	public String getFareFrom() {
		return fareFrom;
	}
	public void setFareFrom(String fareFrom) {
		this.fareFrom = fareFrom;
	}
	public String getFareDayDown() {
		return fareDayDown;
	}
	public void setFareDayDown(String fareDayDown) {
		this.fareDayDown = fareDayDown;
	}
	public String getFareNightDown() {
		return fareNightDown;
	}
	public void setFareNightDown(String fareNightDown) {
		this.fareNightDown = fareNightDown;
	}
	public String getFareWait() {
		return fareWait;
	}
	public void setFareWait(String fareWait) {
		this.fareWait = fareWait;
	}
	public String getDownDistance() {
		return downDistance;
	}
	public void setDownDistance(String downDistance) {
		this.downDistance = downDistance;
	}
	public String getPriceDay() {
		return priceDay;
	}
	public void setPriceDay(String priceDay) {
		this.priceDay = priceDay;
	}
	public String getPriceDayAdd() {
		return priceDayAdd;
	}
	public void setPriceDayAdd(String priceDayAdd) {
		this.priceDayAdd = priceDayAdd;
	}
	public String getPriceNight() {
		return priceNight;
	}
	public void setPriceNight(String priceNight) {
		this.priceNight = priceNight;
	}
	public String getPriceNightAdd() {
		return priceNightAdd;
	}
	public void setPriceNightAdd(String priceNightAdd) {
		this.priceNightAdd = priceNightAdd;
	}
	public String getAddKiloPerTrip() {
		return addKiloPerTrip;
	}
	public void setAddKiloPerTrip(String addKiloPerTrip) {
		this.addKiloPerTrip = addKiloPerTrip;
	}
	public String getNigthFrom() {
		return nigthFrom;
	}
	public void setNigthFrom(String nigthFrom) {
		this.nigthFrom = nigthFrom;
	}
	public String getNightEnd() {
		return nightEnd;
	}
	public void setNightEnd(String nightEnd) {
		this.nightEnd = nightEnd;
	}
	public String getLowSpeedWait() {
		return lowSpeedWait;
	}
	public void setLowSpeedWait(String lowSpeedWait) {
		this.lowSpeedWait = lowSpeedWait;
	}
	public String getSwitchSpeed() {
		return switchSpeed;
	}
	public void setSwitchSpeed(String switchSpeed) {
		this.switchSpeed = switchSpeed;
	}
	public String getFareRemark() {
		return fareRemark;
	}
	public void setFareRemark(String fareRemark) {
		this.fareRemark = fareRemark;
	}
	public String getFareStatusCode() {
		return fareStatusCode;
	}
	public void setFareStatusCode(String fareStatusCode) {
		this.fareStatusCode = fareStatusCode;
	}
	public String getFuelSurcharge() {
		return fuelSurcharge;
	}
	public void setFuelSurcharge(String fuelSurcharge) {
		this.fuelSurcharge = fuelSurcharge;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public int getFareNum() {
		return fareNum;
	}
	public void setFareNum(int fareNum) {
		this.fareNum = fareNum;
	}
	public String getK() {
		return k;
	}
	public void setK(String k) {
		this.k = k;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addKiloPerTrip == null) ? 0 : addKiloPerTrip.hashCode());
		result = prime * result + ((areaId == null) ? 0 : areaId.hashCode());
		result = prime * result + ((downDistance == null) ? 0 : downDistance.hashCode());
		result = prime * result + ((fareDayDown == null) ? 0 : fareDayDown.hashCode());
		result = prime * result + ((fareFrom == null) ? 0 : fareFrom.hashCode());
		result = prime * result + ((fareNightDown == null) ? 0 : fareNightDown.hashCode());
		result = prime * result + fareNum;
		result = prime * result + ((fareRemark == null) ? 0 : fareRemark.hashCode());
		result = prime * result + ((fareStatusCode == null) ? 0 : fareStatusCode.hashCode());
		result = prime * result + ((fareType == null) ? 0 : fareType.hashCode());
		result = prime * result + ((fareWait == null) ? 0 : fareWait.hashCode());
		result = prime * result + ((fuelSurcharge == null) ? 0 : fuelSurcharge.hashCode());
		result = prime * result + ((k == null) ? 0 : k.hashCode());
		result = prime * result + ((lowSpeedWait == null) ? 0 : lowSpeedWait.hashCode());
		result = prime * result + ((nightEnd == null) ? 0 : nightEnd.hashCode());
		result = prime * result + ((nigthFrom == null) ? 0 : nigthFrom.hashCode());
		result = prime * result + ((priceDay == null) ? 0 : priceDay.hashCode());
		result = prime * result + ((priceDayAdd == null) ? 0 : priceDayAdd.hashCode());
		result = prime * result + ((priceNight == null) ? 0 : priceNight.hashCode());
		result = prime * result + ((priceNightAdd == null) ? 0 : priceNightAdd.hashCode());
		result = prime * result + ((switchSpeed == null) ? 0 : switchSpeed.hashCode());
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fare other = (Fare) obj;
		if (addKiloPerTrip == null) {
			if (other.addKiloPerTrip != null)
				return false;
		} else if (!addKiloPerTrip.equals(other.addKiloPerTrip))
			return false;
		if (areaId == null) {
			if (other.areaId != null)
				return false;
		} else if (!areaId.equals(other.areaId))
			return false;
		if (downDistance == null) {
			if (other.downDistance != null)
				return false;
		} else if (!downDistance.equals(other.downDistance))
			return false;
		if (fareDayDown == null) {
			if (other.fareDayDown != null)
				return false;
		} else if (!fareDayDown.equals(other.fareDayDown))
			return false;
		if (fareFrom == null) {
			if (other.fareFrom != null)
				return false;
		} else if (!fareFrom.equals(other.fareFrom))
			return false;
		if (fareNightDown == null) {
			if (other.fareNightDown != null)
				return false;
		} else if (!fareNightDown.equals(other.fareNightDown))
			return false;
		if (fareNum != other.fareNum)
			return false;
		if (fareRemark == null) {
			if (other.fareRemark != null)
				return false;
		} else if (!fareRemark.equals(other.fareRemark))
			return false;
		if (fareStatusCode == null) {
			if (other.fareStatusCode != null)
				return false;
		} else if (!fareStatusCode.equals(other.fareStatusCode))
			return false;
		if (fareType == null) {
			if (other.fareType != null)
				return false;
		} else if (!fareType.equals(other.fareType))
			return false;
		if (fareWait == null) {
			if (other.fareWait != null)
				return false;
		} else if (!fareWait.equals(other.fareWait))
			return false;
		if (fuelSurcharge == null) {
			if (other.fuelSurcharge != null)
				return false;
		} else if (!fuelSurcharge.equals(other.fuelSurcharge))
			return false;
		if (k == null) {
			if (other.k != null)
				return false;
		} else if (!k.equals(other.k))
			return false;
		if (lowSpeedWait == null) {
			if (other.lowSpeedWait != null)
				return false;
		} else if (!lowSpeedWait.equals(other.lowSpeedWait))
			return false;
		if (nightEnd == null) {
			if (other.nightEnd != null)
				return false;
		} else if (!nightEnd.equals(other.nightEnd))
			return false;
		if (nigthFrom == null) {
			if (other.nigthFrom != null)
				return false;
		} else if (!nigthFrom.equals(other.nigthFrom))
			return false;
		if (priceDay == null) {
			if (other.priceDay != null)
				return false;
		} else if (!priceDay.equals(other.priceDay))
			return false;
		if (priceDayAdd == null) {
			if (other.priceDayAdd != null)
				return false;
		} else if (!priceDayAdd.equals(other.priceDayAdd))
			return false;
		if (priceNight == null) {
			if (other.priceNight != null)
				return false;
		} else if (!priceNight.equals(other.priceNight))
			return false;
		if (priceNightAdd == null) {
			if (other.priceNightAdd != null)
				return false;
		} else if (!priceNightAdd.equals(other.priceNightAdd))
			return false;
		if (switchSpeed == null) {
			if (other.switchSpeed != null)
				return false;
		} else if (!switchSpeed.equals(other.switchSpeed))
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Fare [areaId=" + areaId + ", fareType=" + fareType + ", fareFrom=" + fareFrom + ", fareDayDown="
				+ fareDayDown + ", fareNightDown=" + fareNightDown + ", fareWait=" + fareWait + ", downDistance="
				+ downDistance + ", priceDay=" + priceDay + ", priceDayAdd=" + priceDayAdd + ", priceNight="
				+ priceNight + ", priceNightAdd=" + priceNightAdd + ", addKiloPerTrip=" + addKiloPerTrip
				+ ", nigthFrom=" + nigthFrom + ", nightEnd=" + nightEnd + ", lowSpeedWait=" + lowSpeedWait
				+ ", switchSpeed=" + switchSpeed + ", fareRemark=" + fareRemark + ", fareStatusCode=" + fareStatusCode
				+ ", fuelSurcharge=" + fuelSurcharge + ", updateTime=" + updateTime + ", k=" + k + ", fareNum="
				+ fareNum + "]";
	}

}
