package com.bdy.lm.browser.domain;

public class HistoryMsg {

	private String msg;
	private String content;
	private String publishTime;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	@Override
	public String toString() {
		return "HistoryMsg [msg=" + msg + ", content=" + content
				+ ", publishTime=" + publishTime + "]";
	}
	
	
}
