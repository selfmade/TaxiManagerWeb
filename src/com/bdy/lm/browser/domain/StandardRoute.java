package com.bdy.lm.browser.domain;

public class StandardRoute {

	private int num;
	private double Alongitude, Alatitude, Aaltitude, Aradius, 
				  Blongtitde, Blatitude, Baltitude, Bradius, 
				  Smeasure, standard;
	
	public StandardRoute() {
		super();
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public double getAlongitude() {
		return Alongitude;
	}
	public void setAlongitude(double alongitude) {
		Alongitude = alongitude;
	}
	public double getAlatitude() {
		return Alatitude;
	}
	public void setAlatitude(double alatitude) {
		Alatitude = alatitude;
	}
	public double getAaltitude() {
		return Aaltitude;
	}
	public void setAaltitude(double aaltitude) {
		Aaltitude = aaltitude;
	}
	public double getAradius() {
		return Aradius;
	}
	public void setAradius(double aradius) {
		Aradius = aradius;
	}
	public double getBlongtitde() {
		return Blongtitde;
	}
	public void setBlongtitde(double blongtitde) {
		Blongtitde = blongtitde;
	}
	public double getBlatitude() {
		return Blatitude;
	}
	public void setBlatitude(double blatitude) {
		Blatitude = blatitude;
	}
	public double getBaltitude() {
		return Baltitude;
	}
	public void setBaltitude(double baltitude) {
		Baltitude = baltitude;
	}
	public double getBradius() {
		return Bradius;
	}
	public void setBradius(double bradius) {
		Bradius = bradius;
	}
	public double getSmeasure() {
		return Smeasure;
	}
	public void setSmeasure(double smeasure) {
		Smeasure = smeasure;
	}
	public double getStandard() {
		return standard;
	}
	public void setStandard(double standard) {
		this.standard = standard;
	}
	@Override
	public String toString() {
		return "StandardRoute [num=" + num + ", Alongitude=" + Alongitude
				+ ", Alatitude=" + Alatitude + ", Aaltitude=" + Aaltitude
				+ ", Aradius=" + Aradius + ", Blongtitde=" + Blongtitde
				+ ", Blatitude=" + Blatitude + ", Baltitude=" + Baltitude
				+ ", Bradius=" + Bradius + ", Smeasure=" + Smeasure
				+ ", standard=" + standard + "]";
	}
	
	
}
