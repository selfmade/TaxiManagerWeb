package com.bdy.lm.browser.domain;

public class User {
	
	private String identity;
	private String name;
	private String password;
	
	public User(String identity, String name, String password) {
		super();
		this.identity = identity;
		this.name = name;
		this.password = password;
	}

	public User() {
		super();
	}

	public String getIdentity() {
		return identity;
	}
	
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "User [identity=" + identity + ", name=" + name + ", password="
				+ password + "]";
	}

	
}
