package com.bdy.lm.browser.domain;

import java.io.InputStream;

public class Driver {
	
	private String driverName;
	private String sex;
	private String id;
	private String employId;
	private String employCardAgent;
	private String timeBeginEnd;
	private String tel;
	private String companyId;
	private String education;
	private String serviceGrade;
	private String ICID;
	private byte[] driverPhoto;
	
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmployId() {
		return employId;
	}
	public void setEmployId(String employId) {
		this.employId = employId;
	}
	public String getEmployCardAgent() {
		return employCardAgent;
	}
	public void setEmployCardAgent(String employCardAgent) {
		this.employCardAgent = employCardAgent;
	}

	
	public String getTimeBeginEnd() {
		return timeBeginEnd;
	}
	public void setTimeBeginEnd(String timeBeginEnd) {
		this.timeBeginEnd = timeBeginEnd;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	
	public String getServiceGrade() {
		return serviceGrade;
	}
	public void setServiceGrade(String serviceGrade) {
		this.serviceGrade = serviceGrade;
	}

	public String getICID() {
		return ICID;
	}
	public void setICID(String iCID) {
		ICID = iCID;
	}

	public byte[] getDriverPhoto() {
		return driverPhoto;
	}
	public void setDriverPhoto(byte[] driverPhoto) {
		this.driverPhoto = driverPhoto;
	}
	@Override
	public String toString() {
		return "Driver [driverName=" + driverName + ", sex=" + sex + ", id="
				+ id + ", employId=" + employId + ", employCardAgent="
				+ employCardAgent + ", timeBeginEnd=" + timeBeginEnd + ", tel="
				+ tel + ", companyId=" + companyId + ", education=" + education
				+ ", serviceGrade=" + serviceGrade + ", ICID=" + ICID
				+ ", driverPhoto=" + driverPhoto + "]";
	}

}
