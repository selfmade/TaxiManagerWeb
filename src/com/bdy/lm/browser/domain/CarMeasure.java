package com.bdy.lm.browser.domain;

public class CarMeasure {

	private String ISUFlag, license, isRegular, verifyTime;
	private double Alongitude, Alatitude, Aaltitude, Aspeed, 
				  Blongtitde, Blatitude, Baltitude, Bspeed, 
				  ScarMeasure;
	
	public CarMeasure() {
	}

	public String getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(String verifyTime) {
		this.verifyTime = verifyTime;
	}

	public String getISUFlag() {
		return ISUFlag;
	}

	public void setISUFlag(String iSUFlag) {
		ISUFlag = iSUFlag;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public double getAlongitude() {
		return Alongitude;
	}
	public void setAlongitude(double alongitude) {
		Alongitude = alongitude;
	}
	public double getAlatitude() {
		return Alatitude;
	}
	public void setAlatitude(double alatitude) {
		Alatitude = alatitude;
	}
	public double getAaltitude() {
		return Aaltitude;
	}
	public void setAaltitude(double aaltitude) {
		Aaltitude = aaltitude;
	}
	public double getBlongtitde() {
		return Blongtitde;
	}
	public void setBlongtitde(double blongtitde) {
		Blongtitde = blongtitde;
	}
	public double getBlatitude() {
		return Blatitude;
	}
	public void setBlatitude(double blatitude) {
		Blatitude = blatitude;
	}
	public double getBaltitude() {
		return Baltitude;
	}
	public void setBaltitude(double baltitude) {
		Baltitude = baltitude;
	}
	public double getScarMeasure() {
		return ScarMeasure;
	}
	public void setScarMeasure(double scarMeasure) {
		ScarMeasure = scarMeasure;
	}

	public String getIsRegular() {
		return isRegular;
	}

	public void setIsRegular(String isRegular) {
		this.isRegular = isRegular;
	}

	public double getAspeed() {
		return Aspeed;
	}

	public void setAspeed(double aspeed) {
		Aspeed = aspeed;
	}

	public double getBspeed() {
		return Bspeed;
	}

	public void setBspeed(double bspeed) {
		Bspeed = bspeed;
	}

	@Override
	public String toString() {
		return "CarMeasure [ISUFlag=" + ISUFlag + ", license=" + license
				+ ", isRegular=" + isRegular + ", Alongitude=" + Alongitude
				+ ", Alatitude=" + Alatitude + ", Aaltitude=" + Aaltitude
				+ ", Aspeed=" + Aspeed + ", Blongtitde=" + Blongtitde
				+ ", Blatitude=" + Blatitude + ", Baltitude=" + Baltitude
				+ ", Bspeed=" + Bspeed + ", ScarMeasure=" + ScarMeasure + "]";
	}

}
