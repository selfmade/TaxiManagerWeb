package com.bdy.lm.browser.domain;

public class RemoteManageQueryTaxi {

	private String license;
	private String companyName;
	private String driverName;
	private String reportTime;//最后汇报时间
	private String runState;
	private String isOnline;
	
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}

	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	
	public String getRunState() {
		return runState;
	}
	public void setRunState(String runState) {
		this.runState = runState;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	@Override
	public String toString() {
		return "RemoteManageQueryTaxi [license=" + license + ", companyName="
				+ companyName + ", driverName=" + driverName + ", reportTime="
				+ reportTime + ", runState=" + runState + ", isOnline="
				+ isOnline + "]";
	}


}
