package com.bdy.lm.browser.domain;

public class LocationRunState {

	//定位相关信息
    private int status_1;
    private int status_2;
    private int status_3;
    private int status_4;
    private int alarm_byte_1;
    private int alarm_byte_2;
    private int alarm_byte_3;
    private int alarm_byte_4;   
    private double latitude;
    private double longitude;
    private double speed;
    private double direction;//0~179，正北为0，正南90
    private String locationTime;//Unix时间戳
    
	private String license;
	private String companyName;
	private String driverName;
    private String reportTime;//最后汇报时间
	private String runState;
	private String isOnline;
	
	private String time;
	private String state;
	
    
	public int getStatus_1() {
		return status_1;
	}
	public void setStatus_1(int status_1) {
		this.status_1 = status_1;
	}
	public int getStatus_2() {
		return status_2;
	}
	public void setStatus_2(int status_2) {
		this.status_2 = status_2;
	}
	public int getStatus_3() {
		return status_3;
	}
	public void setStatus_3(int status_3) {
		this.status_3 = status_3;
	}
	public int getStatus_4() {
		return status_4;
	}
	public void setStatus_4(int status_4) {
		this.status_4 = status_4;
	}
	public int getAlarm_byte_1() {
		return alarm_byte_1;
	}
	public void setAlarm_byte_1(int alarm_byte_1) {
		this.alarm_byte_1 = alarm_byte_1;
	}
	public int getAlarm_byte_2() {
		return alarm_byte_2;
	}
	public void setAlarm_byte_2(int alarm_byte_2) {
		this.alarm_byte_2 = alarm_byte_2;
	}
	public int getAlarm_byte_3() {
		return alarm_byte_3;
	}
	public void setAlarm_byte_3(int alarm_byte_3) {
		this.alarm_byte_3 = alarm_byte_3;
	}
	public int getAlarm_byte_4() {
		return alarm_byte_4;
	}
	public void setAlarm_byte_4(int alarm_byte_4) {
		this.alarm_byte_4 = alarm_byte_4;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getDirection() {
		return direction;
	}
	public void setDirection(double direction) {
		this.direction = direction;
	}
	public String getLocationTime() {
		return locationTime;
	}
	public void setLocationTime(String locationTime) {
		this.locationTime = locationTime;
	}
	
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	public String getRunState() {
		return runState;
	}
	public void setRunState(String runState) {
		this.runState = runState;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "LocationRunState [status_1=" + status_1 + ", status_2="
				+ status_2 + ", status_3=" + status_3 + ", status_4="
				+ status_4 + ", alarm_byte_1=" + alarm_byte_1
				+ ", alarm_byte_2=" + alarm_byte_2 + ", alarm_byte_3="
				+ alarm_byte_3 + ", alarm_byte_4=" + alarm_byte_4
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", speed=" + speed + ", direction=" + direction
				+ ", locationTime=" + locationTime + ", license=" + license
				+ ", companyName=" + companyName + ", driverName=" + driverName
				+ ", reportTime=" + reportTime + ", runState=" + runState
				+ ", isOnline=" + isOnline + ", time=" + time + ", state="
				+ state + "]";
	}

    
    
}
