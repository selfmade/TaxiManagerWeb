package com.bdy.lm.browser.domain;

public class ParameterQuery {
	private String ISUFlag;
	private String license;
	private String parameter;
	private String taximeterTime;
	private String queryTime;
	private Fare fare;
	
	public String getISUFlag() {
		return ISUFlag;
	}
	public void setISUFlag(String iSUFlag) {
		ISUFlag = iSUFlag;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getTaximeterTime() {
		return taximeterTime;
	}
	public void setTaximeterTime(String taximeterTime) {
		this.taximeterTime = taximeterTime;
	}
	public String getQueryTime() {
		return queryTime;
	}
	public void setQueryTime(String queryTime) {
		this.queryTime = queryTime;
	}
	public Fare getFare() {
		return fare;
	}
	public void setFare(Fare fare) {
		this.fare = fare;
	}
	@Override
	public String toString() {
		return "ParameterQuery [ISUFlag=" + ISUFlag + ", license=" + license + ", parameter=" + parameter
				+ ", taximeterTime=" + taximeterTime + ", queryTime=" + queryTime + ", fare=" + fare + "]";
	}
	
}
