package com.bdy.lm.browser.domain;

public class Company {

	private String companyId;
	private String areaId;
	private String companyName;
	private String principal;
	private String corporateRepre;
	private String economicType;
	private String businessLicenseId;
	private String tradePermission;
	private String businessScope;
	private String address;
	private String companyTel;
	private String businessQualification;
	private String serviceQuality;
	private String registeredFund;
	
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getCorporateRepre() {
		return corporateRepre;
	}
	public void setCorporateRepre(String corporateRepre) {
		this.corporateRepre = corporateRepre;
	}
	public String getEconomicType() {
		return economicType;
	}
	public void setEconomicType(String economicType) {
		this.economicType = economicType;
	}
	public String getBusinessLicenseId() {
		return businessLicenseId;
	}
	public void setBusinessLicenseId(String businessLicenseId) {
		this.businessLicenseId = businessLicenseId;
	}
	public String getTradePermission() {
		return tradePermission;
	}
	public void setTradePermission(String tradePermission) {
		this.tradePermission = tradePermission;
	}
	public String getBusinessScope() {
		return businessScope;
	}
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCompanyTel() {
		return companyTel;
	}
	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}
	public String getBusinessQualification() {
		return businessQualification;
	}
	public void setBusinessQualification(String businessQualification) {
		this.businessQualification = businessQualification;
	}
	public String getServiceQuality() {
		return serviceQuality;
	}
	public void setServiceQuality(String serviceQuality) {
		this.serviceQuality = serviceQuality;
	}
	public String getRegisteredFund() {
		return registeredFund;
	}
	public void setRegisteredFund(String registeredFund) {
		this.registeredFund = registeredFund;
	}
	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", areaId=" + areaId
				+ ", companyName=" + companyName + ", principal=" + principal
				+ ", corporateRepre=" + corporateRepre + ", economicType="
				+ economicType + ", businessLicenseId=" + businessLicenseId
				+ ", tradePermission=" + tradePermission + ", businessScope="
				+ businessScope + ", address=" + address + ", companyTel="
				+ companyTel + ", businessQualification="
				+ businessQualification + ", serviceQuality=" + serviceQuality
				+ ", registeredFund=" + registeredFund + "]";
	}
}
