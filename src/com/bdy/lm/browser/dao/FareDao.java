package com.bdy.lm.browser.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;

import com.bdy.lm.browser.domain.Company;
import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.utils.Utils;

public class FareDao {

	private QueryRunner qr = new TxQueryRunner();
	
	public Fare queryFareByUpdate(String updateTime) {
		try {
			String sql = "SELECT * FROM fare_tb WHERE updateTime=?";
			return qr.query(sql, new BeanHandler<Fare>(Fare.class), updateTime);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Fare> queryFare(String queryMode, String queryContent) {

		List<Fare> fares;
		try {
			if (queryMode.equals("all")) {
				String sql = "SELECT * FROM fare_tb";
				fares = qr.query(sql, new BeanListHandler<Fare>(Fare.class));
			} else {
				String sql = "SELECT * FROM fare_tb WHERE " + queryMode + " = ?";
				Object[] params = { queryContent };
				fares = qr.query(sql, new BeanListHandler<Fare>(Fare.class), params);
			}
			return fares;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean addFare(Fare fare) throws SQLException {
		try {
			String sql = "INSERT INTO fare_tb VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Object[] params = { fare.getFareNum(), Utils.getCurrentTime(), fare.getAreaId(), fare.getFareType(), fare.getFareFrom(), fare.getFareDayDown(), fare.getFareNightDown(),
				fare.getFareWait(), fare.getDownDistance(), fare.getPriceDay(), fare.getPriceDayAdd(), fare.getPriceNight(),
				fare.getPriceNightAdd(), fare.getAddKiloPerTrip(), fare.getNigthFrom(), fare.getNightEnd(), fare.getLowSpeedWait(),
				fare.getSwitchSpeed(), fare.getFareRemark(), fare.getFareStatusCode(), fare.getFuelSurcharge(), fare.getK() };
			if (qr.update(sql, params) > 0) {
				return true;
			} else {
				return false;// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String modifyFare(Fare fare) throws SQLException {
		try {
			String currentTime = Utils.getCurrentTime();
			String sql = "UPDATE fare_tb SET fareNum=?,updateTime=?,areaId=?,fareType=?,fareFrom=?,fareDayDown=?,fareNightDown=?,fareWait=?,downDistance=?,priceDay=?,priceDayAdd=?,priceNight=?,priceNightAdd=?,addKiloPerTrip=?,nigthFrom=?,nightEnd=?,lowSpeedWait=?,switchSpeed=?,fareRemark=?,fareStatusCode=?,fuelSurcharge=?,k=? WHERE updateTime=?";
			Object[] params = {  fare.getFareNum(), currentTime, fare.getAreaId(), fare.getFareType(), fare.getFareFrom(), fare.getFareDayDown(), 
					fare.getFareNightDown(), fare.getFareWait(), fare.getDownDistance(), fare.getPriceDay(), fare.getPriceDayAdd(), 
					fare.getPriceNight(), fare.getPriceNightAdd(), fare.getAddKiloPerTrip(), fare.getNigthFrom(), fare.getNightEnd(), 
					fare.getLowSpeedWait(), fare.getSwitchSpeed(), fare.getFareRemark(), fare.getFareStatusCode(), fare.getFuelSurcharge(), fare.getK(), fare.getUpdateTime() };
			if (qr.update(sql, params) > 0) {
				return currentTime;
			} else {
				return "";// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public boolean deleteFare(List<String> updateTimes) throws SQLException {

		try {
			String sql = "DELETE FROM fare_tb WHERE updateTime=?";
			int sum = 0;
			for (int i = 0; i < updateTimes.size(); i++) {
				Object[] params = { updateTimes.get(i) };
				sum += qr.update(sql, params);
			}
			if (sum == updateTimes.size()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
}
