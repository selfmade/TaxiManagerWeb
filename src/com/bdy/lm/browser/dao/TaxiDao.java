package com.bdy.lm.browser.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;
import com.bdy.lm.browser.domain.Taxi;

public class TaxiDao {

	private QueryRunner qr = new TxQueryRunner();

	public Taxi queryTaxiByLicense(String license) {
		try {
			String sql = "SELECT * FROM car_tb WHERE license=?";
			return qr.query(sql, new BeanHandler<Taxi>(Taxi.class), license);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Taxi> queryTaxi(String queryMode, String queryContent) {
		try {
			String sql = "SELECT * FROM car_tb WHERE " + queryMode + " = ?";
			Object[] params = { queryContent };
			List<Taxi> taxi = qr.query(sql, new BeanListHandler<Taxi>(Taxi.class), params);
			return taxi;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean addTaxi(Taxi taxi) throws SQLException {
		try {
			String sql = "INSERT INTO car_tb VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			Object[] params = { taxi.getLicense(), taxi.getCompanyId(),taxi.getLicenseColor(), taxi.getRunLicense(),
					taxi.getEngineId(), taxi.getISUFlag(),taxi.getBuyDate(), taxi.getFuel(),
					taxi.getFactoryId(), taxi.getTransportId(),taxi.getFrameId() };
			if (qr.update(sql, params) > 0) {
				return true;
			} else {
				return false;// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean modifyTaxi(Taxi taxi) throws SQLException {
		try {
			String sql = "UPDATE car_tb SET companyId=?,licenseColor=?,runLicense=?,engineId=?,ISUFlag=?,buyDate=?,fuel=?,factoryId=?,transportId=?,frameId=? WHERE license=?";
			Object[] params = { taxi.getCompanyId(),taxi.getLicenseColor(), taxi.getRunLicense(),
					taxi.getEngineId(), taxi.getISUFlag(),taxi.getBuyDate(), taxi.getFuel(),
					taxi.getFactoryId(), taxi.getTransportId(),taxi.getFrameId(),taxi.getLicense() };
			if (qr.update(sql, params) > 0) {
				return true;
			} else {
				return false;// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public boolean deleteTaxi(List<String> license) throws SQLException {

		try {
			String sql = "DELETE FROM car_tb WHERE license=?";
			int sum = 0;
			for (int i = 0; i < license.size(); i++) {
				Object[] params = { license.get(i) };
				sum += qr.update(sql, params);
			}
			if (sum == license.size()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
