package com.bdy.lm.browser.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.jdbc.TxQueryRunner;

import com.bdy.lm.browser.domain.User;

public class UserDao {

	private QueryRunner qr = new TxQueryRunner();

	public User queryByUserName(String userName) {

		try {
			String sql = "SELECT * FROM user_tb WHERE name=?";
			return qr.query(sql, new BeanHandler<User>(User.class), userName);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public boolean addUser(User user) {
		try {
			String sql = "INSERT INTO user_tb VALUES(?,?,?)";
			Object[] params = {user.getIdentity(), user.getName(), user.getPassword() };
			return qr.update(sql, params) > 0;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
