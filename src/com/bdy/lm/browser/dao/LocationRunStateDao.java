package com.bdy.lm.browser.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;

import com.bdy.lm.browser.domain.LocationRunState;

public class LocationRunStateDao {

	private Connection con = null;// 定义引用
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private QueryRunner qr = new TxQueryRunner();

	public List<LocationRunState> queryTaxi(String queryMode, String queryContent) {

		String sql = "SELECT car_tb.license,company_tb.companyName,driver_tb.driverName,online_tb.runState,online_tb.reportTime "
				+ "FROM car_tb,company_tb,online_tb,driver_tb "
				+ "WHERE car_tb.companyId=company_tb.companyId AND online_tb.ICID=driver_tb.ICID "
				+ "AND car_tb.ISUFlag=online_tb.ISUFlag ";
		Object[] param = { queryContent };
		try {
			if (queryMode.equals("all")) {
				return qr.query(sql, new BeanListHandler<LocationRunState>(LocationRunState.class));			
			} else if (queryMode.equals("companyName")) {
				String cn = sql + "AND companyName=?";
				return qr.query(cn, new BeanListHandler<LocationRunState>(LocationRunState.class), param);
			} else {
				String l = sql + "AND license=?";
				return qr.query(l, new BeanListHandler<LocationRunState>(LocationRunState.class), param);		
			} 
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 查询该车的所有位置和运行状态信息
	 * @param license 车牌号
	 * @return 位置和运行状态信息集合
	 */
	public List<LocationRunState> queryDetail(String license) {
		String sql = "SELECT location_tb.*,car_tb.license,driver_tb.driverName,online_tb.runState,online_tb.reportTime,company_tb.companyName,loginout_tb.time,loginout_tb.state "
				 + "FROM location_tb,car_tb,driver_tb,online_tb,company_tb,loginout_tb "
				 + "WHERE car_tb.companyId=company_tb.companyId AND online_tb.ICID=driver_tb.ICID "
				 + "AND car_tb.ISUFlag=online_tb.ISUFlag AND driver_tb.ICID=loginout_tb.ICID "
				 + "AND car_tb.ISUFlag=location_tb.ISUFlag AND car_tb.license='" + license + "'";
		try {
			return qr.query(sql, new BeanListHandler<LocationRunState>(LocationRunState.class));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<LocationRunState> queryPoints(String license) {
		
		String sql = "SELECT location_tb.longitude,location_tb.latitude,location_tb.locationTime " 
				+ "FROM location_tb,car_tb "
				+ "WHERE car_tb.ISUFlag=location_tb.ISUFlag AND car_tb.license='" + license + "'";
		try {
			return qr.query(sql, new BeanListHandler<LocationRunState>(LocationRunState.class));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
