package com.bdy.lm.browser.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.bdy.lm.browser.domain.ParameterQuery;

import cn.itcast.jdbc.TxQueryRunner;

public class ParameterQueryDao {
	
	private QueryRunner qr = new TxQueryRunner();
	
	public List<ParameterQuery> queryParameter(String queryMode, String queryContent) {
		List<ParameterQuery> parameterQueries;
		try {
			if (queryMode.equals("all")) {
				String sql = "SELECT * FROM taximeterparameter_tb";
				parameterQueries = qr.query(sql, new BeanListHandler<ParameterQuery>(ParameterQuery.class));
			} else {
				String sql = "SELECT * FROM taximeterparameter_tb WHERE " + queryMode + " = ?";
				Object[] params = { queryContent };
				parameterQueries = qr.query(sql, new BeanListHandler<ParameterQuery>(ParameterQuery.class), params);
			}
			return parameterQueries;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
