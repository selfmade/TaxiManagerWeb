package com.bdy.lm.browser.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;

import com.bdy.lm.browser.domain.Company;
import com.bdy.lm.browser.domain.Driver;

public class CompanyDao {

	private QueryRunner qr = new TxQueryRunner();

	public Company queryCompanyByCompanyId(String companyId) {
		try {
			String sql = "SELECT * FROM company_tb WHERE companyId=?";
			return qr.query(sql, new BeanHandler<Company>(Company.class), companyId);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Company> queryCompany(String queryMode, String queryContent) throws SQLException {
		try {
			String sql = "SELECT * FROM company_tb WHERE " + queryMode + " = ?";
			Object[] params = { queryContent };
			List<Company> company = qr.query(sql, new BeanListHandler<Company>(Company.class), params);
			return company;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean addCompany(Company company) throws SQLException {
		try {
			String sql = "INSERT INTO company_tb VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Object[] params = { company.getCompanyId(), company.getAreaId(),
					company.getCompanyName(), company.getPrincipal(),
					company.getCorporateRepre(), company.getEconomicType(),
					company.getBusinessLicenseId(), company.getTradePermission(), 
					company.getBusinessScope(), company.getAddress(), 
					company.getCompanyTel(), company.getBusinessQualification(), 
					company.getServiceQuality(), company.getRegisteredFund() };
			if (qr.update(sql, params) > 0) {
				return true;
			} else {
				return false;// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean modifyCompany(Company company) throws SQLException {
		try {
			String sql = "UPDATE company_tb SET areaId=?,companyName=?,principal=?,corporateRepre=?,economicType=?,businessLicenseId=?,tradePermission=?,businessScope=?,address=?,companyTel=?,businessQualification=?,serviceQuality=?,registeredFund=? WHERE companyId=?";
			Object[] params = {  company.getAreaId(), company.getCompanyName(), company.getPrincipal(),
					company.getCorporateRepre(), company.getEconomicType(), company.getBusinessLicenseId(), 
					company.getTradePermission(), company.getBusinessScope(), company.getAddress(), 
					company.getCompanyTel(), company.getBusinessQualification(), company.getServiceQuality(), 
					company.getRegisteredFund(), company.getCompanyId(),  };
			if (qr.update(sql, params) > 0) {
				return true;
			} else {
				return false;// 该情况没有处理
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	public boolean deleteCompany(List<String> companyId) throws SQLException {

		try {
			String sql = "delete from company_tb where companyId=?";
			int sum = 0;
			for (int i = 0; i < companyId.size(); i++) {
				Object[] params = { companyId.get(i) };
				sum += qr.update(sql, params);
			}
			if (sum == companyId.size()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
