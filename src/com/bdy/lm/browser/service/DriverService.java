package com.bdy.lm.browser.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bdy.lm.browser.dao.DriverDao;
import com.bdy.lm.browser.domain.Driver;

public class DriverService {

	private DriverDao driverDao = new DriverDao();

	public List<Driver> queryDriver(String queryMode, String queryContent) {
		List<Driver> drivers = new ArrayList<Driver>();
		try {
			drivers = driverDao.queryDriver(queryMode, queryContent);
//			for (int i = 0; i < drivers.size(); i++) {
//				Driver driver = drivers.get(i);
//				driver.setDriverPhoto(driverDao.queryDriverPhoto(driver.getICID()));
//				drivers.set(i, driver);
//				System.out.println("length:" + driver.getDriverPhoto().length);
//			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return drivers;
	}

	public boolean addDriver(Driver driver) {

		try {
			if (driverDao.queryDriverById(driver.getId()) != null) {
				return false;// 添加不成功,已存在
			} else {
				return driverDao.addDriver(driver);// 添加成功
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public boolean deleteDriver(List<String> id) {

		try {
			return driverDao.deleteDriver(id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public Driver editDriver(String id) {
		
		return driverDao.queryDriverById(id);
	}
	
	public boolean modifyDriver(Driver driver) {
		Driver d = driverDao.queryDriverById(driver.getId());
		if(d != null) {
			try {
				if (driverDao.modifyDriver(driver) > 0) {
					return true;
				} else {
					return false;
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		} else {
			return  false;
		}
	}
}
