package com.bdy.lm.browser.service;

import com.bdy.lm.browser.dao.UserDao;
import com.bdy.lm.browser.domain.User;

public class UserService {

	private UserDao userDao = new UserDao();

	public User login(User form) throws UserException {

		User user = userDao.queryByUserName(form.getName());

		if (user == null) {
			throw new UserException("用户名错误！");
		}
		if (!form.getPassword().equals(user.getPassword())) {
			throw new UserException("密码错误！");
		}
		if (!form.getIdentity().equals(user.getIdentity())) {
			throw new UserException("身份错误！");
		}
		return user;
	}
	
	public boolean register(User form) throws UserException {
		if (form == null || form.getName().equals("")) {
			throw new UserException("用户名不为空！");
		}
		if (form.getPassword().equals("")) {
			throw new UserException("密码不为空！");
		}
		User user = userDao.queryByUserName(form.getName());
		if (user == null) {
			return userDao.addUser(form);
		}
		if (user.getName().equals(form.getName())) {
			throw new UserException("用户名已存在！");
		} else {
			return userDao.addUser(form);
		}
	}
}
