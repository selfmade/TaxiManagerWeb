package com.bdy.lm.browser.service;

import java.util.ArrayList;
import java.util.List;

import com.bdy.lm.browser.dao.LocationRunStateDao;
import com.bdy.lm.browser.domain.LocationRunState;
import com.bdy.lm.browser.domain.LocationTracks;

public class LocationRunStateService {

	private LocationRunStateDao locationRunStateDao = new LocationRunStateDao();

	public List<LocationRunState> queryTaxi(String queryMode, String queryContent) {

		List<LocationRunState> taxi = locationRunStateDao.queryTaxi(queryMode, queryContent);
		if (taxi != null) {
			for (LocationRunState sTaxi : taxi) {
				if (System.currentTimeMillis() - Long.parseLong(sTaxi.getReportTime()) > 300000) {//五分钟内无汇报，视为离线
					sTaxi.setIsOnline("离线");					
				} else {
					sTaxi.setIsOnline("在线");
				}
				sTaxi.setReportTime(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.parseLong(sTaxi.getReportTime()))));
			}
		}
		return taxi;
	}

	/**
	 * 查询指定车辆的最新的位置和运行状态信息
	 * @param license 车牌号
	 * @return 位置和运行状态
	 */
	public LocationRunState queryDetail(String license) {
		List<LocationRunState> list = locationRunStateDao.queryDetail(license);
		LocationRunState little;
		if (list == null || list.size() == 0) {
			return null;
		} else {
			little = list.get(0);
			//根据时间找到最新的一条信息
			long currentTime = System.currentTimeMillis();
			long cha = currentTime - Long.parseLong(list.get(0).getLocationTime());
			for (LocationRunState locationRunState : list) {
				if ((currentTime - Long.parseLong(locationRunState.getLocationTime())) < cha) {
					cha = currentTime - Long.parseLong(locationRunState.getLocationTime());
					little = locationRunState;
				}
			}
			//System.out.println("长度" + little.toString());//+ list.get(0).toString()
			String time = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(Long.parseLong(little.getLocationTime()))).toString();
			little.setLocationTime(time);
			//System.out.println(time + "//" + little.getLocationTime());
			if (System.currentTimeMillis() - Long.parseLong(little.getReportTime()) > 300000) {//五分钟内无汇报，视为离线
				little.setIsOnline("离线");
			} else {
				little.setIsOnline("在线");
			}
		}
		return little;
	}
	
	/**
	 * 获取指定车辆从开始时间到结束时间的位置点信息
	 * @param license 车牌号
	 * @param traceTimeBegin 开始时间
	 * @param traceTimeEnd 结束时间
	 * @return 位置点信息集合
	 */
	public List<LocationTracks> traceReplay(String license, long traceTimeBegin, long traceTimeEnd) {

		List<LocationRunState> pointsList = locationRunStateDao.queryPoints(license);
		List<LocationTracks> pointsListFinish = new ArrayList<LocationTracks>();
		if (pointsList.size() > 0) {
			for (int i = 0; i < pointsList.size(); i ++) {
				long time = Long.parseLong(pointsList.get(i).getLocationTime());
				if ((time >= traceTimeBegin) && (time <= traceTimeEnd)) {
					LocationRunState ls = pointsList.get(i);
					pointsListFinish.add(new LocationTracks(ls.getLongitude(), ls.getLatitude(), 0, 0));
				}
			}
		}
//			for (int i = 0; i < pointsList.size(); i++) {
//				System.out.println(pointsList.get(i).getLocationTime());
//			}
		return pointsListFinish;
	}
}
