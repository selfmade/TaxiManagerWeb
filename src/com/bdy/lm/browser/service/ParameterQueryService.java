package com.bdy.lm.browser.service;

import java.util.List;

import com.bdy.lm.browser.dao.ParameterQueryDao;
import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.browser.domain.ParameterQuery;
import com.google.gson.Gson;

public class ParameterQueryService {

	private ParameterQueryDao parameterQueryDao = new ParameterQueryDao();
	private Gson gson = new Gson();
	public List<ParameterQuery> queryParameter(String queryMode, String queryContent){
		List<ParameterQuery> parameterQueries = parameterQueryDao.queryParameter(queryMode, queryContent);
		for (int i = 0; i < parameterQueries.size(); i++) {
			ParameterQuery parameterQuery = parameterQueries.get(i);
			parameterQuery.setFare(gson.fromJson(parameterQuery.getParameter(), Fare.class));
			parameterQueries.set(i, parameterQuery);
		}
		return parameterQueries;
	}
}
