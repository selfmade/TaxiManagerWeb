package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bdy.lm.browser.dao.TaxiDao;
import com.bdy.lm.browser.domain.Taxi;

public class TaxiService {

	private TaxiDao taxiDao = new TaxiDao();

	public List<Taxi> queryTaxi(String queryMode, String queryContent) {
		List<Taxi> taxi = new ArrayList<Taxi>();
		taxi = taxiDao.queryTaxi(queryMode, queryContent);
		return taxi;
	}
	
	public boolean addTaxi(Taxi taxi) {

		try {
			if (taxiDao.queryTaxiByLicense(taxi.getLicense()) != null) {
				return false;// 添加不成功,已存在
			} else {
				return taxiDao.addTaxi(taxi);// 添加成功
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean deleteTaxi(List<String> license) {

		try {
			return taxiDao.deleteTaxi(license);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public Taxi editTaxi(String license) {
		
		return taxiDao.queryTaxiByLicense(license);
	}
	
	public boolean modifyTaxi(Taxi taxi) {
		if(taxiDao.queryTaxiByLicense(taxi.getLicense()) != null) {
			try {
				return taxiDao.modifyTaxi(taxi);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		} else {
			return  false;
		}
	}
}
