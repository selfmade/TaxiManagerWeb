package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.List;

import com.bdy.lm.browser.dao.FareDao;
import com.bdy.lm.browser.domain.Fare;
import com.bdy.lm.utils.Utils;

public class FareService {
	
	private FareDao fareDao = new FareDao();

	public List<Fare> queryFare(String queryMode, String queryContent) {
		List<Fare> fares = fareDao.queryFare(queryMode, queryContent);
		return fares;
	}

	public boolean addFare(Fare fare) {

		try {
			List<Fare> fares = fareDao.queryFare("all", null);
			//添加之前先进行比较，若相同存在完全相同的则不能添加
			int fareNum = 0;
			for (Fare f : fares) {
				if (f.equals(fare)) {//比较时不包含updateTime
					return false;// 添加不成功,已存在
				}
				if (f.getFareNum() > fareNum) {
					fareNum = f.getFareNum();
				}
			}
			fare.setFareNum(fareNum + 1);
			return fareDao.addFare(fare);// 添加成功
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean deleteFare(List<String> updateTime) {

		try {
			return fareDao.deleteFare(updateTime);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Fare editFare(String updateTime) {
		return fareDao.queryFareByUpdate(updateTime);
	}
	
	public String modifyFare(Fare fare) {
		if(fareDao.queryFareByUpdate(fare.getUpdateTime()) != null) {
			try {
				return fareDao.modifyFare(fare);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		} else {
			return  "";
		}
	}

	

}
