package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bdy.lm.browser.dao.RemoteManageDao;
import com.bdy.lm.browser.dao.VerifyAdjustDao;
import com.bdy.lm.browser.domain.CarMeasure;
import com.bdy.lm.browser.domain.Command;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.StandardRoute;
import com.bdy.lm.utils.Utils;

public class VerifyAdjustService {
	
	private VerifyAdjustDao verifyAdjustDao = new VerifyAdjustDao();
	private RemoteManageDao remoteManageDao = new RemoteManageDao();
	
	public List<StandardRoute> queryStandardRoutes() {
		return verifyAdjustDao.queryStandardRoute();
	}
	
	public boolean addStandardRoute(StandardRoute standardRoute) {
		List<StandardRoute> standardRoutes = verifyAdjustDao.queryStandardRoute();
		int num = 0;
		for (StandardRoute route : standardRoutes) {
			if (route.getNum() > num) {
				num = route.getNum();
			}
		}
		num++;
		standardRoute.setNum(num);
		standardRoute.setStandard(standardRoute.getSmeasure() + standardRoute.getAradius() + standardRoute.getBradius());
		return verifyAdjustDao.addStandardRoute(standardRoute);
	}
	
	public StandardRoute editStandardRoute(int num) {
		return verifyAdjustDao.queryStandardRouteByNum(num);
	}
	
	public boolean modifyStandardRoute(StandardRoute standardRoute) {
		standardRoute.setStandard(standardRoute.getSmeasure() + standardRoute.getAradius() + standardRoute.getBradius());
		return verifyAdjustDao.modifyStandardRoute(standardRoute);
	}
	
	public boolean deleteStandardRoute(List<Integer> nums) {
		return verifyAdjustDao.deleteStandardRoute(nums);
	}
	
	public List<CarMeasure> queryCarMeasure() {
		List<CarMeasure> list = verifyAdjustDao.queryCarMeasure();
		for (int i = 0; i < list.size(); i++) {
			CarMeasure c = list.get(i);
			c.setVerifyTime(Utils.stampToDate(c.getVerifyTime()));
			//c.setIsRegular(c.getIsRegular() + "" + claculateDistance(c.get));
		}
		return list;
	}

	public List<LocationTracks> queryVerifyTrace(String verifyTime) {
		//System.out.println("verifyTime " + Utils.dateToStamp(verifyTime));
		return verifyAdjustDao.queryVerifyTrace(Utils.dateToStamp(verifyTime));
	}
	
/*	public boolean sendToISU(int num, List<String> license, String sendType) {
		List<String> ISUFlag = new ArrayList<String>();
		if (sendType.equals("all")) {
			List<Object> allISUFlag = remoteManageDao.queryAllISUFlag();
			for(Object isu : allISUFlag) {
				ISUFlag.add((String) isu);
			}
		} else if (sendType.equals("part")) {
			try {
				ISUFlag = remoteManageDao.queryPartISUFlag(license);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		List<Command> commands = new ArrayList<Command>();
		for(String isu : ISUFlag) {
			Command command = new Command();
			command.setISUFlag(isu);
			command.setMessageID(msgID);
			command.setPublishTime(Long.toString(System.currentTimeMillis()));
			command.setIsExecute("no");
			if (msgID == 108) {
				command.setContent("控制拍照");
				command.setMsg("控制拍照");
			} else if (msgID == 109) {
				command.setContent("普通消息");
				command.setMsg(msg);
			}
			commands.add(command);
		}
		return remoteManageDao.addCommand(commands);
	}*/
	
}
