package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.List;

import com.bdy.lm.browser.dao.OperationQueryDao;
import com.bdy.lm.browser.domain.LocationTracks;
import com.bdy.lm.browser.domain.OperationMsg;
import com.bdy.lm.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class OperationQueryService {

	private OperationQueryDao operationQueryDao = new OperationQueryDao();
	public List<OperationMsg> queryOperation(String queryMode, String queryContent) {
		return operationQueryDao.queryOperation(queryMode, queryContent);
	}
	
	public List<LocationTracks> queryCheatMonitor(String license, String getOnTime) {
		try {
			byte[] lr = operationQueryDao.queryCheatMonitor(license, getOnTime);
			if (lr == null) {
				//System.out.println("reslut:null" + license + getOnTime);
				return null;
			} else {
				String result = new String(lr);
				//System.out.println("reslut:" + result);
				List<LocationTracks> list = new Gson().fromJson(result, new TypeToken<List<LocationTracks>>(){}.getType());
				//System.out.println("reslut json:" + list.size());
//				for (int i = 0; i < list.size(); i++) {
//					LocationTracks l = list.get(i);
//					l.setLongitude(Utils.double11(l.getLongitude()));
//					l.setLatitude(Utils.double11(l.getLatitude()));
//				}
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
