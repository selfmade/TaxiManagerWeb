package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.bdy.lm.browser.dao.RemoteManageDao;
import com.bdy.lm.browser.domain.Command;
import com.bdy.lm.browser.domain.HistoryMsg;
import com.bdy.lm.browser.domain.RemoteManageQueryTaxi;

public class RemoteManageService {

	private RemoteManageDao remoteManageDao = new RemoteManageDao();

	public List<RemoteManageQueryTaxi> queryTaxi(String queryMode, String queryContent) {

		List<RemoteManageQueryTaxi> taxi = remoteManageDao.queryTaxi(queryMode, queryContent);
		if (taxi != null) {
			for (RemoteManageQueryTaxi sTaxi : taxi) {
				if (System.currentTimeMillis() - Long.parseLong(sTaxi.getReportTime()) > 300000) {//五分钟内无汇报，视为离线
					sTaxi.setIsOnline("离线");					
				} else {
					sTaxi.setIsOnline("在线");
				}
				sTaxi.setReportTime(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.parseLong(sTaxi.getReportTime()))));
			}
		}
		return taxi;
	}
	
	public boolean sendCommand(int msgID, List<String> license, String msg, String sendType) {
		List<String> ISUFlag = new ArrayList<String>();
		String[] nums = null;
		if (msg.contains(",")) {
			nums = msg.split(",");
			System.out.println(Arrays.toString(nums));
		}
		if (sendType.equals("all")) {//查询全部的ISU flag保存到一个list
			List<Object> allISUFlag = remoteManageDao.queryAllISUFlag();
			for(Object isu : allISUFlag) {
				ISUFlag.add((String) isu);
			}
		} else if (sendType.equals("part")) {
			try {
				ISUFlag = remoteManageDao.queryPartISUFlag(license);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
		List<Command> commands = new ArrayList<Command>();
		for(String isu : ISUFlag) {
			Command command = new Command();
			command.setISUFlag(isu);
			command.setMessageID(msgID);
			command.setPublishTime(Long.toString(System.currentTimeMillis()));
			command.setIsExecute("no");
			if (msgID == 108) {
				command.setContent("控制拍照");
				command.setMsg("控制拍照");
			} else if (msgID == 109) {
				command.setContent("普通消息");
				command.setMsg(msg);
			} else if (msgID == 115){
				command.setContent("查询ISU参数");
				command.setMsg("查询ISU参数");
			} else if (msgID == 117){
				command.setContent("控制终端校准");
				command.setMsg("控制终端校准");
			}
			commands.add(command);
		}
		return remoteManageDao.addCommand(commands);
	}
	
	public boolean sendVerifyCommand(String msg) {
		List<String> ISUFlag = new ArrayList<String>();
		String[] nums = msg.split(",");
		System.out.println("sendVerifyCommand" + Arrays.toString(nums));
		List<Object> allISUFlag = remoteManageDao.queryAllISUFlag();
		for(Object isu : allISUFlag) {
			ISUFlag.add((String) isu);
		}
		List<Command> commands = new ArrayList<Command>();
		for(String isu : ISUFlag) {
			for (int i = 0; i < nums.length; i++) {
				Command command = new Command();
				command.setISUFlag(isu);
				command.setMessageID(113);
				command.setPublishTime(Long.toString(System.currentTimeMillis()));
				command.setIsExecute("no");
				command.setContent("控制校准");
				command.setMsg(nums[i]);
				commands.add(command);
			}
		}
		//System.out.println("Commands" + commands.toString());
		return remoteManageDao.addCommand(commands);
	}

	public List<HistoryMsg> queryHistoryMsg() {
		List<HistoryMsg> historyMsgs = remoteManageDao.queryHistoryMsg();
		List<HistoryMsg> reHistoryMsgs = new ArrayList<HistoryMsg>();
		for(HistoryMsg hMsg : historyMsgs) {
			hMsg.setPublishTime(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.parseLong(hMsg.getPublishTime()))));
			reHistoryMsgs.add(hMsg);
		}
		return reHistoryMsgs;
	}

	public Map<String, List<String>> queryExecute(List<String> msg) {
		
		try {
			return remoteManageDao.queryExecute(msg);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
