package com.bdy.lm.browser.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bdy.lm.browser.dao.CompanyDao;
import com.bdy.lm.browser.domain.Company;
import com.bdy.lm.browser.domain.Driver;


public class CompanyService {

	private CompanyDao companyDao = new CompanyDao();

	public List<Company> queryCompany(String queryMode, String queryContent) {
		List<Company> company = new ArrayList<Company>();
		try {
			company = companyDao.queryCompany(queryMode, queryContent);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return company;
	}
	
	public boolean addCompany(Company company) {

		try {
			if (companyDao.queryCompanyByCompanyId(company.getCompanyId()) != null) {
				return false;// 添加不成功,已存在
			} else {
				return companyDao.addCompany(company);// 添加成功
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean deleteCompany(List<String> CompanyId) {

		try {
			return companyDao.deleteCompany(CompanyId);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public Company editCompany(String id) {
		
		return companyDao.queryCompanyByCompanyId(id);
	}
	
	public boolean modifyCompany(Company company) {
		if(companyDao.queryCompanyByCompanyId(company.getCompanyId()) != null) {
			try {
				return companyDao.modifyCompany(company);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		} else {
			return  false;
		}
	}
}
