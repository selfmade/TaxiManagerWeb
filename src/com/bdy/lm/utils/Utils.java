package com.bdy.lm.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class Utils {

	public static String doubleFour(double d) {
		return new DecimalFormat("######0.0000").format(d);
	}
	public static double double11(double d) {
		return Double.parseDouble(new DecimalFormat("######0.00000000000").format(d));
	}
	public static String dateToStamp(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return Long.toString(sdf.parse(date).getTime());
		} catch (ParseException e) {
			new RuntimeException(e);
		}
		return "";
	}
	
	public static String stampToDate(String stampDate) {
		return new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.parseLong(stampDate)));
	}
	
	public static String getCurrentStamp() {
		return Long.toString(System.currentTimeMillis());
	}
	
	public static String getCurrentTime() {
		return stampToDate(Long.toString(System.currentTimeMillis()));
	}
	
	
    private static final String SEP1 = "#";  
    private static final String SEP2 = "|";
    
    /** 
     * List转换String 
     *  
     * @param list 
     *            :需要转换的List 
     * @return String转换后的字符串 
     */  
    public static String ListToString(List<?> list) {  
        StringBuffer sb = new StringBuffer();  
        if (list != null && list.size() > 0) {  
            for (int i = 0; i < list.size(); i++) {  
                if (list.get(i) == null || list.get(i) == "") {  
                    continue;  
                }  
                // 如果值是list类型则调用自己  
                if (list.get(i) instanceof List) {  
                    sb.append(ListToString((List<?>) list.get(i)));  
                    sb.append(SEP1);  
                } else if (list.get(i) instanceof Map) {  
                    sb.append(MapToString((Map<?, ?>) list.get(i)));  
                    sb.append(SEP1);  
                } else {  
                    sb.append(list.get(i));  
                    sb.append(SEP1);  
                }  
            }  
        }  
        return "L" + sb.toString();//EspUtils.EncodeBase64(sb.toString());  
    }
    
    /** 
     * Map转换String 
     *  
     * @param map 
     *            :需要转换的Map 
     * @return String转换后的字符串 
     */  
    public static String MapToString(Map<?, ?> map) {  
        StringBuffer sb = new StringBuffer();  
        // 遍历map  
        for (Object obj : map.keySet()) {  
            if (obj == null) {  
                continue;  
            }  
            Object key = obj;  
            Object value = map.get(key);  
            if (value instanceof List<?>) {  
                sb.append(key.toString() + SEP1 + ListToString((List<?>) value));  
                sb.append(SEP2);  
            } else if (value instanceof Map<?, ?>) {  
                sb.append(key.toString() + SEP1  
                        + MapToString((Map<?, ?>) value));  
                sb.append(SEP2);  
            } else {  
                sb.append(key.toString() + SEP1 + value.toString());  
                sb.append(SEP2);  
            }  
        }  
        return "M" + sb.toString();//EspUtils.EncodeBase64(sb.toString());  
    }
    
    /**
     * 获取两点的距离
     * @param beginLat 开始点纬度
     * @param beginLng 开始点经度
     * @param endLat 结束点纬度
     * @param endLng 结束经度
     * @return 两点间距离 单位：千米
     */
    public static double getDistance(double beginLat, double beginLng, double endLat, double endLng) {
        final double EARTH_RADIUS = 6378.137;//地球半径,单位千米
        double a = rad(beginLat) - rad(endLat);
        double b = rad(beginLng) - rad(endLng);
        double radLat1 = rad(beginLat);
        double radLat2 = rad(endLat);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        return s;
    }

    /**
     *  将度转化为弧度
     * @param d
     * @return
     */
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }
}
