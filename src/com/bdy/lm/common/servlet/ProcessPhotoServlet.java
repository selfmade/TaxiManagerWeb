package com.bdy.lm.common.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bdy.lm.browser.dao.DriverDao;
import com.bdy.lm.browser.servlet.BaseFileServlet;

public class ProcessPhotoServlet extends BaseFileServlet {

	public void getPhoto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.setContentType("text/html;charset=UTF-8");//处理响应编码
		request.setCharacterEncoding("UTF-8");
        OutputStream os = null;
		
		try {
			String ICID = request.getParameter("ICID");
			byte[] data = new DriverDao().queryDriverPhoto(ICID);
			if (data != null && data.length > 0) {
				System.out.println("照片大小：" + data.length);
				os = response.getOutputStream();
		        os.write(data);
		        os.flush();
		        os.close();
			} else {
				System.out.println("没有查到照片");
				response.setContentType("text/html;charset=UTF-8");//处理响应编码
				response.getWriter().write("<script>alert(\"没有照片!\");</script>");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				os.close();
			}
		}

	}

	public void photoUpload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		byte[] photo = (byte[]) super.form.get(super.fileName);
		//照片已经获取，未进行保存
		System.out.println("Size:" + photo.length);
	}
}

